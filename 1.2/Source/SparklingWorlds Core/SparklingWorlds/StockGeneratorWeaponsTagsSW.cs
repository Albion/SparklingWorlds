﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Verse;

namespace SparklingWorlds
{
    class StockGeneratorWeaponsTagsSW : StockGenerator_MiscItems
    {
        [NoTranslate]
        private string weaponTag = null;

        private static readonly SimpleCurve SelectionWeightMarketValueCurve = new SimpleCurve
        {
            {
                new CurvePoint(0f, 1f),
                true
            },
            {
                new CurvePoint(500f, 1f),
                true
            },
            {
                new CurvePoint(1500f, 0.2f),
                true
            },
            {
                new CurvePoint(5000f, 0.1f),
                true
            }
        };

        public override bool HandlesThingDef(ThingDef td)
        {
            if (!td.IsWeapon)
            {
                return false;
            }
            if(td.weaponTags == null || weaponTag == null)
            {
                return false;
            }
            return base.HandlesThingDef(td) && td.weaponTags.Contains(this.weaponTag);
        }

        protected override float SelectionWeight(ThingDef thingDef)
        {
            return StockGeneratorWeaponsTagsSW.SelectionWeightMarketValueCurve.Evaluate(thingDef.BaseMarketValue);
        }
    }
}

