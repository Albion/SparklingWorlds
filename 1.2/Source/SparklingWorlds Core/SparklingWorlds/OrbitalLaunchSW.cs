﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RimWorld;
using Verse;
using UnityEngine;
using System.Diagnostics;
using RimWorld.Planet;

namespace SparklingWorlds
{

    public class CompProperties_OrbitalLaunchSW : CompProperties
    {
        public ThingDef spawnThingDef;

        public ThingDef leavingThingDef;

        public CompProperties_OrbitalLaunchSW()
        {
            this.compClass = typeof(CompOrbitalLaunchSW);
        }
    }

    [StaticConstructorOnStartup]
    public class CompOrbitalLaunchSW : ThingComp
    {

        private static readonly Texture2D LaunchCommandTex = ContentFinder<Texture2D>.Get("UI/Commands/LaunchShip", true);

        private static readonly Texture2D TargeterMouseAttachment = ContentFinder<Texture2D>.Get("UI/Overlays/LaunchableMouseAttachment", true);

        public Building FuelingPortSource
        {
            get
            {
                return FuelingPortUtility.FuelingPortGiverAtFuelingPortCell(this.parent.Position, this.parent.Map);
            }
        }

        public bool ConnectedToFuelingPort
        {
            get
            {
                return this.FuelingPortSource != null;
            }
        }

        public bool FuelingPortSourceHasAnyFuel
        {
            get
            {
                return this.ConnectedToFuelingPort && this.FuelingPortSource.GetComp<CompRefuelable>().HasFuel;
            }
        }

        public float FuelingPortSourceFuel
        {
            get
            {
                if (!this.ConnectedToFuelingPort)
                {
                    return 0f;
                }
                return this.FuelingPortSource.GetComp<CompRefuelable>().Fuel;
            }
        }

        public CompProperties_OrbitalLaunchSW Props
        {
            get
            {
                return (CompProperties_OrbitalLaunchSW)this.props;
            }
        }

        [DebuggerHidden]
        public override IEnumerable<Gizmo> CompGetGizmosExtra()
        {
            foreach (Gizmo g in base.CompGetGizmosExtra())
            {
                yield return g;
            }
                Command_Action launch = new Command_Action();
                launch.defaultLabel = "Launch satellite";
                launch.defaultDesc = "Launches the satellite into orbit, spawning a targeter to call down bombardment. Needs full fuel to launch."; //TODO: add translations
                launch.icon = CompOrbitalLaunchSW.LaunchCommandTex;
                launch.alsoClickIfOtherInGroupClicked = false;
                launch.action = delegate
                {
                    this.TryLaunch();
                };
                if (!this.ConnectedToFuelingPort)
                {
                    launch.Disable("CommandLaunchGroupFailNotConnectedToFuelingPort".Translate());
                }
                else if (!this.FuelingPortSourceHasAnyFuel)
                {
                    launch.Disable("CommandLaunchGroupFailNoFuel".Translate());
                }
                else if (this.FuelingPortSourceFuel < 150f)
                {
                    launch.Disable("TransportPodNotEnoughFuel".Translate());
                }
                yield return launch;
        }

        public override string CompInspectStringExtra()
        {
            if (!this.ConnectedToFuelingPort)
            {
                return "NotReadyForLaunch".Translate() + ": " + "NotAllInGroupConnectedToFuelingPort".Translate() + ".";
            }
            if (!this.FuelingPortSourceHasAnyFuel)
            {
                return "NotReadyForLaunch".Translate() + ": " + "NotAllFuelingPortSourcesInGroupHaveAnyFuel".Translate() + ".";
            }
            if (this.FuelingPortSourceFuel < 150f)
            {
                return "NotReadyForLaunch".Translate() + ": " + "TransportPodNotEnoughFuel".Translate();
            }
            return "ReadyForLaunch".Translate();
        }

        private void TryLaunch()
        {
            if (!this.parent.Spawned)
            {
                Log.Error("Tried to launch " + this.parent + ", but it's unspawned.");
                return;
            }
            Map map = this.parent.Map;
            IntVec3 currentPosition = this.parent.Position;
            this.FuelingPortSource.GetComp<CompRefuelable>().ConsumeFuel(150f);

            this.parent.Destroy(DestroyMode.Vanish);
            GenSpawn.Spawn(this.Props.spawnThingDef, currentPosition, map);
            GenSpawn.Spawn(SkyfallerMaker.MakeSkyfaller(this.Props.leavingThingDef), currentPosition, map);
        }

    }
}
