﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Verse;

namespace SparklingWorlds
{
    public class CherubMechanitesCompSW : HediffWithComps
    {

        public override void Tick()
        {
            base.Tick();

            Pawn parentPawn = this.pawn;

            if (parentPawn.health.hediffSet.HasHediff(MechanitesDefOfSW.LuciferiumHigh))
            {

                parentPawn.health.hediffSet.GetFirstHediffOfDef(MechanitesDefOfSW.LuciferiumHigh).Severity = -0.1f;

                parentPawn.health.hediffSet.GetFirstHediffOfDef(MechanitesDefOfSW.LuciferiumAddiction).Severity = -0.1f;

                parentPawn.needs.AddOrRemoveNeedsAsAppropriate();

                Find.LetterStack.ReceiveLetter("LetterLabelConditionHealedEventSW".Translate(new object[] { MechanitesDefOfSW.LuciferiumHigh.LabelCap }), "LetterConditionHealedEventSW".Translate(new object[]
                {
                    MechanitesDefOfSW.LuciferiumHigh.LabelCap,
                    this.def.LabelCap,
                    parentPawn.LabelShort
                }), LetterDefOf.NeutralEvent, parentPawn, null);
            }

        }

    }

    [DefOf]
    public static class MechanitesDefOfSW
    {
        public static HediffDef LuciferiumHigh;

        public static HediffDef LuciferiumAddiction;

        public static NeedDef Chemical_Luciferium;
    }
}
