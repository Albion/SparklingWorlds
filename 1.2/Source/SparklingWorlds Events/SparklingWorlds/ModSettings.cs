﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using UnityEngine;

namespace SparklingWorlds
{
    public class SparklingWorldsSettings : ModSettings
    {
        public bool shipCrash = true;
        public bool thrumboSighting = true;
        public bool doctorRequest = true;
        public bool psychicEmitter = true;
        public bool tradeFair = true;
        public bool insectHiveDrop = true;
        public bool huntingLodge = true;
        public bool weaponsPodCrash = true;
        //public float exampleFloat = 200f;

        public override void ExposeData()
        {
            Scribe_Values.Look(ref shipCrash, "shipCrash");
            Scribe_Values.Look(ref thrumboSighting, "thrumboSighting");
            Scribe_Values.Look(ref doctorRequest, "doctorRequest");
            Scribe_Values.Look(ref psychicEmitter, "psychicEmitter");
            Scribe_Values.Look(ref tradeFair, "tradeFair");
            Scribe_Values.Look(ref insectHiveDrop, "insectHiveDrop");
            Scribe_Values.Look(ref huntingLodge, "huntingLodge");
            Scribe_Values.Look(ref weaponsPodCrash, "weaponsPodCrash");
            //Scribe_Values.Look(ref exampleFloat, "exampleFloat", 200f);
            base.ExposeData();
        }
    }

    public class SparklingWorlds : Mod
    {
        /// <summary>
        /// A reference to our settings.
        /// </summary>
        SparklingWorldsSettings settings;

        /// <summary>
        /// A mandatory constructor which resolves the reference to our settings.
        /// </summary>
        /// <param name="content"></param>
        public SparklingWorlds(ModContentPack content) : base(content)
        {
            this.settings = GetSettings<SparklingWorldsSettings>();
        }

        /// <summary>
        /// The (optional) GUI part to set your settings.
        /// </summary>
        /// <param name="inRect">A Unity Rect with the size of the settings window.</param>
        public override void DoSettingsWindowContents(Rect inRect)
        {
            Listing_Standard listingStandard = new Listing_Standard();
            listingStandard.Begin(inRect);
            listingStandard.CheckboxLabeled("LetterLabelCrashedShipSW".Translate(), ref settings.shipCrash, "BoolToolTipSWorlds".Translate());
            listingStandard.CheckboxLabeled("ThrumboSightingName".Translate(), ref settings.thrumboSighting, "BoolToolTipSWorlds".Translate());
            listingStandard.CheckboxLabeled("DoctorRequestName".Translate(), ref settings.doctorRequest, "BoolToolTipSWorlds".Translate());
            listingStandard.CheckboxLabeled("PsychicEmitterName".Translate(), ref settings.psychicEmitter, "BoolToolTipSWorlds".Translate());
            listingStandard.CheckboxLabeled("TradeFairName".Translate(), ref settings.tradeFair, "BoolToolTipSWorlds".Translate());
            listingStandard.CheckboxLabeled("InsectHiveDropName".Translate(), ref settings.insectHiveDrop, "BoolToolTipSWorlds".Translate());
            listingStandard.CheckboxLabeled("HuntinLodgeOppTitleSW".Translate(), ref settings.huntingLodge, "BoolToolTipSWorlds".Translate());
            listingStandard.CheckboxLabeled("LetterLabelWeaponsCachePodCrash".Translate(), ref settings.weaponsPodCrash, "BoolToolTipSWorlds".Translate());

            //listingStandard.Label("exampleFloatExplanation");
            //settings.exampleFloat = listingStandard.Slider(settings.exampleFloat, 100f, 300f);
            listingStandard.End();
            base.DoSettingsWindowContents(inRect);
        }

        /// <summary>
        /// Override SettingsCategory to show up in the list of settings.
        /// Using .Translate() is optional, but does allow for localisation.
        /// </summary>
        /// <returns>The (translated) mod name.</returns>
        public override string SettingsCategory()
        {
            return "SparklingWorldsName".Translate();
        }
    }
}
