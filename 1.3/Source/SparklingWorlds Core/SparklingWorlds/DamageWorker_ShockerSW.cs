﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RimWorld;
using Verse;

namespace SparklingWorlds
{
    public class DamageWorker_ShockerSW : DamageWorker_AddInjury
    {

        public override DamageWorker.DamageResult Apply(DamageInfo dinfo, Thing thing)
        {
            DamageWorker.DamageResult result = new DamageWorker.DamageResult();
            Pawn pawn = thing as Pawn;
            if (pawn == null)
            {
                return result;
            }
            this.ApplyNervSystemShockToPawn(dinfo, pawn);
            return result;
        }

        protected void ApplyNervSystemShockToPawn(DamageInfo dinfo, Pawn pawn)
        {
            if (dinfo.Def.additionalHediffs != null)
            {
                List<DamageDefAdditionalHediff> additionalHediffs = dinfo.Def.additionalHediffs;
                for (int i = 0; i < additionalHediffs.Count; i++)
                {
                    DamageDefAdditionalHediff damageDefAdditionalHediff = additionalHediffs[i];
                    if (damageDefAdditionalHediff.hediff != null)
                    {
                        float severity = Rand.Range(0.16f, 0.24f);
                        if (severity >= 0f)
                        {
                            if (pawn.RaceProps.IsMechanoid)
                            {
                                severity *= 0.5f;
                            }
                            float bodySizeMultiplier = ((pawn.BodySize - 1.0f) / 2.0f) + 1.0f;
                            severity /= bodySizeMultiplier;
                            Hediff hediff = HediffMaker.MakeHediff(damageDefAdditionalHediff.hediff, pawn, null);
                            hediff.Severity = severity;
                            pawn.health.AddHediff(hediff, null, new DamageInfo?(dinfo));
                        }
                    }
                }
            }
        }
    }

}
