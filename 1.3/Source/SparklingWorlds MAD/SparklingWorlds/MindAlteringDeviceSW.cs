﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RimWorld;
using Verse;
using UnityEngine;
using System.Diagnostics;
using RimWorld.Planet;
using RimWorld.BaseGen;
using Verse.AI;
using Verse.Sound;

namespace SparklingWorlds
{

    public class Building_MADSW : Building_Casket
    {
        protected int IcookingTicking = 0;

        protected int IcookingTime = 12000;

        public CompPowerTrader powerComp;

        public CompGlower Glower;

        private ColorInt red = new ColorInt(255, 100, 100, 0);

        private ColorInt green = new ColorInt(100, 255, 100, 0);

        public ColorInt CurrentColour;

        public bool PowerOn => powerComp.PowerOn;

        public override void SpawnSetup(Map map, bool respawningAfterLoad)
        {
            base.SpawnSetup(map, respawningAfterLoad);
            Glower = this.GetComp<CompGlower>();
            powerComp = this.GetComp<CompPowerTrader>();
            ChangeColour(green);
            CurrentColour = green;
        }

        public override IEnumerable<FloatMenuOption> GetFloatMenuOptions(Pawn myPawn)
        {
            if (!powerComp.PowerOn)
            {
                FloatMenuOption item = new FloatMenuOption("CannotUseNoPower".Translate(), null, MenuOptionPriority.Default, null, null, 0f, null, null);
                return new List<FloatMenuOption>
            {
                item
            };
            }
            if (myPawn.IsQuestLodger())
            {
                FloatMenuOption floatMenuOption = new FloatMenuOption("CannotUseReason".Translate("CryptosleepCasketGuestsNotAllowed".Translate()), null, MenuOptionPriority.Default, null, null, 0f, null, null, true, 0);
                return new List<FloatMenuOption>
            {
                floatMenuOption
            };
            }
            if (!ReservationUtility.CanReserve(myPawn, this, 1))
            {
                FloatMenuOption item2 = new FloatMenuOption(Translator.Translate("CannotUseReserved"), (Action)null, MenuOptionPriority.Default, null, null, 0f, (Func<Rect, bool>)null, null);
                return new List<FloatMenuOption>
            {
                item2
            };
            }
            if (!ReachabilityUtility.CanReach(myPawn, this, PathEndMode.OnCell, Danger.Some, false))
            {
                FloatMenuOption item3 = new FloatMenuOption(Translator.Translate("CannotUseNoPath"), (Action)null, MenuOptionPriority.Default, null, null, 0f, (Func<Rect, bool>)null, null);
                return new List<FloatMenuOption>
            {
                item3
            };
            }
            if (!this.HasAnyContents)
            {
                FloatMenuOption item4 = new FloatMenuOption(Translator.Translate("EnterMadSW"), (Action)delegate
                {
                    Job val2 = new Job(MadDefOfSW.EnterMADSW, this);
                    ReservationUtility.Reserve(myPawn, this, val2);
                    myPawn.jobs.TryTakeOrderedJob(val2);
                }, MenuOptionPriority.Default, null, null, 0f, (Func<Rect, bool>)null, null);
                return new List<FloatMenuOption>
            {
                item4
            };
            }
            /*if (this.HasAnyContents)
            {
                FloatMenuOption item5 = new FloatMenuOption(Translator.Translate("OpenMadSW"), (Action)delegate
                {
                    Job val = new Job(MadDefOfSW.OpenMADSW, this);
                    ReservationUtility.Reserve(myPawn, this, val);
                    myPawn.jobs.TryTakeOrderedJob(val);
                }, MenuOptionPriority.Default, null, null, 0f, (Func<Rect, bool>)null, null);
                return new List<FloatMenuOption>
            {
                item5
            };
            }*/
            return new List<FloatMenuOption>
            {
            };
        }

        public void CookIt()
        {
            foreach (Thing item in this.innerContainer)
            {
                Pawn val = item as Pawn;
                if (val != null)
                {
                    TraitChanger(val);
                    ConclusionAdder(val);
                }
            }
        }

        public void ConclusionAdder(Pawn pawn)
        {
            pawn.health.AddHediff(MadDefOfSW.RewireSW, null, (DamageInfo?)null);
            if (PawnChanger.HasMemory(pawn, MadDefOfSW.WreckedSW) || PawnChanger.HasMemory(pawn, MadDefOfSW.ScrambledSW))
            {
                PawnChanger.ExecuteBadThings(pawn);
            }
            PawnChanger.SetMood(pawn);
        }

        public void TraitChanger(Pawn pawn)
        {
            int newTraitNumber = Rand.RangeInclusive(2, 3);
            int pawnTraitNumber = pawn.story.traits.allTraits.Count;

            //make sure the pawn doesn't get less traits if mods like More Trait Slots are active
            if(pawnTraitNumber > 3)
            {
                newTraitNumber = pawnTraitNumber;
            }

            PawnChanger.SetPawnTraits(pawn, newTraitNumber);
        }

        public override void Tick()
        {
            if (powerComp.PowerOn)
            {
                if (this.HasAnyContents)
                {
                    IcookingTicking++;
                    if (CurrentColour != red)
                    {
                        ChangeColour(red);
                    }
                    if (IcookingTicking >= IcookingTime)
                    {
                        CookIt();
                        this.EjectContents();
                        IcookingTicking = 0;
                    }
                }
                else if (CurrentColour != green)
                {
                    ChangeColour(green);
                }
            }
            else
            {
                if (CurrentColour != red)
                {
                    ChangeColour(red);
                }
                IcookingTicking = 0;
                if (this.HasAnyContents)
                {
                    this.EjectContents();
                }
            }
        }

        public override void EjectContents()
        {
            foreach (Thing item in this.innerContainer)
            {
                Pawn val = item as Pawn;
                if (val != null && IcookingTicking < IcookingTime)
                {
                    PawnChanger.ExecuteBadThings(val);
                }
            }
            if (!this.Destroyed)
            {
                SoundStarter.PlayOneShot(SoundDef.Named("CryptosleepCasketEject"), SoundInfo.InMap(new TargetInfo(this.Position, this.Map, false), 0));
            }
            IcookingTicking = 0;
            this.innerContainer.TryDropAll(this.InteractionCell, base.Map, ThingPlaceMode.Near);
            this.contentsKnown = true;
        }

        public override void Draw()
        {
            base.Draw();
            DrawCookingBar();
        }

        public void DrawCookingBar()
        {
            //Replaced Drawhelper with vanilla drawer here
            GenDraw.FillableBarRequest fillableBarRequest = default(GenDraw.FillableBarRequest);
            fillableBarRequest.size = new Vector2(0.55f, 0.16f);
            fillableBarRequest.fillPercent = (float)IcookingTicking / (float)IcookingTime;
            fillableBarRequest.filledMat = SolidColorMaterials.SimpleSolidColorMaterial(new Color(0.9f, 0.9f, 0.10f));
            fillableBarRequest.unfilledMat = SolidColorMaterials.SimpleSolidColorMaterial(new Color(0.6f, 0.6f, 0.6f));
            Rot4 rotation = this.Rotation;
            rotation.Rotate(RotationDirection.Clockwise);
            fillableBarRequest.rotation = rotation;
            if (fillableBarRequest.rotation == Rot4.West)
            {
                fillableBarRequest.rotation = Rot4.West;
                fillableBarRequest.center = this.DrawPos + Vector3.up * 0.1f + Vector3.back * 0.45f;
            }
            if (fillableBarRequest.rotation == Rot4.North)
            {
                fillableBarRequest.rotation = Rot4.North;
                fillableBarRequest.center = this.DrawPos + Vector3.up * 0.1f + Vector3.left * 0.45f;
            }
            if (fillableBarRequest.rotation == Rot4.East)
            {
                fillableBarRequest.rotation = Rot4.East;
                fillableBarRequest.center = this.DrawPos + Vector3.up * 0.1f + Vector3.forward * 0.45f;
            }
            if (fillableBarRequest.rotation == Rot4.South)
            {
                fillableBarRequest.rotation = Rot4.South;
                fillableBarRequest.center = this.DrawPos + Vector3.up * 0.1f + Vector3.right * 0.45f;
            }
            GenDraw.DrawFillableBar(fillableBarRequest);
        }

        public void ChangeColour(ColorInt colour)
        {
            
            CurrentColour = colour;

            if(Glower != null)
            {
                float newGlowRadius = Glower.Props.glowRadius;
                this.Map.glowGrid.DeRegisterGlower(Glower);
                Glower.Initialize(new CompProperties_Glower
                {
                    compClass = typeof(CompGlower),
                    glowColor = colour,
                    glowRadius = newGlowRadius
                });
                this.Map.mapDrawer.MapMeshDirty(this.Position, MapMeshFlag.Things);
                this.Map.glowGrid.RegisterGlower(Glower);
            }
        }

        public override bool TryAcceptThing(Thing thing, bool allowSpecialEffects = true)
        {
            if (base.TryAcceptThing(thing, allowSpecialEffects))
            {
                if (allowSpecialEffects)
                {
                    SoundDef.Named("CryptosleepCasketAccept").PlayOneShot(new TargetInfo(base.Position, base.Map, false));
                }
                return true;
            }
            return false;
        }

        public static Building_MADSW FindCryptosleepCasketFor(Pawn p, Pawn traveler, bool ignoreOtherReservations = false)
        {
            IEnumerable<ThingDef> enumerable = from def in DefDatabase<ThingDef>.AllDefs
                                               where typeof(Building_MADSW).IsAssignableFrom(def.thingClass)
                                               select def;
            foreach (ThingDef current in enumerable)
            {
                Building_MADSW building_MadSW = (Building_MADSW)GenClosest.ClosestThingReachable(p.Position, p.Map, ThingRequest.ForDef(current), PathEndMode.InteractionCell, TraverseParms.For(traveler, Danger.Deadly, TraverseMode.ByPawn, false), 9999f, delegate (Thing x)
                {
                    bool arg_33_0;
                    if (!((Building_MADSW)x).HasAnyContents)
                    {
                        Pawn traveler2 = traveler;
                        LocalTargetInfo target = x;
                        bool ignoreOtherReservations2 = ignoreOtherReservations;
                        arg_33_0 = traveler2.CanReserve(target, 1, -1, null, ignoreOtherReservations2);
                    }
                    else
                    {
                        arg_33_0 = false;
                    }
                    return arg_33_0;
                }, null, 0, -1, false, RegionType.Set_Passable, false);
                if (building_MadSW != null)
                {
                    return building_MadSW;
                }
            }
            return null;
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Values.Look<int>(ref this.IcookingTicking, "IcookingTicking", 0, false);
            Scribe_Values.Look<int>(ref this.IcookingTime, "IcookingTime", 12000, false);
        }
    }


    public class PawnChanger
    {
        public static void ExecuteBadThings(Pawn pawn)
        {
            try
            {
                GiveNewMemory(pawn, MadDefOfSW.ScrambledSW);
                int num = Rand.RangeInclusive(1, 100);
                if (num <= 30)
                {
                    if (!pawn.health.hediffSet.HasHediff(MadDefOfSW.BadMigraineSW))
                    {
                        pawn.health.AddHediff(MadDefOfSW.BadMigraineSW, null, (DamageInfo?)null);
                        Find.LetterStack.ReceiveLetter(MadDefOfSW.BadMigraineSW.LabelCap.ToString(), "MADBadStuffSW".Translate(pawn.LabelShort, MadDefOfSW.BadMigraineSW.LabelCap), LetterDefOf.NegativeEvent, pawn);
                        return;
                    }
                }

                if (num <= 50)
                {
                    if (!pawn.health.hediffSet.HasHediff(MadDefOfSW.BodyRestartSW))
                    {
                        pawn.health.AddHediff(MadDefOfSW.BodyRestartSW, null, (DamageInfo?)null);
                        Find.LetterStack.ReceiveLetter(MadDefOfSW.BodyRestartSW.LabelCap.ToString(), "MADBadStuffSW".Translate(pawn.LabelShort, MadDefOfSW.BodyRestartSW.LabelCap), LetterDefOf.NegativeEvent, pawn);
                        return;
                    }
                }

                if (num <= 80)
                {
                    if (!pawn.health.hediffSet.HasHediff(MadDefOfSW.Dementia))
                    {
                        pawn.health.AddHediff(MadDefOfSW.Dementia, null, (DamageInfo?)null);
                        Find.LetterStack.ReceiveLetter(MadDefOfSW.Dementia.LabelCap.ToString(), "MADBadStuffSW".Translate(pawn.LabelShort, MadDefOfSW.Dementia.LabelCap), LetterDefOf.NegativeEvent, pawn);
                        return;
                    }
                }
                if (!pawn.health.hediffSet.HasHediff(MadDefOfSW.BraindeathSW))
                {
                    pawn.health.AddHediff(MadDefOfSW.BraindeathSW, null, (DamageInfo?)null);
                    Find.LetterStack.ReceiveLetter(MadDefOfSW.BraindeathSW.LabelCap.ToString(), "MADBadStuffSW".Translate(pawn.LabelShort, MadDefOfSW.BraindeathSW.LabelCap), LetterDefOf.NegativeEvent, pawn);
                    return;
                }
                Log.Warning("Debug: Pawn already has all/worst MAD result.");
            }
            catch
            {
                Log.Warning("Debug: Something went wrong with executing bad things.");
            }
        }

        public static void SetMood(Pawn pawn)
        {
            try
            {
                if (HasMemory(pawn, MadDefOfSW.ScrambledSW))
                {
                    GiveNewMemory(pawn, MadDefOfSW.ScrambledSW);
                }
                else if (HasMemory(pawn, MadDefOfSW.WreckedSW))
                {
                    GiveNewMemory(pawn, MadDefOfSW.ScrambledSW);
                }
                else if (HasMemory(pawn, MadDefOfSW.ConfusedSW))
                {
                    GiveNewMemory(pawn, MadDefOfSW.WreckedSW);
                }
                else if (HasMemory(pawn, MadDefOfSW.WrongSW))
                {
                    GiveNewMemory(pawn, MadDefOfSW.ConfusedSW);
                }
                else
                {
                    GiveNewMemory(pawn, MadDefOfSW.WrongSW);
                }
            }
            catch
            {
                Log.Warning("Debug: Something went wrong with memory giving.");
            }
        }

        public static void GiveNewMemory(Pawn pawn, ThoughtDef Tdef)
        {
            ClearOfMADMemories(pawn);
            pawn.needs.mood.thoughts.memories.TryGainMemory(Tdef, null);
            Messages.Message("MADConslusionSW".Translate(pawn.LabelShort, Tdef.Label), pawn, MessageTypeDefOf.NeutralEvent);
        }

        public static void ClearOfMADMemories(Pawn pawn)
        {
            pawn.needs.mood.thoughts.memories.RemoveMemoriesOfDef(MadDefOfSW.WrongSW);
            pawn.needs.mood.thoughts.memories.RemoveMemoriesOfDef(MadDefOfSW.ConfusedSW);
            pawn.needs.mood.thoughts.memories.RemoveMemoriesOfDef(MadDefOfSW.WreckedSW);
            pawn.needs.mood.thoughts.memories.RemoveMemoriesOfDef(MadDefOfSW.ScrambledSW);
        }

        public static bool HasMemory(Pawn pawn, ThoughtDef Tdef)
        {
            Thought_Memory val = pawn.needs.mood.thoughts.memories.Memories.Find((Thought_Memory x) => (object)x.def == Tdef);
            if (val == null)
            {
                return false;
            }
            return true;
        }

        public static void SetPawnTraits(Pawn pawn, int num)
        {
            pawn.story.traits.allTraits.Clear();
            while (pawn.story.traits.allTraits.Count < num)
            {
                TraitDef random = DefDatabase<TraitDef>.GetRandom();
                if (!pawn.story.traits.HasTrait(random) && (random.conflictingTraits == null || !Contains(pawn, random.conflictingTraits)))
                {
                    Trait val = new Trait(random, PawnGenerator.RandomTraitDegree(random), false);
                    pawn.story.traits.GainTrait(val);
                }
            }
        }

        private static bool Contains(Pawn pPawnSel, List<TraitDef> lTraitDef)
        {
            TraitDef[] array = (TraitDef[])new TraitDef[lTraitDef.Count];
            lTraitDef.CopyTo(array, 0);
            for (int i = 0; i < array.Length; i++)
            {
                if (pPawnSel.story.traits.HasTrait(array[i]))
                {
                    return true;
                }
            }
            return false;
        }

        private static float CurMood(Pawn pawn)
        {
            if ((object)pawn.needs.mood == null)
            {
                return 0.5f;
            }
            return pawn.needs.mood.CurLevel;
        }
    }

    public class JobDriver_EnterMAD_SW : JobDriver_EnterCryptosleepCasket
    {
        protected override IEnumerable<Toil> MakeNewToils()
        {
            this.FailOnDespawnedOrNull(TargetIndex.A);
            yield return Toils_Goto.GotoThing(TargetIndex.A, PathEndMode.InteractionCell);
            Toil prepare = Toils_General.Wait(500, TargetIndex.None);
            prepare.FailOnCannotTouch(TargetIndex.A, PathEndMode.InteractionCell);
            prepare.WithProgressBarToilDelay(TargetIndex.A, false, -0.5f);
            yield return prepare;
            Toil enter = new Toil();
            enter.initAction = delegate
            {
                Pawn actor = enter.actor;
                Building_MADSW pod = (Building_MADSW)actor.CurJob.targetA.Thing;
                Action action = delegate
                {
                    actor.DeSpawn(DestroyMode.Vanish);
                    pod.TryAcceptThing(actor, true);
                };
                if (!pod.def.building.isPlayerEjectable)
                {
                    int freeColonistsSpawnedOrInPlayerEjectablePodsCount = this.Map.mapPawns.FreeColonistsSpawnedOrInPlayerEjectablePodsCount;
                    if (freeColonistsSpawnedOrInPlayerEjectablePodsCount <= 1)
                    {
                        Find.WindowStack.Add(Dialog_MessageBox.CreateConfirmation("CasketWarning".Translate(actor.Named("PAWN")).AdjustedFor(actor, "PAWN", true), action, false, null, WindowLayer.Dialog));
                    }
                    else
                    {
                        action();
                    }
                }
                else
                {
                    action();
                    return;
                }
            };
            enter.defaultCompleteMode = ToilCompleteMode.Instant;
            yield return enter;
        }
    }

    public class JobDriver_OpenMAD_SW : JobDriver_Open
    {
        protected override IEnumerable<Toil> MakeNewToils()
        {
            yield return Toils_Reserve.Reserve(TargetIndex.A, 1);
            yield return Toils_Goto.GotoCell(TargetIndex.A, PathEndMode.InteractionCell);
            yield return Toils_General.Wait(250, TargetIndex.None);
            yield return new Toil
            {
                initAction = (Action)delegate
                {
                    LocalTargetInfo targetA = this.TargetA;
                    ((Building_MADSW)targetA.Thing).EjectContents();
                },
                defaultCompleteMode = ToilCompleteMode.Instant
            };
        }
    }

}
