﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RimWorld;
using Verse;
using UnityEngine;
using System.Diagnostics;
using RimWorld.Planet;
using RimWorld.BaseGen;
using Verse.AI.Group;
using Verse.Sound;
using Verse.AI;

namespace SparklingWorlds
{
    public class HiveChunkSW : ThingWithComps, IAttackTarget, ILoadReferenceable
    {
        public bool active = true;

        public int nextPawnSpawnTick = -1;

        private List<Pawn> spawnedPawns = new List<Pawn>();

        private int ticksToSpawnInitialPawns = -1;

        public bool canSpawnPawns = true;

        private const int InitialPawnSpawnDelay = 500;

        private const int PawnSpawnRadius = 4;

        public const float MaxSpawnedPawnsPoints = 600f;

        public const float InitialPawnsPoints = 250f;

        private static readonly FloatRange PawnSpawnIntervalDays = new FloatRange(0.85f, 1.1f);

        Thing IAttackTarget.Thing
        {
            get
            {
                return this;
            }
        }

        public float TargetPriorityFactor
        {
            get
            {
                return 0.4f;
            }
        }

        public LocalTargetInfo TargetCurrentlyAimingAt
        {
            get
            {
                return LocalTargetInfo.Invalid;
            }
        }

        public bool ThreatDisabled(IAttackTargetSearcher disabledFor)
        {
            if (!base.Spawned)
            {
                return true;
            }
            return !this.active;
        }

        private float SpawnedPawnsPoints
        {
            get
            {
                this.FilterOutUnspawnedPawns();
                float num = 0f;
                for (int i = 0; i < this.spawnedPawns.Count; i++)
                {
                    num += this.spawnedPawns[i].kindDef.combatPower;
                }
                return num;
            }
        }

        private void FilterOutUnspawnedPawns()
        {
            for (int i = this.spawnedPawns.Count - 1; i >= 0; i--)
            {
                if (!this.spawnedPawns[i].Spawned)
                {
                    this.spawnedPawns.RemoveAt(i);
                }
            }
        }

        private Lord Lord
        {
            get
            {
                Predicate<Pawn> hasDefendHiveLord = delegate (Pawn x)
                {
                    Lord lord = x.GetLord();
                    return lord != null && lord.LordJob is LordJob_DefendAndExpandHiveChunkSW;
                };
                Pawn foundPawn = this.spawnedPawns.Find(hasDefendHiveLord);
                if (base.Spawned)
                {
                    if (foundPawn == null)
                    {
                        RegionTraverser.BreadthFirstTraverse(this.GetRegion(RegionType.Set_Passable), (Region from, Region to) => true, delegate (Region r)
                        {
                            List<Thing> list = r.ListerThings.ThingsOfDef(ThingDefsSW.InfestedShipChunkSW);
                            for (int i = 0; i < list.Count; i++)
                            {
                                if (list[i] != this)
                                {
                                    if (list[i].Faction == this.Faction)
                                    {
                                        foundPawn = ((HiveChunkSW)list[i]).spawnedPawns.Find(hasDefendHiveLord);
                                        if (foundPawn != null)
                                        {
                                            return true;
                                        }
                                    }
                                }
                            }
                            return false;
                        }, 20, RegionType.Set_Passable);
                    }
                    if (foundPawn != null)
                    {
                        return foundPawn.GetLord();
                    }
                }
                return null;
            }
        }

        public override void SpawnSetup(Map map, bool respawningAfterLoad)
        {
            base.SpawnSetup(map, respawningAfterLoad);
            if (base.Faction == null)
            {
                this.SetFaction(Faction.OfInsects, null);
            }
            if (!respawningAfterLoad)
            {
                this.ticksToSpawnInitialPawns = 420;
            }
        }

        private void SpawnInitialPawnsNow()
        {
            this.ticksToSpawnInitialPawns = -1;
            this.SpawnPawnsUntilPoints(InitialPawnsPoints);
        }

        public void SpawnPawnsUntilPoints(float points)
        {
            while (this.SpawnedPawnsPoints < points)
            {
                Pawn pawn;
                if (!this.TrySpawnPawn(out pawn))
                {
                    break;
                }
            }
            this.CalculateNextPawnSpawnTick();
        }

        public override void Tick()
        {
            base.Tick();
            if (base.Spawned)
            {
                this.FilterOutUnspawnedPawns();
                if (!this.active && !base.Position.Fogged(base.Map))
                {
                    this.Activate();
                }
                if (this.active)
                {
                    if (this.ticksToSpawnInitialPawns > 0)
                    {
                        this.ticksToSpawnInitialPawns--;
                        if (this.ticksToSpawnInitialPawns <= 0)
                        {
                            this.SpawnInitialPawnsNow();
                        }
                    }
                    else if (Find.TickManager.TicksGame >= this.nextPawnSpawnTick)
                    {
                        if (this.SpawnedPawnsPoints < MaxSpawnedPawnsPoints)
                        {
                            Pawn pawn;
                            bool flag = this.TrySpawnPawn(out pawn);
                            if (flag && pawn.caller != null)
                            {
                                pawn.caller.DoCall();
                            }
                        }
                        this.CalculateNextPawnSpawnTick();
                    }
                }
            }
        }

        public override void DeSpawn(DestroyMode mode = DestroyMode.Vanish)
        {
            Map map = base.Map;
            base.DeSpawn();
            List<Lord> lords = map.lordManager.lords;
            for (int i = 0; i < lords.Count; i++)
            {
                lords[i].ReceiveMemo(Hive.MemoDeSpawned);
            }
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Values.Look<bool>(ref this.active, "active", true, false);
            Scribe_Values.Look<int>(ref this.nextPawnSpawnTick, "nextPawnSpawnTick", 0, false);
            Scribe_Collections.Look<Pawn>(ref this.spawnedPawns, "spawnedPawns", LookMode.Reference, new object[0]);
            Scribe_Values.Look<int>(ref this.ticksToSpawnInitialPawns, "ticksToSpawnInitialPawns", 0, false);
            Scribe_Values.Look<bool>(ref this.canSpawnPawns, "canSpawnPawns", true, false);
            if (Scribe.mode == LoadSaveMode.PostLoadInit)
            {
                this.spawnedPawns.RemoveAll((Pawn x) => x == null);
            }
        }

        private void Activate()
        {
            this.active = true;
            this.nextPawnSpawnTick = Find.TickManager.TicksGame + Rand.Range(200, 400);
            CompSpawnerHives comp = base.GetComp<CompSpawnerHives>();
            if (comp != null)
            {
                comp.CalculateNextHiveSpawnTick();
            }
        }

        private void CalculateNextPawnSpawnTick()
        {
            float num = GenMath.LerpDouble(0f, 5f, 1f, 0.5f, (float)this.spawnedPawns.Count);
            this.nextPawnSpawnTick = Find.TickManager.TicksGame + (int)(HiveChunkSW.PawnSpawnIntervalDays.RandomInRange * 60000f / (num * Find.Storyteller.difficulty.enemyReproductionRateFactor));
        }

        private bool TrySpawnPawn(out Pawn pawn)
        {
            if (!this.canSpawnPawns)
            {
                pawn = null;
                return false;
            }
            float curPoints = this.SpawnedPawnsPoints;
            IEnumerable<PawnKindDef> source = from x in Hive.spawnablePawnKinds
                                              where curPoints + x.combatPower <= MaxSpawnedPawnsPoints
                                              select x;
            PawnKindDef kindDef;
            if (!source.TryRandomElement(out kindDef))
            {
                pawn = null;
                return false;
            }
            pawn = PawnGenerator.GeneratePawn(kindDef, base.Faction);
            this.spawnedPawns.Add(pawn);
            GenSpawn.Spawn(pawn, CellFinder.RandomClosewalkCellNear(base.Position, base.Map, 2, null), base.Map, WipeMode.Vanish);
            Lord lord = this.Lord;
            if (lord == null)
            {
                lord = this.CreateNewLord();
            }
            lord.AddPawn(pawn);
            SoundDefOf.Hive_Spawn.PlayOneShot(this);
            return true;
        }

        [DebuggerHidden]
        public override IEnumerable<Gizmo> GetGizmos()
        {
            foreach (Gizmo g in base.GetGizmos())
            {
                yield return g;
            }
            if (Prefs.DevMode)
            {
                yield return new Command_Action
                {
                    defaultLabel = "DEBUG: Spawn pawn",
                    icon = TexCommand.ReleaseAnimals,
                    action = delegate
                    {
                        Pawn pawn;
                        this.TrySpawnPawn(out pawn);
                    }
                };
            }
        }

        public override bool PreventPlayerSellingThingsNearby(out string reason)
        {
            if (this.spawnedPawns.Count > 0)
            {
                if (this.spawnedPawns.Any((Pawn p) => !p.Downed))
                {
                    reason = this.def.label;
                    return true;
                }
            }
            reason = null;
            return false;
        }

        private Lord CreateNewLord()
        {
            return LordMaker.MakeNewLord(base.Faction, new LordJob_DefendAndExpandHiveChunkSW(), base.Map, null);
        }
    }

    public class LordJob_DefendAndExpandHiveChunkSW : LordJob
    {
        public override bool CanBlockHostileVisitors
        {
            get
            {
                return false;
            }
        }

        public override bool AddFleeToil
        {
            get
            {
                return false;
            }
        }

        public override StateGraph CreateGraph()
        {
            //TODO: This might use some love to expand the Job a bit. Pawn harmed for example. There are some triggers and states that weren't copied.
            StateGraph stateGraph = new StateGraph();
            LordToil_DefendAndExpandHiveChunkSW lordToil_DefendAndExpandHiveChunk = new LordToil_DefendAndExpandHiveChunkSW();
            lordToil_DefendAndExpandHiveChunk.distToHiveToAttack = 10f;
            stateGraph.StartingToil = lordToil_DefendAndExpandHiveChunk;
            LordToil_AssaultColony lordToil_AssaultColony = new LordToil_AssaultColony();
            stateGraph.AddToil(lordToil_AssaultColony);
            Transition transition = new Transition(lordToil_DefendAndExpandHiveChunk, lordToil_AssaultColony);
            transition.AddTrigger(new Trigger_PawnHarmed(0.5f, true));
            transition.AddTrigger(new Trigger_PawnLostViolently());
            transition.AddTrigger(new Trigger_Memo(Hive.MemoAttackedByEnemy));
            transition.AddTrigger(new Trigger_Memo(Hive.MemoBurnedBadly));
            transition.AddTrigger(new Trigger_Memo(Hive.MemoDestroyedNonRoofCollapse));
            transition.AddTrigger(new Trigger_Memo(HediffGiver_Heat.MemoPawnBurnedByAir));
            transition.AddPostAction(new TransitionAction_EndAllJobs());
            stateGraph.AddTransition(transition);
            Transition transition2 = new Transition(lordToil_DefendAndExpandHiveChunk, lordToil_AssaultColony);
            transition2.canMoveToSameState = true;
            transition2.AddSource(lordToil_AssaultColony);
            transition2.AddTrigger(new Trigger_Memo(Hive.MemoDeSpawned));
            stateGraph.AddTransition(transition2);
            Transition transition3 = new Transition(lordToil_AssaultColony, lordToil_DefendAndExpandHiveChunk);
            transition3.AddTrigger(new Trigger_TicksPassedWithoutHarmOrMemos(1200, new string[]
            {
                Hive.MemoAttackedByEnemy,
                Hive.MemoBurnedBadly,
                Hive.MemoDestroyedNonRoofCollapse,
                HediffGiver_Heat.MemoPawnBurnedByAir
            }));
            transition3.AddPostAction(new TransitionAction_EndAttackBuildingJobs());
            stateGraph.AddTransition(transition3);
            return stateGraph;
        }
    }

    public class LordToil_DefendAndExpandHiveChunkSW : LordToil
    {
        public float distToHiveToAttack = 10f;

        private LordToilData_DefendAndExpandHiveChunkSW Data
        {
            get
            {
                return (LordToilData_DefendAndExpandHiveChunkSW)this.data;
            }
        }

        public LordToil_DefendAndExpandHiveChunkSW()
        {
            this.data = new LordToilData_DefendAndExpandHiveChunkSW();
        }

        public override void UpdateAllDuties()
        {
            this.FilterOutUnspawnedHives();
            for (int i = 0; i < this.lord.ownedPawns.Count; i++)
            {
                HiveChunkSW hiveFor = this.GetHiveFor(this.lord.ownedPawns[i]);
                PawnDuty duty = new PawnDuty(DutyDefsSW.DefendAndExpandHiveChunkSW, hiveFor, this.distToHiveToAttack);
                this.lord.ownedPawns[i].mindState.duty = duty;
            }
        }

        private void FilterOutUnspawnedHives()
        {
            this.Data.assignedHives.RemoveAll((KeyValuePair<Pawn, HiveChunkSW> x) => x.Value == null || !x.Value.Spawned);
        }

        private HiveChunkSW GetHiveFor(Pawn pawn)
        {
            HiveChunkSW hive;
            if (this.Data.assignedHives.TryGetValue(pawn, out hive))
            {
                return hive;
            }
            hive = this.FindClosestHive(pawn);
            if (hive != null)
            {
                this.Data.assignedHives.Add(pawn, hive);
            }
            return hive;
        }

        private HiveChunkSW FindClosestHive(Pawn pawn)
        {
            return (HiveChunkSW)GenClosest.ClosestThingReachable(pawn.Position, pawn.Map, ThingRequest.ForDef(ThingDefsSW.InfestedShipChunkSW), PathEndMode.Touch, TraverseParms.For(pawn, Danger.Deadly, TraverseMode.ByPawn, false), 30f, (Thing x) => x.Faction == pawn.Faction, null, 0, 30, false, RegionType.Set_Passable, false);
        }
    }

    public class LordToilData_DefendAndExpandHiveChunkSW : LordToilData
    {
        public Dictionary<Pawn, HiveChunkSW> assignedHives = new Dictionary<Pawn, HiveChunkSW>();

        public override void ExposeData()
        {
            if (Scribe.mode == LoadSaveMode.Saving)
            {
                this.assignedHives.RemoveAll((KeyValuePair<Pawn, HiveChunkSW> x) => x.Key.Destroyed);
            }
            Scribe_Collections.Look<Pawn, HiveChunkSW>(ref this.assignedHives, "assignedHives", LookMode.Reference, LookMode.Reference);
            if (Scribe.mode == LoadSaveMode.PostLoadInit)
            {
                this.assignedHives.RemoveAll((KeyValuePair<Pawn, HiveChunkSW> x) => x.Value == null);
            }
        }
    }

    public class JobGiver_HiveChunkDefenseSW : JobGiver_HiveDefense
    {
        protected override IntVec3 GetFlagPosition(Pawn pawn)
        {
            HiveChunkSW hive = pawn.mindState.duty.focus.Thing as HiveChunkSW;
            if (hive != null && hive.Spawned)
            {
                return hive.Position;
            }
            return pawn.Position;
        }
    }

    public class JobGiver_MaintainHiveChunkSW : JobGiver_AIFightEnemies
    {
        private static readonly float CellsInScanRadius = (float)GenRadial.NumCellsInRadius(7.9f);

        protected override Job TryGiveJob(Pawn pawn)
        {
            Room room = pawn.GetRoom(RegionType.Set_Passable);
            int num = 0;
            while ((float)num < JobGiver_MaintainHiveChunkSW.CellsInScanRadius)
            {
                IntVec3 intVec = pawn.Position + GenRadial.RadialPattern[num];
                if (intVec.InBounds(pawn.Map))
                {
                    if (intVec.GetRoom(pawn.Map) == room)
                    {
                        HiveChunkSW hive = (HiveChunkSW)pawn.Map.thingGrid.ThingAt(intVec, ThingDefsSW.InfestedShipChunkSW);
                        if (hive != null && pawn.CanReserve(hive, 1, -1, null, false))
                        {
                            CompMaintainable compMaintainable = hive.TryGetComp<CompMaintainable>();
                            if (compMaintainable.CurStage != MaintainableStage.Healthy)
                            {
                                return new Job(JobDefOf.Maintain, hive);
                            }
                        }
                    }
                }
                num++;
            }
            return null;
        }
    }

    public class JobGiver_WanderHiveChunkSW : JobGiver_Wander
    {
        public JobGiver_WanderHiveChunkSW()
        {
            this.wanderRadius = 7.5f;
            this.ticksBetweenWandersRange = new IntRange(125, 200);
        }

        protected override IntVec3 GetWanderRoot(Pawn pawn)
        {
            HiveChunkSW hive = pawn.mindState.duty.focus.Thing as HiveChunkSW;
            if (hive == null || !hive.Spawned)
            {
                return pawn.Position;
            }
            return hive.Position;
        }
    }

    public class CompProperties_SpawnerHiveChunkSW : CompProperties
    {
        public float HiveSpawnPreferredMinDist = 3.5f;

        public float HiveSpawnRadius = 10f;

        public FloatRange HiveSpawnIntervalDays = new FloatRange(1.9f, 2.8f);

        public CompProperties_SpawnerHiveChunkSW()
        {
            this.compClass = typeof(CompSpawnerHiveChunkSW);
        }
    }

    public class CompSpawnerHiveChunkSW : CompSpawnerHives
    {
        //Needs to be at least 100 ticks in the future.
        //This is due to it spawning in the inner container of the skyfaller
        //If its <100 the comp will try to spawn a hive on first tick leading to an error
        private int nextHiveSpawnTick = Find.TickManager.TicksGame + 200; 

        private CompProperties_SpawnerHiveChunkSW Props
        {
            get
            {
                return (CompProperties_SpawnerHiveChunkSW)this.props;
            }
        }

        private bool CanSpawnChildHive
        {
            get
            {
                return TotalHivesUtility();
            }
        }

        private bool TotalHivesUtility()
        {
            try
            {
                if (HiveUtility.TotalSpawnedHivesCount(this.parent.Map) < 30)
                {
                    return this.canSpawnHives && true;
                }
                return false;
            }
            catch
            {
                return true;
            }
        }

        public override void PostSpawnSetup(bool respawningAfterLoad)
        {
            if (!respawningAfterLoad)
            {
                this.CalculateNextHiveSpawnTick();
            }
        }

        public override void CompTick()
        {
            HiveChunkSW hive = this.parent as HiveChunkSW;
            if (Find.TickManager.TicksGame >= this.nextHiveSpawnTick)
            {
                Hive hive2;
                if (this.TrySpawnChildHive(true, out hive2))
                {
                    Messages.Message("MessageHiveReproduced".Translate(), hive2, MessageTypeDefOf.NegativeEvent);
                }
                else
                {
                    this.CalculateNextHiveSpawnTick();
                }
            }
        }

        public override string CompInspectStringExtra()
        {
            if (!this.canSpawnHives)
            {
                return "DormantHiveNotReproducing".Translate();
            }
            if (this.CanSpawnChildHive)
            {
                return "HiveReproducesIn".Translate() + ": " + (this.nextHiveSpawnTick - Find.TickManager.TicksGame).ToStringTicksToPeriod();
            }
            return null;
        }

        public new bool TrySpawnChildHive(bool ignoreRoofedRequirement, out Hive newHive)
        {
            bool result;
            if (!this.CanSpawnChildHive)
            {
                newHive = null;
                result = false;
            }
            else
            {
                try
                {
                    IntVec3 loc = CompSpawnerHiveChunkSW.FindChildHiveLocation(this.parent.Position, this.parent.Map, this.parent.def, this.Props, ignoreRoofedRequirement, false);
                    if (!loc.IsValid)
                    {
                        newHive = null;
                        result = false;
                    }
                    else
                    {
                        newHive = (Hive)GenSpawn.Spawn(ThingDefOf.Hive, loc, this.parent.Map, WipeMode.FullRefund);
                        if (newHive.Faction != this.parent.Faction)
                        {
                            newHive.SetFaction(this.parent.Faction, null);
                        }
                        HiveChunkSW hive = this.parent as HiveChunkSW;
                        if (hive != null)
                        {
                            newHive.CompDormant.WakeUp();
                        }
                        this.CalculateNextHiveSpawnTick();
                        result = true;
                    }
                }
                catch
                {
                    Log.Message("Something went wrong during infested ship chunk reproduction. Exception caught and handled. Please contact Albion with this error.");
                    newHive = null;
                    result = false;
                }
            }
            return result;
        }

        [DebuggerHidden]
        public override IEnumerable<Gizmo> CompGetGizmosExtra()
        {
            if (Prefs.DevMode)
            {
                yield return new Command_Action
                {
                    defaultLabel = "DEBUG: Reproduce",
                    icon = TexCommand.GatherSpotActive,
                    action = delegate
                    {
                        Hive hive;
                        this.TrySpawnChildHive(true, out hive);
                    }
                };
            }
        }

        private bool CanSpawnHiveAt(IntVec3 c, float minDist, bool ignoreRoofedRequirement)
        {
            if ((!ignoreRoofedRequirement && !c.Roofed(this.parent.Map)) || !c.Standable(this.parent.Map) || (minDist != 0f && (float)c.DistanceToSquared(this.parent.Position) < minDist * minDist))
            {
                return false;
            }
            for (int i = 0; i < 8; i++)
            {
                IntVec3 c2 = c + GenAdj.AdjacentCells[i];
                if (c2.InBounds(this.parent.Map))
                {
                    List<Thing> thingList = c2.GetThingList(this.parent.Map);
                    for (int j = 0; j < thingList.Count; j++)
                    {
                        if (thingList[j] is Hive)
                        {
                            return false;
                        }
                    }
                }
            }
            List<Thing> thingList2 = c.GetThingList(this.parent.Map);
            for (int k = 0; k < thingList2.Count; k++)
            {
                Thing thing = thingList2[k];
                if ((thing.def.category == ThingCategory.Item || thing.def.category == ThingCategory.Building) && GenSpawn.SpawningWipes(this.parent.def, thing.def))
                {
                    return false;
                }
            }
            return true;
        }

        public static IntVec3 FindChildHiveLocation(IntVec3 pos, Map map, ThingDef parentDef, CompProperties_SpawnerHiveChunkSW props, bool ignoreRoofedRequirement, bool allowUnreachable)
        {

            IntVec3 intVec = IntVec3.Invalid;
            for (int i = 0; i < 3; i++)
            {
                float minDist = props.HiveSpawnPreferredMinDist;
                bool flag;
                if (i < 2)
                {
                    if (i == 1)
                    {
                        minDist = 0f;
                    }
                    flag = CellFinder.TryFindRandomReachableCellNear(pos, map, props.HiveSpawnRadius, TraverseParms.For(TraverseMode.NoPassClosedDoors, Danger.Deadly, false), (IntVec3 c) => CompSpawnerHiveChunkSW.CanSpawnHiveAt(c, map, pos, parentDef, minDist, ignoreRoofedRequirement), null, out intVec, 999999);
                }
                else
                {
                    flag = (allowUnreachable && CellFinder.TryFindRandomCellNear(pos, map, (int)props.HiveSpawnRadius, (IntVec3 c) => CompSpawnerHiveChunkSW.CanSpawnHiveAt(c, map, pos, parentDef, minDist, ignoreRoofedRequirement), out intVec, -1));
                }
                if (flag)
                {
                    intVec = CellFinder.FindNoWipeSpawnLocNear(intVec, map, parentDef, Rot4.North, 2, (IntVec3 c) => CompSpawnerHiveChunkSW.CanSpawnHiveAt(c, map, pos, parentDef, minDist, ignoreRoofedRequirement));
                    break;
                }
            }
            return intVec;
        }

        private static bool CanSpawnHiveAt(IntVec3 c, Map map, IntVec3 parentPos, ThingDef parentDef, float minDist, bool ignoreRoofedRequirement)
        {
            bool result;
            if ((!ignoreRoofedRequirement && !c.Roofed(map)) || (!c.Walkable(map) || (minDist != 0f && (float)c.DistanceToSquared(parentPos) < minDist * minDist)) || c.GetFirstThing(map, ThingDefOf.InsectJelly) != null || c.GetFirstThing(map, ThingDefOf.GlowPod) != null)
            {
                result = false;
            }
            else
            {
                for (int i = 0; i < 9; i++)
                {
                    IntVec3 c2 = c + GenAdj.AdjacentCellsAndInside[i];
                    if (c2.InBounds(map))
                    {
                        List<Thing> thingList = c2.GetThingList(map);
                        for (int j = 0; j < thingList.Count; j++)
                        {
                            if (thingList[j] is Hive || thingList[j] is TunnelHiveSpawner)
                            {
                                result = false;
                                return result;
                            }
                        }
                    }
                }
                List<Thing> thingList2 = c.GetThingList(map);
                for (int k = 0; k < thingList2.Count; k++)
                {
                    Thing thing = thingList2[k];
                    bool flag = thing.def.category == ThingCategory.Building && thing.def.passability == Traversability.Impassable;
                    if (flag && GenSpawn.SpawningWipes(parentDef, thing.def))
                    {
                        result = true;
                        return result;
                    }
                }
                result = true;
            }
            return result;
        }

        public new void CalculateNextHiveSpawnTick()
        {
            this.nextHiveSpawnTick = Find.TickManager.TicksGame + (int)((this.Props.HiveSpawnIntervalDays.RandomInRange * 60000f / (Find.Storyteller.difficulty.enemyReproductionRateFactor))*Rand.Range(0.7f,1.0f));
        }

        public override void PostExposeData()
        {
            Scribe_Values.Look<int>(ref this.nextHiveSpawnTick, "nextHiveSpawnTick", 0, false);
            Scribe_Values.Look<bool>(ref this.canSpawnHives, "canSpawnHives", true, false);
        }
    }
}
