﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RimWorld;
using Verse;

namespace SparklingWorlds
{
    public class HediffGiver_BlueMoonCombatBoost : HediffGiver
    {

        public override void OnIntervalPassed(Pawn pawn, Hediff cause)
		{
            if (pawn.Faction != null)
            {
                if (pawn.Faction.def == BlueMoonCombatBoostDefOfSW.BlueMoonCorpSW && !pawn.IsPrisoner)
                {
                    if (!pawn.health.hediffSet.HasHediff(BlueMoonCombatBoostDefOfSW.BlueMoonCombatBoostSW))
                    {
                        pawn.health.AddHediff(BlueMoonCombatBoostDefOfSW.BlueMoonCombatBoostSW);
                    }
                }
            }
        }
    }

    [DefOf]
    public static class BlueMoonCombatBoostDefOfSW
    {
        public static HediffDef BlueMoonCombatBoostSW;

        public static FactionDef BlueMoonCorpSW;
    }
}
