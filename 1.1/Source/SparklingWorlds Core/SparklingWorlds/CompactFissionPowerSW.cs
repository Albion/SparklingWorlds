﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RimWorld;
using Verse;
using UnityEngine;

namespace SparklingWorlds
{
    public class Building_CompactFissionPowerSW : Building
    {
        public override void Tick()
        {
            base.Tick();
            if (this.GetComp<CompRefuelable>().HasFuel && Find.TickManager.TicksGame % 40 == 0)
            {
                IntVec3 exhaustPos = base.Position;
                exhaustPos.x = exhaustPos.x + 3;
                exhaustPos.z = exhaustPos.z - 2;

                for(int i = 0; i < 5; i++)
                {
                    MoteMaker.ThrowSmoke(exhaustPos.ToVector3(), base.Map, 0.8f);
                    exhaustPos.z++;
                }
            }
        }
    }

}
