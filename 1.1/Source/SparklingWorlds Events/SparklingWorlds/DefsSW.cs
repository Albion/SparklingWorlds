﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Verse;
using Verse.AI;

namespace SparklingWorlds
{
    [DefOf]
    public static class SiteDefsSW
    {
        public static SitePartDef ShipCrashCoreSW;

        public static SitePartDef HiveCrashCoreSW;

        public static SitePartDef ThrumboSightingCoreSW;

        public static SitePartDef PsychicEmitterCoreSW;

        public static SitePartDef TradeFairCoreSW;

        public static SitePartDef HuntingLodgeCoreSW;

        public static SitePartDef RaidOnArrivalSW;

        public static SitePartDef HuntingPartyArrivalSW;

        public static SitePartDef EmitterInsanityPulsSW;

    }

    [DefOf]
    public static class ThingDefsSW
    {
        public static ThingDef PsychicSoothePulser;

        public static ThingDef PsychicAnimalPulser;

        public static ThingDef PsychicShockLance;

        public static ThingDef PsychicInsanityLance;

        public static ThingDef CherubMechInjectorSW;

        public static ThingDef MechanoidHiveComputerCoreSW;

        public static ThingDef PsychicWaveEmitterSW;

        public static ThingDef InfestedShipChunkIncomingSW;

        public static ThingDef InfestedShipChunkSW;

        public static ThingDef DamagedShipEngineSW;

        public static ThingDef TableButcher;

        public static ThingDef FueledStove;
    }

    [DefOf]
    public static class IncidentDefsSW
    {
        public static IncidentDef RaidOnArrivalSW;

        public static IncidentDef HuntingPartyArrivalSW;

        public static IncidentDef PsychicEmitterAnimalInsanitySW;
    }

    [DefOf]
    public static class WorldObjectsDefsSW
    {
        public static WorldObjectDef DoctorRequestSW;

        public static WorldObjectDef EventSiteSW;
    }

    [DefOf]
    public static class DutyDefsSW
    {
        public static DutyDef DefendAndExpandHiveChunkSW;
    }

    [DefOf]
    public static class LetterDefsSW
    {
        public static LetterDef HuntingLodgeOppFeeDemand;
    }
}
