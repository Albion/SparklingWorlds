﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RimWorld;
using Verse;
using UnityEngine;
using System.Diagnostics;
using RimWorld.Planet;
using RimWorld.BaseGen;
using Verse.AI.Group;

namespace SparklingWorlds
{
    public class GenStep_BasicGenSW : GenStep
    {

        protected Dictionary<string, float> randomRoomEvents = new Dictionary<string, float>();

        protected CellRect adventureRegion;

        protected ResolveParams baseResolveParams;

        public override int SeedPart => 54867541;

        public override void Generate(Map map, GenStepParams parms)
        {
            int num = map.Size.x / 10;
            int num2 = 8 * map.Size.x / 10;
            int num3 = map.Size.z / 10;
            int num4 = 8 * map.Size.z / 10;
            this.adventureRegion = new CellRect(num, num3, num2, num4);
            this.adventureRegion.ClipInsideMap(map);
            BaseGen.globalSettings.map = map;
            this.randomRoomEvents.Clear();
            IntVec3 playerStartSpot;
            CellFinder.TryFindRandomEdgeCellWith((IntVec3 v) => GenGrid.Standable(v, map), map, 0f, out playerStartSpot);
            MapGenerator.PlayerStartSpot = playerStartSpot;
            this.baseResolveParams = default(ResolveParams);
            foreach (string current in this.randomRoomEvents.Keys)
            {
                this.baseResolveParams.SetCustom<float>(current, this.randomRoomEvents[current], false);
            }
        }
    }

    public class GenStep_ShipCrashSW : GenStep_BasicGenSW
    {

        public override void Generate(Map map, GenStepParams parms)
        {
            base.Generate(map, parms);
            CellRect rect = new CellRect(Rand.RangeInclusive(this.adventureRegion.minX + 10, this.adventureRegion.maxX - 50), Rand.RangeInclusive(this.adventureRegion.minZ + 10, this.adventureRegion.maxZ - 35), 40, 25);
            rect.ClipInsideMap(map);
            ResolveParams baseResolveParams = this.baseResolveParams;
            baseResolveParams.rect = rect;
            BaseGen.symbolStack.Push("crashedShipSW", baseResolveParams);
            BaseGen.Generate();
        }
    }

    public class GenStep_HiveCrashSW : GenStep_BasicGenSW
    {

        public override void Generate(Map map, GenStepParams parms)
        {
            base.Generate(map, parms);
            CellRect rect = new CellRect(Rand.RangeInclusive(this.adventureRegion.minX + 10, this.adventureRegion.maxX - 60), Rand.RangeInclusive(this.adventureRegion.minZ + 10, this.adventureRegion.maxZ - 50), 48, 36);
            rect.ClipInsideMap(map);
            ResolveParams baseResolveParams = this.baseResolveParams;
            baseResolveParams.rect = rect;
            BaseGen.symbolStack.Push("crashedHiveSW", baseResolveParams);
            BaseGen.Generate();
        }
    }

    public class GenStep_ThrumboSightingSW : GenStep_BasicGenSW
    {

        public override void Generate(Map map, GenStepParams parms)
        {
            base.Generate(map, parms);
            CellRect rect = new CellRect(Rand.RangeInclusive(this.adventureRegion.minX + 10, this.adventureRegion.maxX - 20), Rand.RangeInclusive(this.adventureRegion.minZ + 10, this.adventureRegion.maxZ - 20), 12, 12);
            rect.ClipInsideMap(map);
            ResolveParams baseResolveParams = this.baseResolveParams;
            baseResolveParams.rect = rect;
            BaseGen.symbolStack.Push("spawnThrumboGroupSW", baseResolveParams);
            BaseGen.Generate();
        }
    }

    public class GenStep_PsychicEmitterActivationSW : GenStep_BasicGenSW
    {

        public override void Generate(Map map, GenStepParams parms)
        {
            base.Generate(map, parms);
            CellRect rect = new CellRect(Rand.RangeInclusive(this.adventureRegion.minX + 20, this.adventureRegion.maxX - 50), Rand.RangeInclusive(this.adventureRegion.minZ + 20, this.adventureRegion.maxZ - 50), 30, 30);
            rect.ClipInsideMap(map);
            ResolveParams baseResolveParams = this.baseResolveParams;
            baseResolveParams.rect = rect;
            BaseGen.symbolStack.Push("psychicEmitterBaseSW", baseResolveParams);
            BaseGen.Generate();
        }
    }

    public class GenStep_TradeFairSW : GenStep_BasicGenSW
    {

        public override void Generate(Map map, GenStepParams parms)
        {
            if (map.ParentFaction == null)
            {
                Log.Warning("No ParentFaction for trade fair. Aborting.");
                return;
            }
            Faction hostingFaction = map.ParentFaction;
            base.Generate(map, parms);

            CellRect rect = new CellRect(Rand.RangeInclusive(this.adventureRegion.minX + 20, this.adventureRegion.maxX - 50), Rand.RangeInclusive(this.adventureRegion.minZ + 20, this.adventureRegion.maxZ - 50), 24, 24);
            rect.ClipInsideMap(map);
            ResolveParams baseResolveParams = this.baseResolveParams;
            baseResolveParams.rect = rect;
            baseResolveParams.faction = hostingFaction;
            baseResolveParams.singlePawnLord = LordMaker.MakeNewLord(hostingFaction, new LordJob_DefendTradeFairSW(hostingFaction, baseResolveParams.rect.CenterCell), map, null);
            BaseGen.symbolStack.Push("tradeFairOutpostSW", baseResolveParams);

            BaseGen.Generate();

            MakeTradeCaravans(map);
        }

        public List<Faction> GetAllNonPlayerFriends(Faction faction)
        {
            List<Faction> list = Find.FactionManager.AllFactionsVisible.ToList();
            if (list.Contains(Faction.OfPlayer))
            {
                list.Remove(Faction.OfPlayer);
            }
            if (list.Contains(faction))
            {
                list.Remove(faction);
            }
            if(list.Count == 0)
            {
                return list;
            }
            return (from f in list
                    where faction.GoodwillWith(f) >= 0 && !f.HostileTo(Faction.OfPlayer)
                    select f).ToList();
        }

        private void MakeTradeCaravans(Map map)
        {
            int traderCount = 0;
            IntVec3 spot;
            if (RCellFinder.TryFindRandomSpotJustOutsideColony(CellFinderLoose.RandomCellWith((Predicate<IntVec3>)((IntVec3 c) => GenGrid.Standable(c, map)), map, 1000), map, out spot))
            {
                Faction hostFaction = map.ParentFaction;
                List<Faction> friendlyFactionList = GetAllNonPlayerFriends(hostFaction);

                List<Faction> spawnedFactionsList = new List<Faction>();
                spawnedFactionsList.Add(Faction.OfPlayer);

                if (friendlyFactionList.Count > 0)
                {
                    foreach (Faction faction in friendlyFactionList)
                    {
                        bool flag = true;
                        foreach(Faction spawnedFaction in spawnedFactionsList)
                        {
                            if(faction.RelationKindWith(spawnedFaction) == FactionRelationKind.Hostile)
                            {
                                flag = false;
                                break;
                            }
                        }
                        if (flag)
                        {
                            traderCount++;
                            CreatingTradeCaravan(faction, FindRandomSpot(map), map);
                            spawnedFactionsList.Add(faction);
                        }
                    }
                }
                CreatingTradeCaravan(hostFaction, FindRandomSpot(map), map);
                traderCount++;
                if(traderCount < 3)
                {
                    CreatingTradeCaravan(hostFaction, FindRandomSpot(map), map);
                }
                traderCount++;
                if (traderCount < 3)
                {
                    CreatingTradeCaravan(hostFaction, FindRandomSpot(map), map);
                }
            }
        }

        private IntVec3 FindRandomSpot(Map map)
        {
            IntVec3 spot;
            if (RCellFinder.TryFindRandomSpotJustOutsideColony(CellFinderLoose.RandomCellWith((Predicate<IntVec3>)((IntVec3 c) => GenGrid.Standable(c, map)), map, 1000), map, out spot))
            {
                return spot;
            }
            return new IntVec3();
        }

        private bool CreatingTradeCaravan(Faction faction, IntVec3 spot, Map map)
        {
            IncidentParms val = StorytellerUtility.DefaultParmsNow(IncidentCategoryDefOf.FactionArrival, map);
            val.forced = true;
            val.points = (val.points < 100) ? val.points = 100 : Mathf.Min(800f, val.points);
            val.spawnCenter = spot;
            val.faction = faction;
            List<Pawn> list = SpawnPawns(val);
            if (list.Count == 0)
            {
                return false;
            }
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].needs != null && list[i].needs.food != null)
                {
                    list[i].needs.food.CurLevel = list[i].needs.food.MaxLevel;
                }
            }
            LordJob_TradeWithColony val4 = new LordJob_TradeWithColony(val.faction, spot);
            LordMaker.MakeNewLord(val.faction, val4, map, (IEnumerable<Pawn>)list);
            return true;
        }

        private List<Pawn> SpawnPawns(IncidentParms parms)
        {
            Map val = parms.target as Map;
            PawnGroupMakerParms val2 = IncidentParmsUtility.GetDefaultPawnGroupMakerParms(PawnGroupKindDefOf.Trader, parms, true);
            List<Pawn> list = PawnGroupMakerUtility.GeneratePawns(val2, false).ToList();
            foreach (Pawn item in list)
            {
                IntVec3 val3 = CellFinder.RandomClosewalkCellNear(parms.spawnCenter, val, 5, null);
                GenSpawn.Spawn(item, val3, val);
            }
            return list;
        }
    }

    public class GenStep_HuntingLodgeSW : GenStep_BasicGenSW
    {
        public const int edgeSize = 40;

        public override void Generate(Map map, GenStepParams parms)
        {
            PawnKindDef huntingKind = Find.WorldObjects.WorldObjectAt(map.Tile, WorldObjectsDefsSW.EventSiteSW).GetComponent<WorldObjectComp_AnimalToHuntSW>().animalKindDef;
            base.Generate(map, parms);
            CellRect rect = new CellRect(Rand.RangeInclusive(this.adventureRegion.minX + 10, this.adventureRegion.maxX - (edgeSize +10)), Rand.RangeInclusive(this.adventureRegion.minZ + 10, this.adventureRegion.maxZ - (edgeSize + 10)), edgeSize, edgeSize);
            rect.ClipInsideMap(map);
            //seperate the rect into the herd and lodge half
            CellRect animalRect = new CellRect(rect.minX, rect.minZ, edgeSize, edgeSize/2);
            CellRect lodgeRect = new CellRect(rect.minX, rect.minZ, edgeSize, edgeSize/2);
            if (Rand.Chance(0.5f))
            {
                animalRect = new CellRect(rect.minX, rect.minZ + edgeSize/2, edgeSize, edgeSize/2);
            }
            else
            {
                lodgeRect = new CellRect(rect.minX, rect.minZ + edgeSize/2, edgeSize, edgeSize/2);
            }

            //Spawn hunting lodge
            ResolveParams lodgeResolveParams = this.baseResolveParams;
            lodgeResolveParams.rect = lodgeRect;
            BaseGen.symbolStack.Push("spawnHuntingLodgeSW", lodgeResolveParams);
            
            //Spawn herd of animals
            ResolveParams animalResolveParams = this.baseResolveParams;
            animalResolveParams.rect = animalRect;
            animalResolveParams.singlePawnKindDef = huntingKind;
            BaseGen.symbolStack.Push("spawnHuntingTargetHerdSW", animalResolveParams);

            BaseGen.Generate();
        }
    }
}
