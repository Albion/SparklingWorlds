﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RimWorld;
using Verse;
using UnityEngine;
using System.Diagnostics;
using RimWorld.Planet;
using RimWorld.BaseGen;
using Verse.AI.Group;

namespace SparklingWorlds
{
    public class SymbolResolver_CrashedShipSW : SymbolResolver
    {

        public override void Resolve(ResolveParams rp)
        {
            //Three different Shiptypes are possible. Disintegrated, partially intact and almost intact.
            if (rp.wallStuff == null)
            {
                rp.wallStuff = ThingDefOf.Steel;
            }
            if (rp.floorDef == null)
            {
                rp.floorDef = BaseGenUtility.CorrespondingTerrainDef(rp.wallStuff, true);
            }

            if (Rand.Chance(0.5f)) //is 0.5 usually
            {
                rp.disableHives = true;
            }
            else
            {
                rp.disableHives = false;
            }
            if (Rand.Chance(0.4f)) //Is usually 0.4
            {
                ResolveIntactShip(rp);
            }
            else
            {
                if (Rand.Chance(0.57f)) //Is usually 0.57
                {
                    ResolvePartiallyDamagedShip(rp);
                }
                else
                {
                    ResolveDisintegratedShip(rp);
                }
            }
        }

        public void ResolveIntactShip(ResolveParams rp)
        {
            int firstPartWidth = Rand.RangeInclusive((rp.rect.Width + 6) / 7, (rp.rect.Width + 4) / 5); //this part has a width of 14-20% total
            int secondPartWidth = Rand.RangeInclusive((rp.rect.Width + 2) / 3, (rp.rect.Width * 2 + 4) / 5); //this part has a width of 33-40% total
            int thirdPartWidth = Rand.RangeInclusive((rp.rect.Width + 2) / 3, (rp.rect.Width * 2 + 4) / 5);

            int maxPartHeight = 25;
            if (rp.rect.Height < 25)
            {
                maxPartHeight = rp.rect.Height - 1;
            }
            maxPartHeight /= 2; //divide by 2 and later multiply by two to make sure room height is even
            int firstPartHeight = Rand.RangeInclusive((maxPartHeight + 1) / 2, (maxPartHeight * 2 + 2) / 3) * 2 + 1;
            int secondPartHeight = Rand.RangeInclusive((maxPartHeight * 2 + 2) / 3, maxPartHeight) * 2 + 1;
            int thirdPartHeight = Rand.RangeInclusive((maxPartHeight + 1) / 2, (maxPartHeight * 3 + 3) / 4) * 2 + 1;

            int centerline = rp.rect.minZ + (rp.rect.Height / 2);
            int currentOffset = 0;

            //Spawn first Part
            ResolveParams resolveParams = rp;
            resolveParams.rect = new CellRect(rp.rect.minX, centerline - (firstPartHeight / 2), firstPartWidth, firstPartHeight);
            BaseGen.symbolStack.Push("brokenCrashRoomOpenLeftSW", resolveParams);
            currentOffset += firstPartWidth - 1;

            //Spawn Second Part(s)
            resolveParams.rect = new CellRect(rp.rect.minX + currentOffset, centerline - (secondPartHeight / 2) + 1, secondPartWidth, secondPartHeight / 2);
            BaseGen.symbolStack.Push("lootDangerRoomSW", resolveParams);

            resolveParams.rect = new CellRect(rp.rect.minX + currentOffset, centerline, secondPartWidth, secondPartHeight / 2);
            BaseGen.symbolStack.Push("lootDangerRoomSW", resolveParams);
            currentOffset += secondPartWidth - 1;

            //Spawn Third Part
            resolveParams.rect = new CellRect(rp.rect.minX + currentOffset, centerline - (thirdPartHeight / 2), thirdPartWidth, thirdPartHeight);
            BaseGen.symbolStack.Push("triangularRoomSW", resolveParams); //triangle Room has its own fillCrashRoom call

            int maxHeight = Math.Max(firstPartHeight, (Math.Max(thirdPartHeight, secondPartHeight)));
            resolveParams.rect = new CellRect(0, centerline - ((maxHeight) / 2), rp.rect.minX + currentOffset, maxHeight);
            BaseGen.symbolStack.Push("pathOfDestructionSW", resolveParams);
        }

        public void ResolvePartiallyDamagedShip(ResolveParams rp)
        {

            int firstPartWidth = Rand.RangeInclusive((rp.rect.Width + 3) / 4, (rp.rect.Width + 2) / 3); //every part has a width of 25-33% of the whole rect
            int secondPartWidth = Rand.RangeInclusive((rp.rect.Width + 3) / 4, (rp.rect.Width + 2) / 3);
            int thirdPartWidth = Rand.RangeInclusive((rp.rect.Width + 3) / 4, (rp.rect.Width + 2) / 3);

            int maxPartHeight = 17;
            if (rp.rect.Height < 17)
            {
                maxPartHeight = rp.rect.Height - 1;
            }
            maxPartHeight /= 2; //divide by 2 and later multiply by two +1 to make sure room height is uneven
            int firstPartHeight = Rand.RangeInclusive((maxPartHeight + 1) / 2, (maxPartHeight * 3 + 3) / 4) * 2 + 1;
            int secondPartHeight = Rand.RangeInclusive((maxPartHeight * 2 + 2) / 3, maxPartHeight) * 2 + 1;
            int thirdPartHeight = Rand.RangeInclusive((maxPartHeight + 1) / 2, maxPartHeight) * 2 + 1;

            int centerline = rp.rect.minZ + (rp.rect.Height / 2);
            int currentOffset = 0;

            //Spawn Engines
            ResolveParams resolveParamsEngine = rp;
            resolveParamsEngine.rect = new CellRect(rp.rect.minX - 2, centerline - (firstPartHeight / 2) + 1, 1, 1);
            resolveParamsEngine.thingRot = new Rot4?(new Rot4(1));
            resolveParamsEngine.singleThingDef = ThingDefsSW.DamagedShipEngineSW;
            BaseGen.symbolStack.Push("insertThingSW", resolveParamsEngine);

            resolveParamsEngine.rect = new CellRect(rp.rect.minX - 2, centerline + (firstPartHeight / 2) - 1, 1, 1);
            BaseGen.symbolStack.Push("insertThingSW", resolveParamsEngine);

            ResolveParams resolveParams = rp;

            //Spawn First Part
            resolveParams.rect = new CellRect(rp.rect.minX, centerline - (firstPartHeight / 2), firstPartWidth, firstPartHeight);
            BaseGen.symbolStack.Push("lootDangerRoomSW", resolveParams);
            currentOffset += firstPartWidth - 1;

            //Spawn Second Part(s)
            resolveParams.rect = new CellRect(rp.rect.minX + currentOffset, centerline - (secondPartHeight / 2), secondPartWidth / 2, secondPartHeight);
            BaseGen.symbolStack.Push("brokenCrashRoomOpenRightSW", resolveParams);
            int offSetToSecondBrokenRoom = (secondPartWidth + 1) / 2; //+1 so I always get a rounded up result
            resolveParams.rect = new CellRect(rp.rect.minX + currentOffset + offSetToSecondBrokenRoom, centerline - (secondPartHeight / 2), secondPartWidth / 2, secondPartHeight);
            BaseGen.symbolStack.Push("brokenCrashRoomOpenLeftSW", resolveParams);
            currentOffset += secondPartWidth - 1;

            //Spawn Third Part
            resolveParams.rect = new CellRect(rp.rect.minX + currentOffset, centerline - (thirdPartHeight / 2), thirdPartWidth, thirdPartHeight);
            BaseGen.symbolStack.Push("triangularRoomSW", resolveParams); //triangle Room has its own fillCrashRoom call

            int maxHeight = Math.Max(firstPartHeight, (Math.Max(thirdPartHeight, secondPartHeight)));
            resolveParams.rect = new CellRect(0, centerline - ((maxHeight) / 2), rp.rect.minX + currentOffset, maxHeight);
            BaseGen.symbolStack.Push("pathOfDestructionSW", resolveParams);
        }

        public void ResolveDisintegratedShip(ResolveParams rp)
        {
            ResolveParams resolveParams = rp;

            //fill with slag and rubble
            BaseGen.symbolStack.Push("fillWithDebrisSW", rp);

            resolveParams.singleThingDef = ThingDefOf.ShipChunk;
            int shipChunkCount = Rand.RangeInclusive(4, 6);
            for (int i = 0; i < shipChunkCount; i++)
            {
                BaseGen.symbolStack.Push("thing", resolveParams);
            }

            //Chance for infested ship chunks (set to 0.6)
            if (Rand.Chance(0.6f))
            {
                ResolveParams InfestedResolveParams = rp;
                InfestedResolveParams.singleThingDef = ThingDefsSW.InfestedShipChunkSW;
                InfestedResolveParams.disableHives = false;
                InfestedResolveParams.hivesCount = Rand.RangeInclusive(3, 6);
                BaseGen.symbolStack.Push("spawnHiveSW", InfestedResolveParams);
            }

            //Spawn Engines
            ResolveParams resolveParamsEngine = rp;
            resolveParamsEngine.rect = new CellRect(rp.rect.minX, rp.rect.minZ, rp.rect.Width/2, rp.rect.Height);
            resolveParamsEngine.thingRot = new Rot4?(new Rot4(1));
            resolveParamsEngine.singleThingDef = ThingDefsSW.DamagedShipEngineSW;
            int shipEngineCount = Rand.RangeInclusive(1, 3);
            for (int i = 0; i < shipEngineCount; i++)
            {
                BaseGen.symbolStack.Push("thing", resolveParamsEngine);
            }

            //Spawn Beam
            ResolveParams resolveParamsBeam = rp;
            resolveParamsBeam.rect = new CellRect(rp.rect.minX + rp.rect.Width / 2, rp.rect.minZ, rp.rect.Width / 2, rp.rect.Height);
            resolveParamsBeam.singleThingDef = ThingDefOf.Ship_Beam;
            int shipBeamCount = Rand.RangeInclusive(1, 2);
            for (int i = 0; i < shipBeamCount; i++)
            {
                resolveParamsBeam.thingRot = new Rot4(Rand.RangeInclusive(1, 3));
                BaseGen.symbolStack.Push("thing", resolveParamsBeam);
            }
        }
    }

    public class SymbolResolver_CrashedHiveSW : SymbolResolver
    {

        public override void Resolve(ResolveParams rp)
        {
            //Three different Shiptypes are possible. Disintegrated, partially intact and almost intact.
            if (rp.wallStuff == null)
            {
                rp.wallStuff = ThingDefOf.Steel;
            }
            if (rp.floorDef == null)
            {
                rp.floorDef = BaseGenUtility.CorrespondingTerrainDef(rp.wallStuff, true);
            }

                ResolveHiveShip(rp);
        }

        public void ResolveHiveShip(ResolveParams rp)
        {
            int outerPartWidth = Rand.RangeInclusive((rp.rect.Width + 3) / 4, (rp.rect.Width + 2) / 3); //this part has a width of 25-33% total
            int innerPartWidth = Rand.RangeInclusive((rp.rect.Width + 3) / 4, (rp.rect.Width + 2) / 3); //this part has a width of 25-33% total

            int outerUpDownPartWidth = (rp.rect.Width *2 + 6) / 7; //this part has a width of 28% total

            int maxPartHeight = rp.rect.Height;
            maxPartHeight /= 2; //divide by 2 and later multiply by two to make sure room height is even
            int innerLineHeight = Rand.RangeInclusive((maxPartHeight + 3) / 4, (maxPartHeight + 2) / 3) * 2; //this part has a height of 25-33% total
            int outerLineHeight = Rand.RangeInclusive((maxPartHeight + 3) / 4, (maxPartHeight *3 + 9) / 10) * 2; //this part has a height of 25-30% total

            int totalHeight = outerLineHeight * 2 + innerLineHeight;

            int centerline = rp.rect.minZ + (rp.rect.Height / 2);
            int lineOffset = (outerPartWidth * 2 + innerPartWidth - outerUpDownPartWidth * 2 +1) / 2;

            ResolveParams resolveParams = rp;

            int firstLineMinZ = centerline - (innerLineHeight / 2) - outerLineHeight + 1;
            int secondLineMinZ = centerline - (innerLineHeight / 2);
            int thirdLineMinZ = centerline + (innerLineHeight / 2) -1;

            //Spawn Engines
            ResolveParams resolveParamsEngine = rp;
            resolveParamsEngine.rect = new CellRect(rp.rect.minX - 4 + lineOffset, secondLineMinZ - 2, 1, 1);
            resolveParamsEngine.thingRot = new Rot4?(new Rot4(1));
            resolveParamsEngine.singleThingDef = ThingDefsSW.DamagedShipEngineSW;
            BaseGen.symbolStack.Push("insertThingSW", resolveParamsEngine);

            resolveParamsEngine.rect = new CellRect(rp.rect.minX - 4 + lineOffset, thirdLineMinZ +2, 1, 1);
            BaseGen.symbolStack.Push("insertThingSW", resolveParamsEngine);

            //Center Room has to be pushed first so it is surounded by plasteel
            resolveParams.rect = new CellRect(rp.rect.minX + outerPartWidth -1, secondLineMinZ, innerPartWidth, innerLineHeight);
            BaseGen.symbolStack.Push("centerHiveRoomSW", resolveParams);

            //First line
            //Spawn first Part
            resolveParams.rect = new CellRect(rp.rect.minX + lineOffset -1, firstLineMinZ, outerUpDownPartWidth, outerLineHeight);
            BaseGen.symbolStack.Push("lootHiveRoomSW", resolveParams);

            //Spawn Second Part
            resolveParams.rect = new CellRect(rp.rect.minX + lineOffset + outerUpDownPartWidth -2, firstLineMinZ, outerUpDownPartWidth, outerLineHeight);
            BaseGen.symbolStack.Push("lootHiveRoomSW", resolveParams);

            //Last part
            resolveParams.rect = new CellRect(rp.rect.minX + lineOffset + outerUpDownPartWidth * 2 -3, firstLineMinZ, lineOffset, outerLineHeight);
            BaseGen.symbolStack.Push("brokenCrashRoomOpenRightSW", resolveParams);

            //Second line
            //Spawn first Part
            resolveParams.rect = new CellRect(rp.rect.minX, secondLineMinZ, outerPartWidth, innerLineHeight);
            BaseGen.symbolStack.Push("lootHiveRoomSW", resolveParams);

            //Sawn thrid part
            resolveParams.rect = new CellRect(rp.rect.minX + outerPartWidth + innerPartWidth -2, secondLineMinZ, outerPartWidth, innerLineHeight);
            BaseGen.symbolStack.Push("lootHiveRoomSW", resolveParams);

            //Third Line
            //Spawn first Part
            resolveParams.rect = new CellRect(rp.rect.minX + lineOffset -1, thirdLineMinZ, outerUpDownPartWidth, outerLineHeight);
            BaseGen.symbolStack.Push("lootHiveRoomSW", resolveParams);

            //Spawn Second Part
            resolveParams.rect = new CellRect(rp.rect.minX + lineOffset + outerUpDownPartWidth -2, thirdLineMinZ, outerUpDownPartWidth, outerLineHeight);
            BaseGen.symbolStack.Push("lootHiveRoomSW", resolveParams);

            //Last part
            resolveParams.rect = new CellRect(rp.rect.minX + lineOffset + outerUpDownPartWidth * 2 -3, thirdLineMinZ, lineOffset, outerLineHeight);
            BaseGen.symbolStack.Push("brokenCrashRoomOpenRightSW", resolveParams);

            //Path of destruction
            int maxHeight = outerLineHeight * 2 + innerLineHeight;
            resolveParams.rect = new CellRect(0, centerline - ((maxHeight) / 2) +1, rp.rect.minX + lineOffset + outerUpDownPartWidth, maxHeight -2);
            BaseGen.symbolStack.Push("pathOfDestructionSW", resolveParams);
        }
    }

    public class SymbolResolver_InsertThingSW : SymbolResolver
    {
        public override bool CanResolve(ResolveParams rp)
        {
            return base.CanResolve(rp);
        }

        public override void Resolve(ResolveParams rp)
        {
            ThingDef thingDef = rp.singleThingStuff ?? GenStuff.RandomStuffInexpensiveFor(rp.singleThingDef, rp.faction);
            Rot4? thingRot = rp.thingRot;
            Rot4 rot = (!thingRot.HasValue) ? Rot4.East : thingRot.Value;
            if (rp.singleThingDef.rotatable)
            {
                if (rp.singleThingDef.MadeFromStuff)
                {
                    Thing thing = ThingMaker.MakeThing(rp.singleThingDef, thingDef);
                    GenSpawn.Spawn(thing, rp.rect.RandomCell, BaseGen.globalSettings.map, rot);
                }
                else
                {
                    Thing thing2 = ThingMaker.MakeThing(rp.singleThingDef, null);
                    GenSpawn.Spawn(thing2, rp.rect.RandomCell, BaseGen.globalSettings.map, rot);
                }
            }
            else if (rp.singleThingDef.MadeFromStuff)
            {
                Thing thing3 = ThingMaker.MakeThing(rp.singleThingDef, thingDef);
                GenSpawn.Spawn(thing3, rp.rect.RandomCell, BaseGen.globalSettings.map, rot);
            }
            else
            {
                Thing thing4 = ThingMaker.MakeThing(rp.singleThingDef, null);
                GenSpawn.Spawn(thing4, rp.rect.RandomCell, BaseGen.globalSettings.map, rot);
            }
        }
    }

    public class SymbolResolver_SpawnStockpileSW : SymbolResolver
    {
        public override bool CanResolve(ResolveParams rp)
        {
            return base.CanResolve(rp);
        }

        public override void Resolve(ResolveParams rp)
        {
            if (rp.stockpileConcreteContents != null && (rp.stockpileConcreteContents.Count > 0))
            {
                for (int i = 0; i < rp.stockpileConcreteContents.Count; i++)
                {
                    ResolveParams resolveParamsDebris = rp;
                    resolveParamsDebris.singleThingToSpawn = rp.stockpileConcreteContents[i];
                    BaseGen.symbolStack.Push("thing", resolveParamsDebris);
                }
            }
        }
    }

    public class SymbolResolver_TriangularRoomSW : SymbolResolver
    {
        public override bool CanResolve(ResolveParams rp)
        {
            return base.CanResolve(rp);
        }

        private static void TryToSetFloorTile(IntVec3 c, Map map, TerrainDef floorDef)
        {
            List<Thing> thingList = GridsUtility.GetThingList(c, map);
            for (int i = 0; i < thingList.Count; i++)
            {
                if (!thingList[i].def.destroyable)
                {
                    return;
                }
            }
            for (int j = thingList.Count - 1; j >= 0; j--)
            {
                thingList[j].Destroy(0);
            }
            map.terrainGrid.SetTerrain(c, floorDef);
        }

        public override void Resolve(ResolveParams rp)
        {
            Map map = BaseGen.globalSettings.map;
            if (rp.wallStuff == null)
            {
                rp.wallStuff = BaseGenUtility.RandomCheapWallStuff(Find.FactionManager.FirstFactionOfDef(FactionDefOf.Ancients), false);
            }
            if (rp.floorDef == null)
            {
                rp.floorDef = BaseGenUtility.CorrespondingTerrainDef(rp.wallStuff, true);
            }
            if (rp.floorDef == null)
            {
                rp.floorDef = BaseGenUtility.RandomBasicFloorDef(Faction.OfMechanoids, false);
            }
            ResolveParams resolveParams = rp;
            resolveParams.rect = new CellRect(rp.rect.minX, rp.rect.minZ, 1, rp.rect.Height);
            BaseGen.symbolStack.Push("edgeWalls", resolveParams);

            RoofGrid roofGrid = BaseGen.globalSettings.map.roofGrid;
            RoofDef roofDef = rp.roofDef ?? RoofDefOf.RoofConstructed;

            for (int i = 1; i <= rp.rect.Width; i++)
            {
                int currentRow = rp.rect.minX + i;
                int offset = (int)Math.Floor((double)(((double)rp.rect.Height / (double)((double)rp.rect.Width / (double)i)) / (double)2));
                int offsetRoundUp = (int)Math.Ceiling((double)(((double)rp.rect.Height / (double)((double)rp.rect.Width / (double)i)) / (double)2));
                for (int j = rp.rect.minZ + offset; j < rp.rect.minZ + rp.rect.Height - offset; j++)
                {
                    foreach (Thing current in map.thingGrid.ThingsAt(new IntVec3(currentRow, 1, j)))
                    {
                        current.TakeDamage(new DamageInfo(DamageDefOf.Blunt, 10000));
                    }
                    TryToSetFloorTile(new IntVec3(currentRow, 1, j), map, rp.floorDef);

                    if (j == rp.rect.minZ + offset || j == rp.rect.minZ + offsetRoundUp || j == rp.rect.minZ + rp.rect.Height - offset - 1 || j == rp.rect.minZ + rp.rect.Height - offsetRoundUp - 1)
                    {
                        ResolveParams resolveParams2 = rp;
                        resolveParams2.rect = new CellRect(currentRow, j, 1, 1);
                        BaseGen.symbolStack.Push("edgeWalls", resolveParams2);
                    }

                    IntVec3 intVec = new IntVec3(currentRow, 1, j);
                    if (!roofGrid.Roofed(intVec))
                    {
                        roofGrid.SetRoof(intVec, roofDef);
                    }
                }
            }

            //Fill triangle with stuff
            ResolveParams rpTriangleInterior = rp;
            rpTriangleInterior.rect = new CellRect(rp.rect.minX + 1, rp.rect.minZ + ((rp.rect.Height + 3) / 4), (rp.rect.Width + 1) / 2, (rp.rect.Height + 1) / 2);
            BaseGen.symbolStack.Push("fillCrashRoomSW", rpTriangleInterior);
        }
    }

    public class SymbolResolver_LootDangerRoomSW : SymbolResolver
    {
        public override bool CanResolve(ResolveParams rp)
        {
            return base.CanResolve(rp);
        }

        public override void Resolve(ResolveParams rp)
        {
            ResolveParams resolveParams = rp;
            resolveParams.rect = new CellRect(rp.rect.minX + 1, rp.rect.minZ + 1, rp.rect.Width - 2, rp.rect.Height - 2);
            BaseGen.symbolStack.Push("fillCrashRoomSW", resolveParams);

            BaseGen.symbolStack.Push("emptyRoom", rp);

            BaseGen.symbolStack.Push("clear", rp);
        }
    }

    public class SymbolResolver_LootHiveRoomSW : SymbolResolver
    {
        public override bool CanResolve(ResolveParams rp)
        {
            return base.CanResolve(rp);
        }

        public override void Resolve(ResolveParams rp)
        {
            ResolveParams resolveParams = rp;
            resolveParams.rect = new CellRect(rp.rect.minX + 1, rp.rect.minZ + 1, rp.rect.Width - 2, rp.rect.Height - 2);
            BaseGen.symbolStack.Push("fillHiveCrashRoomSW", resolveParams);

            BaseGen.symbolStack.Push("emptyRoom", rp);

            BaseGen.symbolStack.Push("clear", rp);
        }
    }

    public class SymbolResolver_centerHiveRoomSW : SymbolResolver
    {
        public override bool CanResolve(ResolveParams rp)
        {
            return base.CanResolve(rp);
        }

        public override void Resolve(ResolveParams rp)
        {

            //MechanoidsSpawn
            ResolveParams resolveParams = rp;
            resolveParams.rect = new CellRect(rp.rect.minX + 1, rp.rect.minZ + 1, rp.rect.Width - 2, rp.rect.Height - 2);
            resolveParams.mechanoidsCount = Rand.RangeInclusive(5, 12);
            BaseGen.symbolStack.Push("randomMechanoidGroup", resolveParams);

            //Spawn Core
            ResolveParams resolveParamsCentralCore = rp;
            resolveParamsCentralCore.rect = new CellRect(rp.rect.minX + ((rp.rect.Width +1) / 2) -1, rp.rect.minZ + rp.rect.Height / 2 -1, 1, 1);
            resolveParamsCentralCore.thingRot = new Rot4?(new Rot4(4));
            resolveParamsCentralCore.singleThingDef = ThingDefsSW.MechanoidHiveComputerCoreSW;
            BaseGen.symbolStack.Push("insertThingSW", resolveParamsCentralCore);

            ResolveParams resolveParamsCenterRoom = rp;
            resolveParamsCenterRoom.wallStuff = ThingDefOf.Plasteel;
            resolveParamsCenterRoom.floorDef = BaseGenUtility.CorrespondingTerrainDef(resolveParamsCenterRoom.wallStuff, true);
            BaseGen.symbolStack.Push("emptyRoom", resolveParamsCenterRoom);

            BaseGen.symbolStack.Push("clear", rp);
        }
    }

    public class SymbolResolver_FillWithDebrisSW : SymbolResolver
    {
        public override bool CanResolve(ResolveParams rp)
        {
            return base.CanResolve(rp);
        }

        public override void Resolve(ResolveParams rp)
        {
            ResolveParams resolveParamsDebris = rp;
            resolveParamsDebris.singleThingDef = ThingDefOf.Filth_RubbleBuilding;
            int slagRubbleCount = rp.rect.Width * rp.rect.Height / 3;
            for (int i = 0; i < slagRubbleCount; i++)
            {
                BaseGen.symbolStack.Push("thing", resolveParamsDebris);
            }
            resolveParamsDebris.singleThingDef = ThingDefOf.ChunkSlagSteel;
            int steelChunkCount = (rp.rect.Width * rp.rect.Height + 19) / 20;
            for (int i = 0; i < steelChunkCount; i++)
            {
                BaseGen.symbolStack.Push("thing", resolveParamsDebris);
            }
        }
    }

    public class SymbolResolver_FillCrashRoomSW : SymbolResolver
    {
        public override bool CanResolve(ResolveParams rp)
        {
            return base.CanResolve(rp);
        }

        public override void Resolve(ResolveParams rp)
        {
            //Dangerspawn
            if (Rand.Chance(0.60f))
            {
                if (rp.disableHives != null)
                {
                    if (rp.disableHives == true)
                    {
                        //MechanoidsSpawn
                        ResolveParams resolveParams = rp;
                        resolveParams.mechanoidsCount = Rand.RangeInclusive(2, 4);
                        BaseGen.symbolStack.Push("randomMechanoidGroup", resolveParams);
                    }
                    else
                    {
                        //HiveSpawn
                        ResolveParams resolveParams = rp;
                        resolveParams.hivesCount = 2; //formerly Rand.RangeInclusive(2, 3) or 1 to 3
                        resolveParams.singleThingDef = ThingDefOf.Hive;
                        BaseGen.symbolStack.Push("spawnHiveSW", resolveParams);
                    }
                }
                else
                {
                    Log.Message("There was an issue with the disabledHives value");
                }
            }

            //Lootspawn
            if (Rand.Chance(0.32f))
            {
                //Cryocaskets
                ResolveParams resolveParams = rp;
                fillRoomWithAncientShrines(rp, 8);
            }
            else
            {
                if (Rand.Chance(0.45f))
                {
                    //Scattered Loot
                    ResolveParams resolveParams = rp;
                    resolveParams.stockpileConcreteContents = RewardGeneratorSW.GenerateStockpileReward(0.5f);
                    BaseGen.symbolStack.Push("spawnStockpileSW", resolveParams);
                }
                else
                {
                    if (Rand.Chance(0.42f))
                    {
                        //Scattered Weaponscache
                        ResolveParams resolveParams = rp;
                        resolveParams.stockpileConcreteContents = RewardGeneratorSW.GenerateWeaponsCacheReward(Rand.RangeInclusive(5, 9));
                        BaseGen.symbolStack.Push("spawnStockpileSW", resolveParams);
                    }
                    else
                    {
                        //Scattered Refugees
                        ResolveParams resolveParams = rp;
                        resolveParams.stockpileConcreteContents = RewardGeneratorSW.GenerateRefugees(Rand.RangeInclusive(3, 6));
                        BaseGen.symbolStack.Push("spawnStockpileSW", resolveParams);
                    }
                }
            }
            //fill with slag and rubble
            BaseGen.symbolStack.Push("fillWithDebrisSW", rp);

        }


        public void fillRoomWithAncientShrines(ResolveParams rp, int maxShrineCount = 6, float skipChance = 0.10f)
        {
            int shrineWidth = (rp.rect.Width) / (SymbolResolver_AncientShrinesGroup.StandardAncientShrineSize.x);
            int shrineHeight = (rp.rect.Height) / (SymbolResolver_AncientShrinesGroup.StandardAncientShrineSize.z);
            IntVec3 bottomLeft = rp.rect.BottomLeft;
            PodContentsType? podContentsType = rp.podContentsType;
            if (!podContentsType.HasValue)
            {
                float value = Rand.Value;
                if (value < 0.2f)
                {
                    podContentsType = null;
                }
                else if (value < 0.3f)
                {
                    podContentsType = new PodContentsType?(PodContentsType.Slave);
                }
                else
                {
                    podContentsType = new PodContentsType?(PodContentsType.AncientHostile);
                }
            }
            int? ancientCryptosleepCasketGroupID = rp.ancientCryptosleepCasketGroupID;
            int value2 = (!ancientCryptosleepCasketGroupID.HasValue) ? Find.UniqueIDsManager.GetNextAncientCryptosleepCasketGroupID() : ancientCryptosleepCasketGroupID.Value;
            int num3 = 0;
            for (int i = 0; i < shrineHeight; i++)
            {
                for (int j = 0; j < shrineWidth; j++)
                {
                    if (!Rand.Chance(skipChance))
                    {
                        if (num3 >= maxShrineCount)
                        {
                            break;
                        }
                        CellRect rect = new CellRect(bottomLeft.x + j * (SymbolResolver_AncientShrinesGroup.StandardAncientShrineSize.x), bottomLeft.z + i * (SymbolResolver_AncientShrinesGroup.StandardAncientShrineSize.z), SymbolResolver_AncientShrinesGroup.StandardAncientShrineSize.x, SymbolResolver_AncientShrinesGroup.StandardAncientShrineSize.z);
                        if (rect.FullyContainedWithin(rp.rect))
                        {
                            ResolveParams resolveParams = rp;
                            resolveParams.rect = rect;
                            resolveParams.ancientCryptosleepCasketGroupID = new int?(value2);
                            resolveParams.podContentsType = podContentsType;
                            BaseGen.symbolStack.Push("ancientShrine", resolveParams);
                            num3++;
                        }
                    }
                }
            }
        }
    }

    public class SymbolResolver_FillHiveCrashRoomSW : SymbolResolver
    {
        public override bool CanResolve(ResolveParams rp)
        {
            return base.CanResolve(rp);
        }

        public override void Resolve(ResolveParams rp)
        {
            //Dangerspawn
            if (Rand.Chance(0.72f))
            {
                //MechanoidsSpawn
                ResolveParams resolveParams = rp;
                resolveParams.mechanoidsCount = Rand.RangeInclusive(5, 9);
                BaseGen.symbolStack.Push("randomMechanoidGroup", resolveParams);
            }

            //Lootspawn
            if (Rand.Chance(0.99f))
            {
                //Scattered Loot
                ResolveParams resolveParams = rp;
                resolveParams.stockpileConcreteContents = RewardGeneratorSW.GenerateHiveStockReward(0.28f);
                BaseGen.symbolStack.Push("spawnStockpileSW", resolveParams);
            }
            //fill with slag and rubble
            BaseGen.symbolStack.Push("fillWithDebrisSW", rp);

        }
    }

    public class RewardGeneratorSW
    {
        public static List<Thing> GenerateStockpileReward(float singleRewardChance)
        {
            List<Thing> returnList = new List<Thing>();

            if (Rand.Chance(singleRewardChance))
            {
                ThingDef neurotrainer = NeurotrainerDefGenerator.ImpliedThingDefs().RandomElement<ThingDef>();
                List<ThingDef> potentialList = new List<ThingDef>();
                potentialList.Add(ThingDefOf.TechprofSubpersonaCore);
                potentialList.Add(ThingDefOf.AIPersonaCore);
                potentialList.Add(neurotrainer);
                potentialList.Add(ThingDefOf.PsychicEmanator);
                potentialList.Add(ThingDefOf.VanometricPowerCell);
                potentialList.Add(ThingDefOf.InfiniteChemreactor);
                potentialList.Add(ThingDefsSW.CherubMechInjectorSW);

                ThingDef rewardDef;
                potentialList.TryRandomElement(out rewardDef);
                if (rewardDef != null)
                {
                    if (rewardDef == neurotrainer && Rand.Chance(0.6f))
                    {
                        returnList.Add(ThingMaker.MakeThing(rewardDef));
                    }
                    returnList.Add(ThingMaker.MakeThing(rewardDef));
                }
            }
            else
            {
                if (Rand.Chance(0.4f))
                {
                    //Spawn Artifacts
                    List<ThingDef> potentialList = new List<ThingDef>();
                    potentialList.Add(ThingDefsSW.PsychicAnimalPulser);
                    potentialList.Add(ThingDefsSW.PsychicInsanityLance);
                    potentialList.Add(ThingDefsSW.PsychicShockLance);
                    potentialList.Add(ThingDefsSW.PsychicSoothePulser);

                    int artifactCount = Rand.RangeInclusive(3, 5);
                    ThingDef rewardDef;
                    for (int i = 0; i < artifactCount; i++)
                    {
                        potentialList.TryRandomElement(out rewardDef);
                        if (rewardDef != null)
                        {
                            returnList.Add(ThingMaker.MakeThing(rewardDef));
                        }
                    }
                }
                else
                {
                    if (Rand.Chance(0.25f))
                    {
                        //Spawn adv Components
                        int componentStackCount = Rand.RangeInclusive(2, 3);
                        for (int i = 0; i < componentStackCount; i++)
                        {
                            Thing reward = ThingMaker.MakeThing(ThingDefOf.ComponentSpacer);
                            reward.stackCount = Rand.RangeInclusive(3, 5);
                            returnList.Add(reward);
                        }
                    }
                    else
                    {
                        //Spawn precious materials
                        List<ThingDef> potentialList = new List<ThingDef>();
                        potentialList.Add(ThingDefOf.Plasteel);
                        potentialList.Add(ThingDefOf.Uranium);
                        potentialList.Add(ThingDefOf.Gold);

                        ThingDef rewardDef;
                        potentialList.TryRandomElement(out rewardDef);
                        int StackCount = Rand.RangeInclusive(4, 5);
                        if (rewardDef != null)
                        {
                            for (int i = 0; i < StackCount; i++)
                            {
                                Thing reward = ThingMaker.MakeThing(rewardDef);
                                reward.stackCount = Rand.RangeInclusive(15, 35);
                                returnList.Add(reward);
                            }
                        }
                        else
                        {
                            Log.Error("Could not resolve thingdef to spawn reward.");
                        }
                    }
                }
            }
            return returnList;
        }

        public static List<Thing> GenerateHiveStockReward(float singleRewardChance)
        {
            List<Thing> returnList = new List<Thing>();

            if (Rand.Chance(singleRewardChance))
            {
                List<ThingDef> potentialList = new List<ThingDef>();
                potentialList.Add(ThingDefOf.TechprofSubpersonaCore);
                potentialList.Add(ThingDefOf.VanometricPowerCell);
                potentialList.Add(ThingDefOf.VanometricPowerCell); //second time to increase chance to find this reward
                potentialList.Add(ThingDefOf.InfiniteChemreactor);

                ThingDef rewardDef;
                potentialList.TryRandomElement(out rewardDef);
                if (rewardDef != null)
                {
                    returnList.Add(ThingMaker.MakeThing(rewardDef));
                }
            }
            else
            {
                if (Rand.Chance(0.20f))
                {
                    //Spawn adv Components
                    int componentStackCount = Rand.RangeInclusive(3, 5);
                    for (int i = 0; i < componentStackCount; i++)
                    {
                        Thing reward = ThingMaker.MakeThing(ThingDefOf.ComponentSpacer);
                        reward.stackCount = Rand.RangeInclusive(3, 6);
                        returnList.Add(reward);
                    }
                }
                else
                {
                    //Spawn precious materials
                    List<ThingDef> potentialList = new List<ThingDef>();
                    potentialList.Add(ThingDefOf.Plasteel);
                    potentialList.Add(ThingDefOf.Uranium);
                    potentialList.Add(ThingDefOf.Gold);
                    potentialList.Add(ThingDefOf.Silver);
                    potentialList.Add(ThingDefOf.Steel);

                    ThingDef rewardDef;
                    potentialList.TryRandomElement(out rewardDef);
                    int StackCount = Rand.RangeInclusive(4, 8);
                    if (rewardDef != null)
                    {
                        for (int i = 0; i < StackCount; i++)
                        {
                            Thing reward = ThingMaker.MakeThing(rewardDef);
                            reward.stackCount = Rand.RangeInclusive(15, 35);
                            returnList.Add(reward);
                        }
                        if (rewardDef == ThingDefOf.Silver)
                        {
                            for (int i = 0; i < StackCount; i++)
                            {
                                Thing reward = ThingMaker.MakeThing(rewardDef);
                                reward.stackCount = Rand.RangeInclusive(50, 150);
                                returnList.Add(reward);
                            }
                        }
                        if (rewardDef == ThingDefOf.Steel)
                        {
                            for (int i = 0; i < StackCount; i++)
                            {
                                Thing reward = ThingMaker.MakeThing(rewardDef);
                                reward.stackCount = Rand.RangeInclusive(25, 50);
                                returnList.Add(reward);
                            }
                        }
                    }
                    else
                    {
                        Log.Error("Could not resolve thingdef to spawn reward.");
                    }
                }
            }
            return returnList;
        }

        public static List<Thing> GenerateWeaponsCacheReward(int gunCount)
        {

            List<Thing> returnList = new List<Thing>();

            IEnumerable<ThingDef> weaponList = (from x in ThingSetMakerUtility.allGeneratableItems
                                                where x.weaponTags != null && (x.weaponTags.Contains("SpacerGun") || x.weaponTags.Contains("SniperRifle") || x.weaponTags.Contains("GunHeavy") || x.weaponTags.Contains("IndustrialGunAdvanced"))
                                                select x);
            for (int i = 0; i < gunCount; i++)
            {
                ThingDef thingDef;
                weaponList.TryRandomElement(out thingDef);
                if (thingDef == null)
                {
                    Log.Error("Could not resolve thingdef to spawn weapons");
                    continue;
                }
                Thing weapon = ThingMaker.MakeThing(thingDef);
                CompQuality compQuality = weapon.TryGetComp<CompQuality>();
                if (compQuality != null)
                {
                    compQuality.SetQuality(QualityUtility.GenerateQualityTraderItem(), ArtGenerationContext.Outsider);
                }
                returnList.Add(weapon);
            }

            return returnList;
        }

        public static List<Thing> GenerateRefugees(int refugeeCount)
        {
            List<Thing> refugeeList = new List<Thing>();
            PawnGenerationRequest request = new PawnGenerationRequest(PawnKindDefOf.SpaceRefugee, Faction.OfAncients, PawnGenerationContext.NonPlayer, -1);
            for (int i = 0; i < refugeeCount; i++)
            {
                Pawn pawn = PawnGenerator.GeneratePawn(request);
                refugeeList.Add(pawn);
                HealthUtility.DamageUntilDowned(pawn);
            }
            return refugeeList;
        }
    }

    public class SymbolResolver_SpawnHiveSW : SymbolResolver
    {

        public override bool CanResolve(ResolveParams rp)
        {
            IntVec3 intVec;
            return base.CanResolve(rp) && this.TryFindHivePos(rp.rect, out intVec);
        }

        public override void Resolve(ResolveParams rp)
        {
            IntVec3 loc;
            if (this.TryFindHivePos(rp.rect, out loc))
            {
                if(rp.hivesCount == null)
                {
                    return;
                }
                int hiveCount = rp.hivesCount.Value;
                for (int i = 0; i < hiveCount; i++)
                {
                    this.TryFindHivePos(rp.rect, out loc);
                    Thing thingHive = ThingMaker.MakeThing(rp.singleThingDef, null);
                    thingHive.SetFaction(Faction.OfInsects, null);
                    if (rp.singleThingDef == ThingDefOf.Hive)
                    {
                        Hive hive = (Hive)GenSpawn.Spawn(thingHive, loc, BaseGen.globalSettings.map, WipeMode.Vanish);
                        hive.PawnSpawner.SpawnPawnsUntilPoints(Hive.InitialPawnsPoints);
                    }
                    if (rp.singleThingDef == ThingDefsSW.InfestedShipChunkSW)
                    {
                        HiveChunkSW hive = (HiveChunkSW)GenSpawn.Spawn(thingHive, loc, BaseGen.globalSettings.map);
                        hive.SpawnPawnsUntilPoints(HiveChunkSW.InitialPawnsPoints);
                    }
                }
            }
        }

        private bool TryFindHivePos(CellRect rect, out IntVec3 pos)
        {
            Map map = BaseGen.globalSettings.map;
            return (from mc in rect.Cells
                    where mc.Standable(map)
                    select mc).TryRandomElement(out pos);
        }
    }

    public class SymbolResolver_BrokenCrashRoomOpenLeftSW : SymbolResolver
    {
        public override bool CanResolve(ResolveParams rp)
        {
            return base.CanResolve(rp);
        }

        public override void Resolve(ResolveParams rp)
        {
            if (rp.wallStuff == null)
            {
                rp.wallStuff = BaseGenUtility.RandomCheapWallStuff(Find.FactionManager.FirstFactionOfDef(FactionDefOf.Ancients), false);
            }
            if (rp.floorDef == null)
            {
                rp.floorDef = BaseGenUtility.CorrespondingTerrainDef(rp.wallStuff, true);
            }
            if (rp.floorDef == null)
            {
                rp.floorDef = BaseGenUtility.RandomBasicFloorDef(Faction.OfMechanoids, false);
            }

            int wallOffset = 1;
            if (rp.rect.Width > 3)
            {
                wallOffset = Rand.RangeInclusive(rp.rect.Width / 4, rp.rect.Width * 3 / 4);
            }

            ResolveParams resolveParams = rp;

            resolveParams.rect = new CellRect(rp.rect.maxX, rp.rect.minZ, 1, rp.rect.Height);
            BaseGen.symbolStack.Push("edgeWalls", resolveParams);

            resolveParams.rect = new CellRect(rp.rect.minX + wallOffset, rp.rect.minZ, rp.rect.Width - wallOffset, 1);
            BaseGen.symbolStack.Push("edgeWalls", resolveParams);

            resolveParams.rect = new CellRect(rp.rect.minX, rp.rect.maxZ, rp.rect.Width, 1);
            BaseGen.symbolStack.Push("edgeWalls", resolveParams);

            ResolveParams resolveFloor = rp;
            int floorFillWidth = rp.rect.Width - wallOffset;
            if (floorFillWidth > 2)
            {
                int floorOffset = (floorFillWidth + 2) / 3;

                resolveFloor.rect = new CellRect(rp.rect.minX, rp.rect.minZ, wallOffset, rp.rect.Height);
                resolveFloor.chanceToSkipFloor = 0.7f;
                BaseGen.symbolStack.Push("floor", resolveFloor);

                resolveFloor.rect = new CellRect(rp.rect.minX + wallOffset, rp.rect.minZ, floorOffset, rp.rect.Height);
                resolveFloor.chanceToSkipFloor = 0.4f;
                BaseGen.symbolStack.Push("floor", resolveFloor);

                resolveFloor.rect = new CellRect(rp.rect.minX + wallOffset + floorOffset, rp.rect.minZ, floorFillWidth - floorOffset, rp.rect.Height);
                resolveFloor.chanceToSkipFloor = 0.0f;
                BaseGen.symbolStack.Push("floor", resolveFloor);
            }
            else
            {
                resolveFloor.rect = new CellRect(rp.rect.minX + wallOffset, rp.rect.minZ, floorFillWidth, rp.rect.Height);
                resolveFloor.chanceToSkipFloor = 0.4f;
                BaseGen.symbolStack.Push("floor", resolveFloor);
            }

            //fill with slag and rubble
            BaseGen.symbolStack.Push("fillWithDebrisSW", rp);
        }
    }

    public class SymbolResolver_BrokenCrashRoomOpenRightSW : SymbolResolver
    {
        public override bool CanResolve(ResolveParams rp)
        {
            return base.CanResolve(rp);
        }

        public override void Resolve(ResolveParams rp)
        {
            if (rp.wallStuff == null)
            {
                rp.wallStuff = BaseGenUtility.RandomCheapWallStuff(Find.FactionManager.FirstFactionOfDef(FactionDefOf.Ancients), false);
            }
            if (rp.floorDef == null)
            {
                rp.floorDef = BaseGenUtility.CorrespondingTerrainDef(rp.wallStuff, true);
            }
            if (rp.floorDef == null)
            {
                rp.floorDef = BaseGenUtility.RandomBasicFloorDef(Faction.OfMechanoids, false);
            }

            int wallOffset = 1;
            if (rp.rect.Width > 3)
            {
                wallOffset = Rand.RangeInclusive(rp.rect.Width / 4, rp.rect.Width * 3 / 4);
            }

            ResolveParams resolveParams = rp;

            resolveParams.rect = new CellRect(rp.rect.minX, rp.rect.minZ, 1, rp.rect.Height);
            BaseGen.symbolStack.Push("edgeWalls", resolveParams);

            resolveParams.rect = new CellRect(rp.rect.minX, rp.rect.minZ, rp.rect.Width, 1);
            BaseGen.symbolStack.Push("edgeWalls", resolveParams);

            resolveParams.rect = new CellRect(rp.rect.minX, rp.rect.maxZ, rp.rect.Width - wallOffset, 1);
            BaseGen.symbolStack.Push("edgeWalls", resolveParams);

            ResolveParams resolveFloor = rp;
            int floorFillWidth = rp.rect.Width - wallOffset;
            if (floorFillWidth > 2)
            {
                int floorOffset = floorFillWidth / 3;

                resolveFloor.rect = new CellRect(rp.rect.minX + floorFillWidth, rp.rect.minZ, wallOffset, rp.rect.Height);
                resolveFloor.chanceToSkipFloor = 0.7f;
                BaseGen.symbolStack.Push("floor", resolveFloor);

                resolveFloor.rect = new CellRect(rp.rect.minX + floorFillWidth - floorOffset, rp.rect.minZ, floorOffset, rp.rect.Height);
                resolveFloor.chanceToSkipFloor = 0.4f;
                BaseGen.symbolStack.Push("floor", resolveFloor);

                resolveFloor.rect = new CellRect(rp.rect.minX, rp.rect.minZ, floorFillWidth - floorOffset, rp.rect.Height);
                resolveFloor.chanceToSkipFloor = 0.0f;
                BaseGen.symbolStack.Push("floor", resolveFloor);
            }
            else
            {
                resolveFloor.rect = new CellRect(rp.rect.minX, rp.rect.minZ, floorFillWidth, rp.rect.Height);
                resolveFloor.chanceToSkipFloor = 0.4f;
                BaseGen.symbolStack.Push("floor", resolveFloor);
            }

            //fill with slag and rubble
            BaseGen.symbolStack.Push("fillWithDebrisSW", rp);
        }
    }

    public class SymbolResolver_PathOfDestructionSW : SymbolResolver
    {
        public override bool CanResolve(ResolveParams rp)
        {
            return base.CanResolve(rp);
        }

        public override void Resolve(ResolveParams rp)
        {
            //fill with slag and rubble
            BaseGen.symbolStack.Push("fillWithDebrisSW", rp);

            rp.clearRoof = true;
            BaseGen.symbolStack.Push("clear", rp);
        }
    }

    public class SymbolResolver_SpawnThrumboGroupSW : SymbolResolver
    {
        public override bool CanResolve(ResolveParams rp)
        {
            return base.CanResolve(rp);
        }

        public override void Resolve(ResolveParams rp)
        {
            BaseGen.symbolStack.Push("ensureCanReachMapEdge", rp);

            int thrumboCount = Rand.RangeInclusive(3, 6);

            for (int i = 0; i < thrumboCount; i++)
            {
                ResolveParams resolveParams = rp;
                Pawn thrumboPawn = PawnGenerator.GeneratePawn(PawnKindDefOf.Thrumbo, null);
                resolveParams.singlePawnToSpawn = thrumboPawn;
                BaseGen.symbolStack.Push("pawn", resolveParams);
            }

        }
    }

    public class SymbolResolver_MainRoomPsychicEmitterSW : SymbolResolver
    {

        public override void Resolve(ResolveParams rp)
        {
            // Spawn Generator in Main Room
            ResolveParams resolveEmitter = rp;
            resolveEmitter.rect = new CellRect(rp.rect.minX + 3, rp.rect.maxZ -4, 1, 1);
            resolveEmitter.singleThingDef = ThingDefsSW.PsychicWaveEmitterSW;
            BaseGen.symbolStack.Push("thing", resolveEmitter);

            BaseGen.symbolStack.Push("ensureCanReachMapEdge", resolveEmitter);

            ThingDef thingDef = rp.wallStuff ?? BaseGenUtility.RandomCheapWallStuff(rp.faction, false);
            TerrainDef floorDef = rp.floorDef ?? BaseGenUtility.CorrespondingTerrainDef(thingDef, true);
            if (!rp.noRoof.HasValue || !rp.noRoof.Value)
            {
                BaseGen.symbolStack.Push("roof", rp);
            }
            ResolveParams resolveParams = rp;
            resolveParams.wallStuff = thingDef;
            BaseGen.symbolStack.Push("edgeWalls", resolveParams);
            ResolveParams resolveParams2 = rp;
            resolveParams2.floorDef = floorDef;
            BaseGen.symbolStack.Push("floor", resolveParams2);
            BaseGen.symbolStack.Push("clear", rp);
            if (rp.addRoomCenterToRootsToUnfog.HasValue && rp.addRoomCenterToRootsToUnfog.Value && Current.ProgramState == ProgramState.MapInitializing)
            {
                MapGenerator.rootsToUnfog.Add(rp.rect.CenterCell);
            }
        }
    }

    public class SymbolResolver_PsychicEmitterBaseSW : SymbolResolver
    {

        public override void Resolve(ResolveParams rp)
        {
            int totalWidth = rp.rect.Width;
            int columnWidth = (totalWidth + 2) / 3;
            int widthFirstColumn = Rand.RangeInclusive(columnWidth * 6 / 10, columnWidth);
            int widthSecondColumn = Rand.RangeInclusive(columnWidth * 7 / 10, columnWidth);
            int widthThirdColumn = Rand.RangeInclusive(columnWidth * 6 / 10, columnWidth);

            int totalHeight = rp.rect.Height;

            ResolveParams resolveParams = rp;
            resolveParams.chanceToSkipWallBlock = 0.23f;
            resolveParams.chanceToSkipFloor = 0.3f;
            resolveParams.noRoof = true;

            int zOffset = 0;
            int columnOffset = 0;
            int roomHeight = 2;

            //Second (Main) Column
            zOffset = 0;
            columnOffset = widthFirstColumn - 1;

            roomHeight = Rand.RangeInclusive(totalHeight / 5, totalHeight / 3);
            resolveParams.rect = new CellRect(rp.rect.minX + columnOffset, rp.rect.minZ + zOffset, widthSecondColumn, roomHeight);
            BaseGen.symbolStack.Push("emptyRoom", resolveParams);
            zOffset += roomHeight - 1;

            roomHeight = Rand.RangeInclusive(totalHeight / 4, totalHeight / 3);
            resolveParams.rect = new CellRect(rp.rect.minX + columnOffset, rp.rect.minZ + zOffset, widthSecondColumn, roomHeight);
            BaseGen.symbolStack.Push("mainRoomPsychicEmitter", resolveParams);

            zOffset += roomHeight - 1;

            roomHeight = Rand.RangeInclusive(totalHeight / 5, totalHeight / 3);
            resolveParams.rect = new CellRect(rp.rect.minX + columnOffset, rp.rect.minZ + zOffset, widthSecondColumn, roomHeight);
            BaseGen.symbolStack.Push("emptyRoom", resolveParams);

            //First Column
            zOffset = Rand.RangeInclusive(totalHeight / 12, (totalHeight +5) / 6);

            roomHeight = Rand.RangeInclusive(totalHeight / 5, totalHeight / 3);
            resolveParams.rect = new CellRect(rp.rect.minX, rp.rect.minZ + zOffset, widthFirstColumn, roomHeight);
            BaseGen.symbolStack.Push("emptyRoom", resolveParams);
            zOffset += roomHeight -1;

            roomHeight = Rand.RangeInclusive(totalHeight / 5, totalHeight / 3);
            resolveParams.rect = new CellRect(rp.rect.minX, rp.rect.minZ + zOffset, widthFirstColumn, roomHeight);
            BaseGen.symbolStack.Push("emptyRoom", resolveParams);

            zOffset = 0;

            //Third Column
            columnOffset = widthFirstColumn + widthSecondColumn - 2;
            zOffset = Rand.RangeInclusive(totalHeight / 12, (totalHeight + 5) / 6);

            roomHeight = Rand.RangeInclusive(totalHeight / 5, totalHeight / 3);
            resolveParams.rect = new CellRect(rp.rect.minX + columnOffset, rp.rect.minZ + zOffset, widthThirdColumn, roomHeight);
            BaseGen.symbolStack.Push("emptyRoom", resolveParams);
            zOffset += roomHeight - 1;

            roomHeight = Rand.RangeInclusive(totalHeight / 5, totalHeight / 3);
            resolveParams.rect = new CellRect(rp.rect.minX + columnOffset, rp.rect.minZ + zOffset, widthThirdColumn, roomHeight);
            BaseGen.symbolStack.Push("emptyRoom", resolveParams);

        }
    }

    public class SymbolResolver_HuntingLodgeBaseSW : SymbolResolver
    {
        bool butheringSpawned = false;

        bool barracksSpawned = false;

        bool kitchenSpawned = false;

        public override void Resolve(ResolveParams rp)
        {
            ResolveParams canReachEdgeParams = rp;
            BaseGen.symbolStack.Push("ensureCanReachMapEdge", canReachEdgeParams);

            CellRect rect = rp.rect;
            //Chance to use the right half of the rect
            if (Rand.Chance(0.5f))
            {
                rect = new CellRect(rect.minX + (rect.Width / 2), rect.minZ, rect.Width / 2, rect.Height);
            }
            else
            {
                rect = new CellRect(rect.minX, rect.minZ, rect.Width / 2, rect.Height);
            }

            CellRect rect1;
            CellRect rect2;
            CellRect rect3;

            //seperate the rect into 3 chunks. One of which is double the size of the other ones
            if (Rand.Chance(0.25f))
            {
                rect1 = new CellRect(rect.minX, rect.minZ, rect.Width / 2, rect.Height / 2);
                rect2 = new CellRect(rect.minX + rect.Width / 2, rect.minZ, rect.Width / 2, rect.Height / 2);
                rect3 = new CellRect(rect.minX, rect.minZ + rect.Height / 2, rect.Width, rect.Height / 2);
            }
            else
            {
                if (Rand.Chance(0.333f))
                {
                    rect1 = new CellRect(rect.minX, rect.minZ + rect.Height / 2, rect.Width / 2, rect.Height / 2);
                    rect2 = new CellRect(rect.minX + rect.Width / 2, rect.minZ + rect.Height / 2, rect.Width / 2, rect.Height / 2);
                    rect3 = new CellRect(rect.minX, rect.minZ, rect.Width, rect.Height / 2);
                }
                else
                {
                    if (Rand.Chance(0.5f))
                    {
                        rect1 = new CellRect(rect.minX, rect.minZ, rect.Width / 2, rect.Height);
                        rect2 = new CellRect(rect.minX + rect.Width / 2, rect.minZ + rect.Height / 2, rect.Width / 2, rect.Height / 2);
                        rect3 = new CellRect(rect.minX + rect.Width / 2, rect.minZ, rect.Width / 2, rect.Height / 2);
                    }
                    else
                    {
                        rect1 = new CellRect(rect.minX + rect.Width / 2, rect.minZ, rect.Width / 2, rect.Height);
                        rect2 = new CellRect(rect.minX, rect.minZ + rect.Height / 2, rect.Width / 2, rect.Height / 2);
                        rect3 = new CellRect(rect.minX, rect.minZ, rect.Width / 2, rect.Height / 2);
                    }
                }
            }

            rp.addRoomCenterToRootsToUnfog = true;

            SpawnChildBuilding(rect1, rp);
            SpawnChildBuilding(rect2, rp);
            SpawnChildBuilding(rect3, rp);

            this.butheringSpawned = false;
            this.barracksSpawned = false;
            this.kitchenSpawned = false;
        }

        private void SpawnChildBuilding(CellRect rect, ResolveParams rp)
        {
            ResolveParams resolveParams = rp;

            int buildingWidth;
            int buildingHeight;

            buildingWidth = Rand.RangeInclusive(7, Math.Min(rect.Width - 2, 9));
            buildingHeight = Rand.RangeInclusive(7, Math.Min(rect.Height - 2, 9));

            resolveParams.rect = new CellRect(rect.minX + Rand.RangeInclusive(1, rect.Width - buildingWidth), rect.minZ + Rand.RangeInclusive(1, rect.Height - buildingHeight), buildingWidth, buildingHeight);
            BaseGen.symbolStack.Push(PickNextBuildingSymbol(), resolveParams);
        }

        private string PickNextBuildingSymbol()
        {
            if(Rand.Chance(0.33f) && !this.butheringSpawned)
            {
                this.butheringSpawned = true;
                return "spawnButcherFoodStorageSW";
            }
            else
            {
                if(Rand.Chance(0.5f) && !this.barracksSpawned)
                {
                    this.barracksSpawned = true;
                    return "barracks";
                }
                else
                {
                    if (!this.kitchenSpawned)
                    {
                        this.kitchenSpawned = true;
                        return "kitchenSW";
                    }
                }
            }
            if (!this.barracksSpawned)
            {
                this.barracksSpawned = true;
                return "barracks";
            }
            else
            {
                if (!this.butheringSpawned)
                {
                    this.butheringSpawned = true;
                    return "spawnButcherFoodStorageSW";
                }
            }
            this.kitchenSpawned = true;
            return "kitchenSW";
        }
    }

    public class SymbolResolver_SpawnHuntingTargetHerdSW : SymbolResolver
    {

        public override void Resolve(ResolveParams rp)
        {
            ResolveParams canReachEdgeParams = rp;
            BaseGen.symbolStack.Push("ensureCanReachMapEdge", canReachEdgeParams);

            CellRect rect = rp.rect;
            //Chance to use the right half of the rect
            if (Rand.Chance(0.5f))
            {
                rp.rect = new CellRect(rect.minX + (rect.Width / 2), rect.minZ, rect.Width/2, rect.Height);
            }
            else
            {
                rp.rect = new CellRect(rect.minX, rect.minZ, rect.Width/2, rect.Height);
            }

            int spawnCount;
            PawnKindDef huntingTarget = rp.singlePawnKindDef;

            bool wildGroupFlag = false;
            if (huntingTarget.wildGroupSize != null)
            {
                if(huntingTarget.wildGroupSize.RandomInRange > 3)
                {
                    wildGroupFlag = true;
                }
            }
            if (wildGroupFlag)
            {
                spawnCount = (huntingTarget.wildGroupSize.RandomInRange * 6 +4) / 5; //times 1.2 to make herd sizes larger
            }
            else
            {
                if(huntingTarget.combatPower > 102)
                {
                    spawnCount = Rand.RangeInclusive(6, 8);
                }
                else
                {
                    spawnCount = Rand.RangeInclusive(4, 5);
                }
                spawnCount = (int)((float)spawnCount * (float)(210f / huntingTarget.combatPower));
            }

            for (int i = 0; i < spawnCount; i++)
            {
                ResolveParams resolveParams = rp;
                Pawn pawnToSpawn = PawnGenerator.GeneratePawn(huntingTarget, null);
                resolveParams.singlePawnToSpawn = pawnToSpawn;
                BaseGen.symbolStack.Push("pawn", resolveParams);
            }
        }
    }

    public class SymbolResolver_SpawnStovesSW : SymbolResolver
    {

        public override void Resolve(ResolveParams rp)
        {
            ResolveParams woodPile = rp;
            Thing reward = ThingMaker.MakeThing(ThingDefOf.WoodLog);
            reward.stackCount = Rand.RangeInclusive(50, 75);
            woodPile.singleThingToSpawn = reward;
            BaseGen.symbolStack.Push("thing", woodPile);

            int height = rp.rect.Height;
            int width = rp.rect.Width;
            int minX = rp.rect.minX;
            int maxX = rp.rect.maxX;
            int minZ = rp.rect.minZ;
            int maxZ = rp.rect.maxZ;

            bool stoveSpawned = false;

            if (Rand.Chance(0.2f)) //South stove
            {
                ResolveParams stoveParams = rp;
                stoveParams.rect = new CellRect(minX + Rand.RangeInclusive(2, width - 2), maxZ, 1, 1);
                stoveParams.thingRot = new Rot4?(Rot4.North); //North = 0, East = 1, South = 2, West = 3
                stoveParams.singleThingDef = ThingDefsSW.FueledStove;
                BaseGen.symbolStack.Push("insertThingSW", stoveParams);
                stoveSpawned = true;
            }
            if (Rand.Chance(0.2f)) //East stove
            {
                ResolveParams stoveParams = rp;
                stoveParams.rect = new CellRect(maxX, minZ + Rand.RangeInclusive(1, height - 2), 1, 1);
                stoveParams.thingRot = new Rot4?(Rot4.East); //North = 0, East = 1, South = 2, West = 3
                stoveParams.singleThingDef = ThingDefsSW.FueledStove;
                BaseGen.symbolStack.Push("insertThingSW", stoveParams);
                stoveSpawned = true;
            }
            if (Rand.Chance(0.2f)) //North stove
            {
                ResolveParams stoveParams = rp;
                stoveParams.rect = new CellRect(minX + Rand.RangeInclusive(1,width - 2), minZ, 1, 1);
                stoveParams.thingRot = new Rot4?(Rot4.South); //North = 0, East = 1, South = 2, West = 3
                stoveParams.singleThingDef = ThingDefsSW.FueledStove;
                BaseGen.symbolStack.Push("insertThingSW", stoveParams);
                stoveSpawned = true;
            }
            if (Rand.Chance(0.2f) || !stoveSpawned) //if no stove spawned yet this one will
            {
                ResolveParams stoveParams = rp;
                stoveParams.rect = new CellRect(minX, minZ + Rand.RangeInclusive(2, height - 2), 1, 1);
                stoveParams.thingRot = new Rot4?(Rot4.West); //North = 0, East = 1, South = 2, West = 3
                stoveParams.singleThingDef = ThingDefsSW.FueledStove;
                BaseGen.symbolStack.Push("insertThingSW", stoveParams);
            }
            
        }
    }

    public class SymbolResolver_InteriorButcherFoodStorageSW : SymbolResolver
    {

        public override void Resolve(ResolveParams rp)
        {
            ResolveParams butcherParams = rp;
            butcherParams.rect = new CellRect(rp.rect.minX + 1, rp.rect.minZ+1, rp.rect.Width - 2, rp.rect.Height-2);
            butcherParams.thingRot = new Rot4?(Rot4.South);
            butcherParams.singleThingDef = ThingDefsSW.TableButcher;
            BaseGen.symbolStack.Push("insertThingSW", butcherParams);

            ResolveParams veggyPileParams = rp;
            int stackCount = Rand.RangeInclusive(2, 8);
            for(int i = 0; i < stackCount; i++)
            {
                Thing reward = ThingMaker.MakeThing(ThingDefOf.RawPotatoes);
                reward.stackCount = Rand.RangeInclusive(30, 60);
                veggyPileParams.singleThingToSpawn = reward;
                BaseGen.symbolStack.Push("thing", veggyPileParams);
            }
        }
    }

    public class SymbolResolver_TradeFairOutpostSW : SymbolResolver
    {
        private static readonly FloatRange DefaultPawnsPoints = new FloatRange(1150f, 1600f);

        public override void Resolve(ResolveParams rp)
        {
            //Most of this is simply copied from the vanilla factionBase resolver
            Map map = BaseGen.globalSettings.map;
            Faction faction = rp.faction ?? Find.FactionManager.RandomEnemyFaction(false, false, true, TechLevel.Undefined);
            
            float num2 = (float)rp.rect.Area / 144f * 0.17f;
            BaseGen.globalSettings.minEmptyNodes = ((!(num2 < 1f)) ? GenMath.RoundRandom(num2) : 0);
            Lord singlePawnLord = rp.singlePawnLord ?? LordMaker.MakeNewLord(faction, new LordJob_DefendTradeFairSW(faction, rp.rect.CenterCell), map, null);
            TraverseParms traverseParms = TraverseParms.For(TraverseMode.PassDoors, Danger.Deadly, false);
            ResolveParams resolveParams = rp;
            resolveParams.rect = rp.rect;
            resolveParams.faction = faction;
            resolveParams.singlePawnLord = singlePawnLord;
            resolveParams.pawnGroupKindDef = (rp.pawnGroupKindDef ?? PawnGroupKindDefOf.Settlement);
            resolveParams.singlePawnSpawnCellExtraPredicate = (rp.singlePawnSpawnCellExtraPredicate ?? ((Predicate<IntVec3>)((IntVec3 x) => map.reachability.CanReachMapEdge(x, traverseParms))));
            if (resolveParams.pawnGroupMakerParams == null)
            {
                resolveParams.pawnGroupMakerParams = new PawnGroupMakerParms();
                resolveParams.pawnGroupMakerParams.tile = map.Tile;
                resolveParams.pawnGroupMakerParams.faction = faction;
                PawnGroupMakerParms arg_20E_0 = resolveParams.pawnGroupMakerParams;
                float? settlementPawnGroupPoints = rp.settlementPawnGroupPoints;
                arg_20E_0.points = ((!settlementPawnGroupPoints.HasValue) ? SymbolResolver_TradeFairOutpostSW.DefaultPawnsPoints.RandomInRange : settlementPawnGroupPoints.Value);
                resolveParams.pawnGroupMakerParams.inhabitants = true;
                resolveParams.pawnGroupMakerParams.seed = rp.settlementPawnGroupSeed;
            }
            BaseGen.symbolStack.Push("pawnGroup", resolveParams);

            ResolveParams resolveParams4 = rp;
            resolveParams4.faction = faction;
            BaseGen.symbolStack.Push("ensureCanReachMapEdge", resolveParams4);
            ResolveParams resolveParams5 = rp;
            resolveParams5.faction = faction;
            BaseGen.symbolStack.Push("tradeFairBaseSplitSW", resolveParams5);
        }
    }

    public class SymbolResolver_TradeFairBaseSplitSW : SymbolResolver
    {

        public override void Resolve(ResolveParams rp)
        {

            BaseGen.symbolStack.Push("ensureCanReachMapEdge", rp);
            //seperate into 4 sections
            CellRect rect1 = new CellRect(rp.rect.minX, rp.rect.minZ, rp.rect.Width / 2, rp.rect.Height / 2);
            CellRect rect2 = new CellRect(rp.rect.minX, rp.rect.minZ + rp.rect.Height / 2, rp.rect.Width / 2, rp.rect.Height / 2);
            CellRect rect3 = new CellRect(rp.rect.minX + rp.rect.Width / 2, rp.rect.minZ, rp.rect.Width / 2, rp.rect.Height / 2);
            CellRect rect4 = new CellRect(rp.rect.minX + rp.rect.Width / 2, rp.rect.minZ + rp.rect.Height / 2, rp.rect.Width / 2, rp.rect.Height / 2);

            PushSymbolWithRect(rect1, rp);
            PushSymbolWithRect(rect2, rp);
            PushSymbolWithRect(rect3, rp);
            PushSymbolWithRect(rect4, rp);
        }

        private void PushSymbolWithRect(CellRect rect, ResolveParams rp)
        {
            ResolveParams resolveParams = rp;
            int buildingWidth;
            int buildingHeight;

            buildingHeight = Rand.RangeInclusive(5, Math.Min(rect.Height - 2, 9));
            buildingWidth = Rand.RangeInclusive(5, Math.Min(rect.Width - 2, 9));

            resolveParams.rect = new CellRect(rect.minX + Rand.RangeInclusive(0, rect.Width - buildingWidth), rect.minZ + Rand.RangeInclusive(0, rect.Height - buildingHeight), buildingWidth, buildingHeight);
            BaseGen.symbolStack.Push("tradeFairLeafSW", resolveParams);
        }

    }
}
