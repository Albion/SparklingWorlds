﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RimWorld;
using Verse;
using Verse.AI;

namespace SparklingWorlds
{
    public class LazarusMechanitesCompSW : HediffWithComps
    {
        public int ticksTillNextHeal = 0;

        public bool startedHealingProcess = false;

        public override void Tick()
        {
            base.Tick();

            Pawn parentPawn = this.pawn;

            if (parentPawn.health.Downed)
            {
                if (ticksTillNextHeal < Find.TickManager.TicksGame)
                {
                    bool foundSomethingToHeal = false;
                    List<Hediff> hediffsList = parentPawn.health.hediffSet.hediffs;
                    foreach (Hediff hediff in hediffsList)
                    {
                        Hediff_Injury hediff_Injury = hediff as Hediff_Injury;
                        if (hediff_Injury != null && hediff_Injury.CanHealNaturally())
                        {
                            hediff_Injury.Heal(0.06f);
                            Severity -= 0.002f;
                            base.pawn.health.Notify_HediffChanged(this);
                            foundSomethingToHeal = true;
                        }
                    }
                    if (!startedHealingProcess)
                    {
                        if (foundSomethingToHeal)
                        {
                            startedHealingProcess = true;
                            Messages.Message("LazarusActivationMessageSW".Translate(new object[] { parentPawn.LabelShort }), parentPawn, MessageTypeDefOf.PositiveEvent);
                        }
                    }
                    ticksTillNextHeal = Find.TickManager.TicksGame + 12;
                }
            }
            else
            {
                startedHealingProcess = false;
            }
        }

    }

    [DefOf]
    public static class ReconstructorJobDefsSW
    {
        public static JobDef DNAReconstructorSW;
    }

    public class CompTargetEffect_ReconstructSW : CompTargetEffect
    {
        public override void DoEffectOn(Pawn user, Thing target)
        {
            if (!user.IsColonistPlayerControlled)
            {
                return;
            }
            if (!user.CanReserveAndReach(target, PathEndMode.Touch, Danger.Deadly, 1, -1, null, false))
            {
                return;
            }
            Job job = new Job(ReconstructorJobDefsSW.DNAReconstructorSW, target, this.parent);
            job.count = 1;
            user.jobs.TryTakeOrderedJob(job, JobTag.Misc);
        }
    }

    public class JobDriver_ReconstructorSW : JobDriver
    {
        private Corpse Corpse
        {
            get
            {
                return (Corpse)this.job.GetTarget(TargetIndex.A).Thing;
            }
        }

        private Thing Item
        {
            get
            {
                return this.job.GetTarget(TargetIndex.B).Thing;
            }
        }

        public override bool TryMakePreToilReservations(bool errorOnFailed)
        {
            Pawn pawn = this.pawn;
            LocalTargetInfo target = this.Corpse;
            Job job = this.job;
            bool result;
            if (pawn.Reserve(target, job, 1, -1, null, errorOnFailed))
            {
                pawn = this.pawn;
                target = this.Item;
                job = this.job;
                result = pawn.Reserve(target, job, 1, -1, null, errorOnFailed);
            }
            else
            {
                result = false;
            }
            return result;
        }

        protected override IEnumerable<Toil> MakeNewToils()
        {
            yield return Toils_Goto.GotoThing(TargetIndex.B, PathEndMode.Touch).FailOnDespawnedOrNull(TargetIndex.B).FailOnDespawnedOrNull(TargetIndex.A);
            yield return Toils_Haul.StartCarryThing(TargetIndex.B, false, false, false);
            yield return Toils_Goto.GotoThing(TargetIndex.A, PathEndMode.Touch).FailOnDespawnedOrNull(TargetIndex.A);
            Toil prepare = Toils_General.Wait(DurationTicks, TargetIndex.None);
            prepare.WithProgressBarToilDelay(TargetIndex.A, false, -0.5f);
            prepare.FailOnDespawnedOrNull(TargetIndex.A);
            prepare.FailOnCannotTouch(TargetIndex.A, PathEndMode.Touch);
            yield return prepare;
            yield return Toils_General.Do(new Action(this.Reconstruct));
            yield break;
        }

        private void Reconstruct()
        {
            Pawn innerPawn = this.Corpse.InnerPawn;
            CompRottable rotComp = Corpse.GetComp<CompRottable>();
            rotComp.RotProgress = 0f;
            Messages.Message("MessageCorpseReconstructed".Translate(innerPawn).CapitalizeFirst(), innerPawn, MessageTypeDefOf.PositiveEvent, true);
            this.Item.SplitOff(1).Destroy(DestroyMode.Vanish);
        }

        private const TargetIndex CorpseInd = TargetIndex.A;

        private const TargetIndex ItemInd = TargetIndex.B;

        private const int DurationTicks = 600;
    }

}
