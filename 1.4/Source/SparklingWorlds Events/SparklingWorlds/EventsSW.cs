﻿using System;
using System.Collections.Generic;
using System.Linq;
using RimWorld;
using Verse;
using System.Diagnostics;
using RimWorld.Planet;
using Verse.AI.Group;
using Verse.Sound;
using UnityEngine;

namespace SparklingWorlds
{

    public class IncidentWorker_ShipCrashSW : IncidentWorker
    {
        private static readonly IntRange TimeoutDaysRange = new IntRange(7, 11);

        protected override bool CanFireNowSub(IncidentParms parms)
        {
            int tile;
            return base.CanFireNowSub(parms) && TileFinder.TryFindNewSiteTile(out tile, 5, 12, false, tileFinderMode: TileFinderMode.Random ,exitOnFirstTileFound: false) && LoadedModManager.GetMod<SparklingWorlds>().GetSettings<SparklingWorldsSettings>().shipCrash;
        }

        public static Site CreateShipCrashSiteSW(int tile, int days)
        {
            List<SitePartDef> siteParts = new List<SitePartDef>();
            siteParts.Add(SiteDefsSW.ShipCrashCoreSW);
            if (Rand.Chance(0.23f))
            {
                siteParts.Add(SiteDefsSW.RaidOnArrivalSW);
            }
            Site site = SiteMaker.MakeSite(siteParts, tile, Faction.OfAncientsHostile);
            site.GetComponent<TimeoutComp>().StartTimeout(days * 60000);
            Find.WorldObjects.Add(site);
            return site;
        }

        public static Site CreateHiveShipCrashSiteSW(int tile, int days)
        {
            Site site = SiteMaker.MakeSite(SiteDefsSW.HiveCrashCoreSW, tile: tile, faction: Faction.OfMechanoids);
            site.GetComponent<TimeoutComp>().StartTimeout(days * 60000);
            Find.WorldObjects.Add(site);
            return site;
        }

        protected override bool TryExecuteWorker(IncidentParms parms)
        {
            int tile;
            if (!TileFinder.TryFindNewSiteTile(out tile, 5, 12, false, TileFinderMode.Random ,exitOnFirstTileFound: false))
            {
                return false;
            }
            if(Rand.Chance(0.16f) && CommsConsoleUtility.PlayerHasPoweredCommsConsole())
            {
                Site site = CreateHiveShipCrashSiteSW(tile, TimeoutDaysRange.RandomInRange + 3);
                Find.LetterStack.ReceiveLetter("LetterLabelCrashedHiveShipSW".Translate(), "LetterCrashedHiveShipSW".Translate(), LetterDefOf.PositiveEvent, site, null);
            }
            else
            {
                Site site = CreateShipCrashSiteSW(tile, TimeoutDaysRange.RandomInRange);
                if (CommsConsoleUtility.PlayerHasPoweredCommsConsole())
                {
                    Find.LetterStack.ReceiveLetter("LetterLabelCrashedShipSW".Translate(), "LetterCrashedShipSW".Translate(), LetterDefOf.PositiveEvent, site, null);
                }
                else
                {
                    Find.LetterStack.ReceiveLetter(this.def.letterLabel, this.def.letterText, LetterDefOf.PositiveEvent, site, null);
                }
            }
            return true;
        }
    }

    public class IncidentWorker_ThrumboSightingSW : IncidentWorker
    {
        private static readonly IntRange TimeoutDaysRange = new IntRange(4, 6);

        protected override bool CanFireNowSub(IncidentParms parms)
        {
            int tile;
            return base.CanFireNowSub(parms) && TileFinder.TryFindNewSiteTile(out tile, 3, 7, false, TileFinderMode.Random ,exitOnFirstTileFound: false) && LoadedModManager.GetMod<SparklingWorlds>().GetSettings<SparklingWorldsSettings>().thrumboSighting;
        }

        public static Site CreateThrumboSightingSiteSW(int tile, int days)
        {
            List<SitePartDef> siteParts = new List<SitePartDef>();
            siteParts.Add(SiteDefsSW.ThrumboSightingCoreSW);
            if (Rand.Chance(0.23f))
            {
                siteParts.Add(SiteDefsSW.HuntingPartyArrivalSW);
            }
            Site site = SiteMaker.MakeSite(siteParts, tile, Faction.OfAncientsHostile);

            site.GetComponent<TimeoutComp>().StartTimeout(days * 60000);
            Find.WorldObjects.Add(site);
            return site;
        }

        protected override bool TryExecuteWorker(IncidentParms parms)
        {
            int tile;
            if (!TileFinder.TryFindNewSiteTile(out tile, 3, 7, false, TileFinderMode.Random ,exitOnFirstTileFound: false))
            {
                return false;
            }
            Site site = CreateThrumboSightingSiteSW(tile, TimeoutDaysRange.RandomInRange);
            Find.LetterStack.ReceiveLetter(this.def.letterLabel, this.def.letterText, LetterDefOf.PositiveEvent, site, null);
            return true;
        }
    }

    public class SitePartWorker_RaidOnArrivalSW : SitePartWorker
    {
        public override void PostMapGenerate(Map map)
        {
            IncidentParms incidentParms = StorytellerUtility.DefaultParmsNow(IncidentCategoryDefOf.ThreatBig, map);
            incidentParms.forced = true;
            IntVec3 spawnCenter;
            if (RCellFinder.TryFindRandomPawnEntryCell(out spawnCenter, map, 0f, false, (IntVec3 v) => GenGrid.Standable(v, map)))
            {
                incidentParms.spawnCenter = spawnCenter;
            }
            Faction faction;
            if (GenCollection.TryRandomElement<Faction>(from f in Find.FactionManager.AllFactions
                                                        where !f.def.hidden && FactionUtility.HostileTo(f, Faction.OfPlayer)
                                                        select f, out faction))
            {
                if (CellFinder.TryFindRandomEdgeCellWith((IntVec3 c) => map.reachability.CanReachColony(c), map, CellFinder.EdgeRoadChance_Neutral, out spawnCenter))
                {
                    incidentParms.faction = faction;
                    incidentParms.raidStrategy = RaidStrategyDefOf.ImmediateAttack;
                    incidentParms.raidArrivalMode = PawnsArrivalModeDefOf.EdgeWalkIn;
                    incidentParms.spawnCenter = spawnCenter;
                    incidentParms.points = Rand.Range(500f, 800f);
                    QueuedIncident queuedIncident = new QueuedIncident(new FiringIncident(IncidentDefsSW.RaidOnArrivalSW, null, incidentParms), Find.TickManager.TicksGame + Rand.RangeInclusive(3000, 15000));
                    Find.Storyteller.incidentQueue.Add(queuedIncident);
                }
            }
        }
    }

    public class IncidentWorker_RaidOnArrivalSW : IncidentWorker_RaidEnemy
    {
        protected override bool TryExecuteWorker(IncidentParms parms)
        {
            try
            {
                //The following part is to check if the map still exists. If it doesn't the function should break because it can't trigger anymore
                Map map = (Map)parms.target;
                Region region = parms.spawnCenter.GetRegion(map, RegionType.Set_Passable);
                IEnumerable<IntVec3> source = region.Cells;
                try
                {
                    source.Any();
                }
                catch
                {
                    Log.Warning("Site map doesn't exists anymore. Exception caught.");
                    return false;
                }

                if (!base.TryExecuteWorker(parms))
                {
                    return false;
                }
                Find.TickManager.slower.SignalForceNormalSpeedShort();
                return true;
            }
            catch
            {
                return false;
            }
        }

        protected override string GetLetterLabel(IncidentParms parms)
        {
            return "LetterLabelSalvageTeamSW".Translate();
        }
    }

    public class SitePartWorker_HuntingPartyArrivalSW : SitePartWorker
    {
        public override void PostMapGenerate(Map map)
        {
            IncidentParms incidentParms = StorytellerUtility.DefaultParmsNow(IncidentCategoryDefOf.ThreatBig, map);
            incidentParms.forced = true;
            IntVec3 spawnCenter;
            if (RCellFinder.TryFindRandomPawnEntryCell(out spawnCenter, map, 0f, false, (IntVec3 v) => GenGrid.Standable(v, map)))
            {
                incidentParms.spawnCenter = spawnCenter;
            }
            Faction faction;
            if (GenCollection.TryRandomElement<Faction>(from f in Find.FactionManager.AllFactions
                                                        where !f.def.hidden && FactionUtility.HostileTo(f, Faction.OfPlayer)
                                                        select f, out faction))
            {
                if (CellFinder.TryFindRandomEdgeCellWith((IntVec3 c) => map.reachability.CanReachColony(c), map, CellFinder.EdgeRoadChance_Neutral, out spawnCenter))
                {
                    incidentParms.faction = faction;
                    incidentParms.raidStrategy = RaidStrategyDefOf.ImmediateAttack;
                    incidentParms.raidArrivalMode = PawnsArrivalModeDefOf.EdgeWalkIn;
                    incidentParms.spawnCenter = spawnCenter;
                    incidentParms.points = Rand.Range(500f, 1000f);
                    QueuedIncident queuedIncident = new QueuedIncident(new FiringIncident(IncidentDefsSW.HuntingPartyArrivalSW, null, incidentParms), Find.TickManager.TicksGame + Rand.RangeInclusive(3000, 18000));
                    Find.Storyteller.incidentQueue.Add(queuedIncident);
                }
            }
        }
    }

    public class IncidentWorker_HuntingPartyArrivalSW : IncidentWorker_RaidEnemy
    {
        protected override bool TryExecuteWorker(IncidentParms parms)
        {
            try
            {
                //The following part is to check if the map still exists. If it doesn't the function should break because it can't trigger anymore
                Map map = (Map)parms.target;
                Region region = parms.spawnCenter.GetRegion(map, RegionType.Set_Passable);
                IEnumerable<IntVec3> source = region.Cells;
                try
                {
                    source.Any();
                }
                catch
                {
                    Log.Warning("Site map doesn't exists anymore. Exception caught.");
                    return false;
                }

                if (!base.TryExecuteWorker(parms))
                {
                    return false;
                }
                Find.TickManager.slower.SignalForceNormalSpeedShort();
                return true;
            }
            catch
            {
                return false;
            }
        }

        protected override string GetLetterLabel(IncidentParms parms)
        {
            return "LetterLabelHuntingPartySW".Translate();
        }
    }

    public class IncidentWorker_DoctorRequestSW : IncidentWorker
    {
        private const int MinDistance = 5;

        private const int MaxDistance = 15;

        private const int TimeoutDays = 12;

        protected override bool CanFireNowSub(IncidentParms parms)
        {
            Faction faction;
            int num;
            return base.CanFireNowSub(parms) && this.TryFindFaction(out faction) && this.TryFindTile(out num) && LoadedModManager.GetMod<SparklingWorlds>().GetSettings<SparklingWorldsSettings>().doctorRequest;
        }

        protected override bool TryExecuteWorker(IncidentParms parms)
        {
            Faction faction;
            if (!this.TryFindFaction(out faction))
            {
                return false;
            }
            int tile;
            if (!this.TryFindTile(out tile))
            {
                return false;
            }
            WorldObject_DoctorRequestSW doctorRequest = (WorldObject_DoctorRequestSW)WorldObjectMaker.MakeWorldObject(WorldObjectsDefsSW.DoctorRequestSW);
            doctorRequest.Tile = tile;
            doctorRequest.SetFaction(faction);
            doctorRequest.GetComponent<TimeoutComp>().StartTimeout(TimeoutDays * 60000);
            Find.WorldObjects.Add(doctorRequest);
            string text = string.Format(this.def.letterText.AdjustedFor(faction.leader, "PAWN"), faction.def.leaderTitle, faction.Name, TimeoutDays).CapitalizeFirst();
            Find.LetterStack.ReceiveLetter(this.def.letterLabel, text, this.def.letterDef, doctorRequest, null);
            return true;
        }

        private bool TryFindFaction(out Faction faction)
        {
            return (from x in Find.FactionManager.AllFactions
                    where !x.def.hidden && !x.def.permanentEnemy && !x.IsPlayer && !x.defeated && !SettlementUtility.IsPlayerAttackingAnySettlementOf(x)
                    select x).TryRandomElement(out faction);
        }

        private bool TryFindTile(out int tile)
        {
            return TileFinder.TryFindNewSiteTile(out tile, MinDistance, MaxDistance, false, TileFinderMode.Random ,exitOnFirstTileFound: false);
        }

    }

    public class WorldObject_DoctorRequestSW : WorldObject
    {
        private Material cachedMat;

        private static FloatRange RewardtotalMarketValueRange = new FloatRange(900f, 2000f);

        private static readonly SimpleCurve BadOutcomeFactorAtMedicalTendQuality = new SimpleCurve
        {
            {
                new CurvePoint(0.1f, 4f),
                true
            },
            {
                new CurvePoint(1.1f, 0.85f),
                true
            },
            {
                new CurvePoint(1.6f, 0.15f),
                true
            }
        };

        private const float BaseWeight_Disaster = 0.08f;

        private const float BaseWeight_Backfire = 0.15f;

        private const float BaseWeight_TalksFlounder = 0.3f;

        private const float BaseWeight_Success = 0.50f;

        private const float BaseWeight_Triumph = 0.15f;

        private static readonly IntRange DisasterFactionRelationOffset = new IntRange(-50, -25);

        private static readonly IntRange BackfireFactionRelationOffset = new IntRange(-25, -10);

        private static readonly IntRange SuccessFactionRelationOffset = new IntRange(10, 30);

        private static readonly IntRange TriumphFactionRelationOffset = new IntRange(25, 40);

        private const float MedicalXPGainAmount = 6000f;

        private const float SocialXPGainAmount = 2000f;

        private static List<Pair<Action, float>> tmpPossibleOutcomes = new List<Pair<Action, float>>();

        public override Material Material
        {
            get
            {
                if (this.cachedMat == null)
                {
                    Color color;
                    if (base.Faction != null)
                    {
                        color = base.Faction.Color;
                    }
                    else
                    {
                        color = Color.white;
                    }
                    this.cachedMat = MaterialPool.MatFrom(this.def.texture, ShaderDatabase.WorldOverlayTransparentLit, color, WorldMaterials.WorldObjectRenderQueue);
                }
                return this.cachedMat;
            }
        }

        public void Notify_CaravanArrived(Caravan caravan)
        {
            Pawn pawn = BestCaravanPawnUtility.FindPawnWithBestStat(caravan, StatDefOf.MedicalTendQuality);
            if (pawn == null)
            {
                Messages.Message("MessagePeaceTalksNoDiplomat".Translate(), caravan, MessageTypeDefOf.NegativeEvent);
                return;
            }
            float badOutcomeWeightFactor = WorldObject_DoctorRequestSW.GetBadOutcomeWeightFactor(pawn);
            float num = 1f / badOutcomeWeightFactor;
            WorldObject_DoctorRequestSW.tmpPossibleOutcomes.Clear();
            WorldObject_DoctorRequestSW.tmpPossibleOutcomes.Add(new Pair<Action, float>(delegate
            {
                this.Outcome_Disaster(caravan);
            }, BaseWeight_Disaster * badOutcomeWeightFactor));
            WorldObject_DoctorRequestSW.tmpPossibleOutcomes.Add(new Pair<Action, float>(delegate
            {
                this.Outcome_Backfire(caravan);
            }, BaseWeight_Backfire * badOutcomeWeightFactor));
            WorldObject_DoctorRequestSW.tmpPossibleOutcomes.Add(new Pair<Action, float>(delegate
            {
                this.Outcome_Neutral(caravan);
            }, BaseWeight_TalksFlounder));
            WorldObject_DoctorRequestSW.tmpPossibleOutcomes.Add(new Pair<Action, float>(delegate
            {
                this.Outcome_Success(caravan);
            }, BaseWeight_Success * num));
            WorldObject_DoctorRequestSW.tmpPossibleOutcomes.Add(new Pair<Action, float>(delegate
            {
                this.Outcome_Triumph(caravan);
            }, BaseWeight_Triumph * num));
            Action first = WorldObject_DoctorRequestSW.tmpPossibleOutcomes.RandomElementByWeight((Pair<Action, float> x) => x.Second).First;
            first();
            pawn.skills.Learn(SkillDefOf.Medicine, MedicalXPGainAmount, true);
            pawn.skills.Learn(SkillDefOf.Social, SocialXPGainAmount, true);
            Find.WorldObjects.Remove(this);
        }

        private void Outcome_Disaster(Caravan caravan)
        {
            LongEventHandler.QueueLongEvent(delegate
            {
                int randomInRange = WorldObject_DoctorRequestSW.DisasterFactionRelationOffset.RandomInRange;
                this.Faction.TryAffectGoodwillWith(Faction.OfPlayer, randomInRange, false, false, null, null);
                if (!this.Faction.HostileTo(Faction.OfPlayer))
                {
                    Faction.OfPlayer.TryAffectGoodwillWith(this.Faction, Mathf.Min(randomInRange, Faction.OfPlayer.GoodwillToMakeHostile(this.Faction)), false, false, null, null);
                }
                IncidentParms incidentParms = StorytellerUtility.DefaultParmsNow(IncidentCategoryDefOf.ThreatBig, caravan);
                incidentParms.faction = this.Faction;
                PawnGroupMakerParms defaultPawnGroupMakerParms = IncidentParmsUtility.GetDefaultPawnGroupMakerParms(PawnGroupKindDefOf.Combat, incidentParms, true);
                defaultPawnGroupMakerParms.generateFightersOnly = true;
                List<Pawn> list = PawnGroupMakerUtility.GeneratePawns(defaultPawnGroupMakerParms, true).ToList<Pawn>();
                Map map = CaravanIncidentUtility.SetupCaravanAttackMap(caravan, list, false);
                if (list.Any<Pawn>())
                {
                    LordMaker.MakeNewLord(incidentParms.faction, new LordJob_AssaultColony(this.Faction, true, true, false, false, true), map, list);
                }
                Find.TickManager.CurTimeSpeed = TimeSpeed.Paused;
                GlobalTargetInfo lookTarget = (!list.Any<Pawn>()) ? GlobalTargetInfo.Invalid : new GlobalTargetInfo(list[0].Position, map, false);
                Find.LetterStack.ReceiveLetter("LetterLabelDoctorRequest_DisasterSW".Translate(), this.GetLetterText("LetterDoctorRequest_DisasterSW".Translate(new object[]
                {
                    this.Faction.def.pawnsPlural.CapitalizeFirst(),
                    this.Faction.Name,
                    Mathf.RoundToInt(randomInRange)
                }), caravan), LetterDefOf.ThreatBig, lookTarget, null);
            }, "GeneratingMapForNewEncounter", false, null);
        }

        private void Outcome_Backfire(Caravan caravan)
        {
            int randomInRange = WorldObject_DoctorRequestSW.BackfireFactionRelationOffset.RandomInRange;
            base.Faction.TryAffectGoodwillWith(Faction.OfPlayer, randomInRange, false, false, null, null);
            Find.LetterStack.ReceiveLetter("LetterLabelDoctorRequest_BackfireSW".Translate(), this.GetLetterText("LetterDoctorRequest_BackfireSW".Translate(new object[]
            {
                base.Faction.Name,
                Mathf.RoundToInt(randomInRange)
            }), caravan), LetterDefOf.NegativeEvent, caravan, null);
        }

        private void Outcome_Neutral(Caravan caravan)
        {
            Find.LetterStack.ReceiveLetter("LetterLabelDoctorRequest_NeutralSW".Translate(), this.GetLetterText("LetterDoctorRequest_NeutralSW".Translate(new object[]
            {
                base.Faction.Name
            }), caravan), LetterDefOf.NeutralEvent, caravan, null);
        }

        private void Outcome_Success(Caravan caravan)
        {
            int randomInRange = WorldObject_DoctorRequestSW.SuccessFactionRelationOffset.RandomInRange;
            base.Faction.TryAffectGoodwillWith(Faction.OfPlayer, randomInRange, false, false, null, null);
            Find.LetterStack.ReceiveLetter("LetterLabelDoctorRequest_SuccessSW".Translate(), this.GetLetterText("LetterDoctorRequest_SuccessSW".Translate(new object[]
            {
                base.Faction.Name,
                Mathf.RoundToInt(randomInRange)
            }), caravan), LetterDefOf.PositiveEvent, caravan, null);
        }

        private void Outcome_Triumph(Caravan caravan)
        {
            int randomInRange = WorldObject_DoctorRequestSW.TriumphFactionRelationOffset.RandomInRange;
            base.Faction.TryAffectGoodwillWith(Faction.OfPlayer, randomInRange);
            ThingSetMakerParams thingMakerparms = default(ThingSetMakerParams);
            thingMakerparms.totalMarketValueRange = new FloatRange?(RewardtotalMarketValueRange);
            List<Thing> list = ThingSetMakerDefOf.Reward_ItemsStandard.root.Generate(thingMakerparms);
            for (int i = 0; i < list.Count; i++)
            {
                caravan.AddPawnOrItem(list[i], true);
            }
            Find.LetterStack.ReceiveLetter("LetterLabelDoctorRequest_TriumphSW".Translate(), this.GetLetterText("LetterDoctorRequest_TriumphSW".Translate(new object[]
            {
                base.Faction.Name,
                Mathf.RoundToInt(randomInRange),
                list[0].Label
            }), caravan), LetterDefOf.PositiveEvent, caravan, null);
        }

        private string GetLetterText(string baseText, Caravan caravan)
        {
            string text = baseText;
            Pawn pawn = BestCaravanPawnUtility.FindPawnWithBestStat(caravan, StatDefOf.MedicalTendQuality);
            if (pawn != null)
            {
                text = text + "\n\n" + "DoctorRequestXPGainSW".Translate(new object[]
                {
                    pawn.LabelShort,
                    MedicalXPGainAmount,
                    SocialXPGainAmount
                });
            }
            return text;
        }

        private static float GetBadOutcomeWeightFactor(Pawn diplomat)
        {
            float statValue = diplomat.GetStatValue(StatDefOf.MedicalTendQuality, true);
            return WorldObject_DoctorRequestSW.GetBadOutcomeWeightFactor(statValue);
        }

        private static float GetBadOutcomeWeightFactor(float MedicalTendQuality)
        {
            return WorldObject_DoctorRequestSW.BadOutcomeFactorAtMedicalTendQuality.Evaluate(MedicalTendQuality);
        }

        [DebuggerHidden]
        public override IEnumerable<FloatMenuOption> GetFloatMenuOptions(Caravan caravan)
        {
            //Old Stuff that gets removed the apparently produced errors
            /*
            yield return new FloatMenuOption("VisitPeaceTalks".Translate(new object[]
            {
                this.Label
            }), delegate
            {
                caravan.pather.StartPath(this.Tile, new CaravanArrivalAction_VisitDoctorRequest(this), true);
            }, MenuOptionPriority.Default, null, null, 0f, null, this);
            if (Prefs.DevMode)
            {
                yield return new FloatMenuOption("VisitPeaceTalks".Translate(new object[]
                {
                    this.Label
                }) + " (Dev: instantly)", delegate
                {
                    caravan.Tile = this.Tile;
                    caravan.pather.StopDead();
                    new CaravanArrivalAction_VisitDoctorRequest(this).Arrived(caravan);
                }, MenuOptionPriority.Default, null, null, 0f, null, this);
            }
            */

            foreach (FloatMenuOption o in base.GetFloatMenuOptions(caravan))
            {
                yield return o;
            }
            foreach (FloatMenuOption f in CaravanArrivalAction_VisitDoctorRequest.GetFloatMenuOptions(caravan, this))
            {
                yield return f;
            }
            yield break;
        }

    }

    public class CaravanArrivalAction_VisitDoctorRequest : CaravanArrivalAction
    {
        private WorldObject_DoctorRequestSW doctorRequest;

        //I can leave Peacetalks here because it just says Visit {0} and I can piggyback on already existing translations this way
        public override string Label => "VisitPeaceTalks".Translate(doctorRequest.Label);

        public override string ReportString => "CaravanVisiting".Translate(doctorRequest.Label);

        public static FloatMenuAcceptanceReport CanVisit(Caravan caravan, WorldObject_DoctorRequestSW doctorRequest)
        {
            return doctorRequest != null && doctorRequest.Spawned;
        }

        public CaravanArrivalAction_VisitDoctorRequest()
        {
        }

        public CaravanArrivalAction_VisitDoctorRequest(WorldObject_DoctorRequestSW doctorRequest)
        {
            this.doctorRequest = doctorRequest;
        }

        public override FloatMenuAcceptanceReport StillValid(Caravan caravan, int destinationTile)
        {
            FloatMenuAcceptanceReport floatMenuAcceptanceReport = base.StillValid(caravan, destinationTile);
            if (!(bool)floatMenuAcceptanceReport)
            {
                return floatMenuAcceptanceReport;
            }
            if (doctorRequest != null && doctorRequest.Tile != destinationTile)
            {
                return false;
            }
            return CanVisit(caravan, doctorRequest);
        }

        public override void Arrived(Caravan caravan)
        {
            this.doctorRequest.Notify_CaravanArrived(caravan);
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_References.Look<WorldObject_DoctorRequestSW>(ref this.doctorRequest, "doctorRequest", false);
        }

        public static IEnumerable<FloatMenuOption> GetFloatMenuOptions(Caravan caravan, WorldObject_DoctorRequestSW doctorRequest)
        {
            return CaravanArrivalActionUtility.GetFloatMenuOptions<CaravanArrivalAction_VisitDoctorRequest>(() => CanVisit(caravan, doctorRequest), () => new CaravanArrivalAction_VisitDoctorRequest(doctorRequest), "VisitPeaceTalks".Translate(doctorRequest.Label), caravan, doctorRequest.Tile, doctorRequest);
        }
    }

    public class IncidentWorker_PsychicEmitterActivationSW : IncidentWorker
    {
        private static readonly IntRange SiteTimeoutDaysRange = new IntRange(17, 21);
        private static readonly IntRange EmitterPowerTimeoutDaysRange = new IntRange(8, 16);

        protected override bool CanFireNowSub(IncidentParms parms)
        {
            int tile;
            //there should be a free tile nearby and no active psychic drone or sooth
            return base.CanFireNowSub(parms) && TileFinder.TryFindNewSiteTile(out tile, 5, 10, false, TileFinderMode.Random ,exitOnFirstTileFound: false) && CanFireOnAllPlayerMaps() && LoadedModManager.GetMod<SparklingWorlds>().GetSettings<SparklingWorldsSettings>().psychicEmitter;
        }

        public bool CanFireOnAllPlayerMaps()
        {
            List<Map> mapList = Find.Maps;
            foreach(Map map in mapList)
            {
                if (!map.IsPlayerHome)
                {
                    //If the map is not a player map move on to next map
                    continue;
                }
                if (map.listerThings.ThingsOfDef(ThingDefOf.PsychicDronerShipPart).Count > 0)
                {
                    return false;
                }
                if(map.gameConditionManager.ConditionIsActive(GameConditionDefOf.PsychicDrone) || map.gameConditionManager.ConditionIsActive(GameConditionDefOf.PsychicSoothe))
                {
                    return false;
                }
            }
            return true;
        }

        public static Site CreatePsychicEmitterSiteSW(int tile, int days)
        {
            Site site = (Site)WorldObjectMaker.MakeWorldObject(WorldObjectsDefsSW.EventSiteSW);
            site.SetFaction(Faction.OfAncientsHostile);
            site.Tile = tile;

            site.AddPart(new SitePart(site, SiteDefsSW.PsychicEmitterCoreSW, SiteDefsSW.PsychicEmitterCoreSW.Worker.GenerateDefaultParams(StorytellerUtility.DefaultSiteThreatPointsNow(), tile, Faction.OfAncients)));
            site.AddPart(new SitePart(site, SiteDefsSW.EmitterInsanityPulsSW, SiteDefsSW.EmitterInsanityPulsSW.Worker.GenerateDefaultParams(StorytellerUtility.DefaultSiteThreatPointsNow(), tile, Faction.OfAncients)));

            site.GetComponent<TimeoutComp>().StartTimeout(days * 60000);
            Find.WorldObjects.Add(site);
            return site;
        }


        protected override bool TryExecuteWorker(IncidentParms parms)
        {
            int tile;
            if (!TileFinder.TryFindNewSiteTile(out tile, 5, 10, false, TileFinderMode.Random ,exitOnFirstTileFound: false))
            {
                return false;
            }
            int EventDuration = SiteTimeoutDaysRange.RandomInRange;
            Gender targetGender = SelectRandomGender();

            Site site = CreatePsychicEmitterSiteSW(tile, EventDuration);
            site.GetComponent<WorldObjectComp_PsychicEmitterActivationSW>().StartEvent(ThingDefsSW.PsychicWaveEmitterSW, targetGender, EmitterPowerTimeoutDaysRange.RandomInRange);
            string text = string.Format(this.def.letterText, targetGender.ToString().Translate().ToLower()).CapitalizeFirst();
            Find.LetterStack.ReceiveLetter(this.def.letterLabel, text, this.def.letterDef, site, null);

            return true;
        }

        public Gender SelectRandomGender()
        {
            if (Rand.Chance(0.5f))
            {
                return Gender.Female;
            }
            return Gender.Male;
        }
    }

    public class SitePartWorker_EmitterInsanityPulsSW : SitePartWorker
    {
        public override void PostMapGenerate(Map map)
        {
            if (Find.WorldObjects.WorldObjectOfDefAt(WorldObjectsDefsSW.EventSiteSW, map.Tile).GetComponent<WorldObjectComp_PsychicEmitterActivationSW>().IsActive())
            {
                IncidentParms incidentParms = StorytellerUtility.DefaultParmsNow(IncidentCategoryDefOf.ThreatBig, map);
                incidentParms.forced = true;

                QueuedIncident queuedIncident = new QueuedIncident(new FiringIncident(IncidentDefsSW.PsychicEmitterAnimalInsanitySW, null, incidentParms), Find.TickManager.TicksGame + 30);
                Find.Storyteller.incidentQueue.Add(queuedIncident);
            }
        }
    }

    public class WorldObjectCompProperties_PsychicEmitterActivationSW : WorldObjectCompProperties
    {
        public WorldObjectCompProperties_PsychicEmitterActivationSW()
        {
            this.compClass = typeof(WorldObjectComp_PsychicEmitterActivationSW);
        }
    }

    public class WorldObjectComp_PsychicEmitterActivationSW : WorldObjectComp
    {
        private bool activeEmitter;

        public GameConditionDef currentGameConditionDef = GameConditionDefOf.PsychicDrone;

        public Gender currentGenderAffected = Gender.Male;

        private Thing thingToDestroy;

        private ThingDef targetThingDef = ThingDefsSW.PsychicWaveEmitterSW;

        private static readonly IntRange DroneTimeoutQuarterDaysRange = new IntRange(6, 12);

        private static readonly IntRange MassAnimalInsanityTimeoutQuarterDaysRange = new IntRange(10, 18);

        private int tickAtNextConditionReRoll = -1;

        private int tickAtNextAnimalInstanity = Find.TickManager.TicksGame + MassAnimalInsanityTimeoutQuarterDaysRange.RandomInRange * 15000;

        private int tickAtEmitterShutdown = Find.TickManager.TicksGame + (10 * 60000) - 1000;

        private PsychicDroneLevel psychicLevel = PsychicDroneLevel.BadMedium;

        public bool IsActive()
        {
            return activeEmitter;
        }

        public bool StartEvent(ThingDef targetDef, Gender startingGender, int emitterPowerDays)
        {
            calculateDroneLevel();
            targetThingDef = targetDef;

            MakeAndRegisterGameCondition(GameConditionDefOf.PsychicDrone, startingGender);

            tickAtNextAnimalInstanity = Find.TickManager.TicksGame + MassAnimalInsanityTimeoutQuarterDaysRange.RandomInRange * 15000 - 600;
            tickAtEmitterShutdown = Find.TickManager.TicksGame + emitterPowerDays * 60000 -1000;
            activeEmitter = true;
            return activeEmitter;
        }

        public PsychicDroneLevel calculateDroneLevel()
        {
            float maxPoints = 0;
            List<Map> mapList = Find.Maps;
            foreach (Map map in mapList)
            {
                float points = StorytellerUtility.DefaultThreatPointsNow(map);
                if(maxPoints < points)
                {
                    maxPoints = points;
                }
            }
            PsychicDroneLevel level;
            if (maxPoints < 1500f)
            {
                level = PsychicDroneLevel.BadLow;
            }
            else if (maxPoints < 3800f)
            {
                level = PsychicDroneLevel.BadMedium;
            }
            else
            {
                level = PsychicDroneLevel.BadHigh;
            }
            psychicLevel = level;
            return level;
        }

        private void MakeAndRegisterGameCondition(GameConditionDef gameConditionDef, Gender affectedGender)
        {
            int durationTicks = DroneTimeoutQuarterDaysRange.RandomInRange * 15000;
            GameCondition_PsychicEmanation gameCondition_PsychicEmanation = (GameCondition_PsychicEmanation)GameConditionMaker.MakeCondition(gameConditionDef, durationTicks);
            gameCondition_PsychicEmanation.gender = affectedGender;
            if (gameConditionDef == GameConditionDefOf.PsychicDrone)
            {
                gameCondition_PsychicEmanation.level = psychicLevel;
            }

            List<Map> mapList = Find.Maps;

            foreach(Map affectedMap in mapList)
            {
                affectedMap.gameConditionManager.RegisterCondition(gameCondition_PsychicEmanation);
            }

            currentGameConditionDef = gameConditionDef;
            currentGenderAffected = affectedGender;
            tickAtNextConditionReRoll = Find.TickManager.TicksGame + durationTicks;
            SoundDefOf.PsychicPulseGlobal.PlayOneShotOnCamera(Find.CurrentMap);
        }

        private void RemoveAllRegisteredGameConditions()
        {
            try
            {
                List<Map> mapList = Find.Maps;
                foreach (Map affectedMap in mapList)
                {
                    GameConditionManager conditionManager = affectedMap.gameConditionManager;
                    if (conditionManager.ConditionIsActive(currentGameConditionDef))
                    {
                        conditionManager.ActiveConditions.Remove(conditionManager.GetActiveCondition(currentGameConditionDef));
                    }
                }
                Messages.Message(currentGameConditionDef.endMessage, MessageTypeDefOf.NeutralEvent, true);
            }
            catch
            {
                Log.Warning("Something went wrong with psychic sooth/drone removal. Exception caught and handeled");
            }
        }

        public Gender SelectRandomGender()
        {
            if (Rand.Chance(0.5f))
            {
                return Gender.Female;
            }
            return Gender.Male;
        }

        public override void CompTick()
        {
            try
            {
                if (activeEmitter)
                {
                    MapParent mapParent = this.parent as MapParent;
                    if (mapParent != null && ParentHasMap && getThingToDestroy() != null && getThingToDestroy().Destroyed)
                    {
                        EndEvent();
                        return;
                    }

                    if (tickAtNextConditionReRoll <= Find.TickManager.TicksGame)
                    {
                        RemoveAllRegisteredGameConditions();
                        Gender targetGender = SelectRandomGender();
                        if (currentGameConditionDef == GameConditionDefOf.PsychicSoothe)
                        {
                            MakeAndRegisterGameCondition(GameConditionDefOf.PsychicDrone, targetGender);
                            string text = "LetterPsychicEmitter_NewDroneSW".Translate(targetGender.ToString());
                            Find.LetterStack.ReceiveLetter("LetterLabelPsychicEmitter_NewDroneSW".Translate(), text, LetterDefOf.ThreatSmall, null);
                        }
                        else
                        {
                            if (Rand.Chance(0.75f))
                            {
                                if(currentGenderAffected == Gender.Male)
                                {
                                    MakeAndRegisterGameCondition(GameConditionDefOf.PsychicDrone, Gender.Female);
                                    string text = "LetterPsychicEmitter_NewDroneSW".Translate(Gender.Female.ToString());
                                    Find.LetterStack.ReceiveLetter("LetterLabelPsychicEmitter_NewDroneSW".Translate(), text, LetterDefOf.ThreatSmall, null);
                                }
                                else
                                {
                                    MakeAndRegisterGameCondition(GameConditionDefOf.PsychicDrone, Gender.Male);
                                    string text = "LetterPsychicEmitter_NewDroneSW".Translate(Gender.Male.ToString());
                                    Find.LetterStack.ReceiveLetter("LetterLabelPsychicEmitter_NewDroneSW".Translate(), text, LetterDefOf.ThreatSmall, null);
                                }
                            }
                            else
                            {
                                MakeAndRegisterGameCondition(GameConditionDefOf.PsychicSoothe, targetGender);
                                string text = "LetterPsychicEmitter_NewSootheSW".Translate(targetGender.ToString());
                                Find.LetterStack.ReceiveLetter("LetterLabelPsychicEmitter_NewSootheSW".Translate(), text, LetterDefOf.PositiveEvent, null);
                            }
                        }

                    }

                    if (tickAtNextAnimalInstanity <= Find.TickManager.TicksGame)
                    {
                        IncidentParms incidentParms = StorytellerUtility.DefaultParmsNow(IncidentCategoryDefOf.ThreatBig, Find.AnyPlayerHomeMap);
                        incidentParms.forced = true;

                        QueuedIncident queuedIncident = new QueuedIncident(new FiringIncident(IncidentDefsSW.PsychicEmitterAnimalInsanitySW, null, incidentParms), Find.TickManager.TicksGame + 10);
                        Find.Storyteller.incidentQueue.Add(queuedIncident);

                        tickAtNextAnimalInstanity = MassAnimalInsanityTimeoutQuarterDaysRange.RandomInRange * 15000 + Find.TickManager.TicksGame;
                    }

                    if (tickAtEmitterShutdown <= Find.TickManager.TicksGame)
                    {
                        Find.LetterStack.ReceiveLetter("LetterLabelPsychicEmitter_ShutdownSW".Translate(), "LetterPsychicEmitter_ShutdownSW".Translate(), LetterDefOf.NeutralEvent, null);
                        EndEvent();
                    }
                }

            }

            catch
            {
                EndEvent();
            }
        }

        public Thing getThingToDestroy()
        {
            if (thingToDestroy == null)
            {
                if (targetThingDef == null)
                {
                    return null;
                }
                MapParent mapParent = this.parent as MapParent;
                if (mapParent != null && ParentHasMap)
                {
                    thingToDestroy = GenCollection.RandomElement<Thing>((IEnumerable<Thing>)mapParent.Map.listerThings.ThingsOfDef(targetThingDef));
                }
            }
            return thingToDestroy;
        }

        public void EndEvent()
        {
            if (!activeEmitter)
            {
                //already shut down
                return;
            }
            activeEmitter = false;
            try
            {
                RemoveAllRegisteredGameConditions();
            }
            catch
            {
                Log.Warning("Affected map doesn't exist anymore. Exception caught.");
            }
                
        }

        public override void PostPostRemove()
        {
            EndEvent();
        }

        public override void PostMyMapRemoved()
        {
            EndEvent();
        }

        public override void PostExposeData()
        {
            Scribe_Values.Look<bool>(ref this.activeEmitter, "activeEmitter", true, false);
            Scribe_Values.Look<int>(ref this.tickAtNextConditionReRoll, "tickAtNextConditionReRoll", 0, false);
            Scribe_Values.Look<int>(ref this.tickAtNextAnimalInstanity, "tickAtNextAnimalInstanity", 0, false);
            Scribe_Values.Look<int>(ref this.tickAtEmitterShutdown, "tickAtEmitterShutdown", 0, false);
            Scribe_Defs.Look<GameConditionDef>(ref this.currentGameConditionDef, "currentGameConditionDef");
            Scribe_Values.Look<Gender>(ref this.currentGenderAffected, "currentGenderAffected");
            Scribe_References.Look<Thing>(ref this.thingToDestroy, "thingToDestroy");
            Scribe_Defs.Look<ThingDef>(ref this.targetThingDef, "targetThingDef");
            Scribe_Values.Look<PsychicDroneLevel>(ref this.psychicLevel, "psychicLevel");
        }

    }

    public class IncidentWorker_PsychicEmitterAnimalInsanitySW : IncidentWorker
    {
        public static bool AnimalUsable(Pawn p)
        {
            return p.Spawned && !p.Position.Fogged(p.Map) && (!p.InMentalState || !p.MentalStateDef.IsAggro) && !p.Downed && p.Faction == null;
        }

        public static void DriveInsane(Pawn p)
        {
            p.mindState.mentalStateHandler.TryStartMentalState(MentalStateDefOf.Manhunter, null, true, false, null);
        }

        public float maxResolvePoints()
        {
            float maxPoints = 0;
            List<Map> mapList = Find.Maps;
            foreach (Map map in mapList)
            {
                float points = StorytellerUtility.DefaultThreatPointsNow(map);
                if (maxPoints < points)
                {
                    maxPoints = points;
                }
            }
            return maxPoints;
        }

        public PawnKindDef getMostCombatPowerPawns(Map map, float resolvePoints)
        {
            float maxCombatPower = 0f;
            PawnKindDef mostCombatPawnKind = null;
            List<PawnKindDef> potentialPawnKinds = new List<PawnKindDef>();

            //get all PawnKindDefs that are on the map and are suitable
            IEnumerable<PawnKindDef> candidatePawnKinds = from def in DefDatabase<PawnKindDef>.AllDefs
                                              where def.RaceProps.Animal && def.combatPower <= resolvePoints && (from p in map.mapPawns.AllPawnsSpawned
                                                                                                                 where p.kindDef == def && AnimalUsable(p)
                                                                                                                 select p).Count() >= 1
                                              select def;
            //Check which PawnKind can yield the highest total combat power
            foreach(PawnKindDef candidate in candidatePawnKinds)
            {
                int pawnKindAmount = (from p in map.mapPawns.AllPawnsSpawned
                                      where p.kindDef == candidate && AnimalUsable(p)
                                      select p).Count();
                float pawnTotalPoints = candidate.combatPower * pawnKindAmount;
                if(pawnTotalPoints >= resolvePoints)
                {
                    potentialPawnKinds.Add(candidate);
                }
                if(pawnTotalPoints > maxCombatPower)
                {
                    mostCombatPawnKind = candidate;
                }
            }
            
            //Pick the kind with the highest combat power
            if (mostCombatPawnKind != null)
            {
                return mostCombatPawnKind;
            }

            if (!potentialPawnKinds.TryRandomElement(out PawnKindDef animalDef))
            {
                return null;
            }

            return animalDef;
        }

        protected override bool TryExecuteWorker(IncidentParms parms)
        {
            try
            {
                Map map = (Map)parms.target;

                float resolvePoints = maxResolvePoints();

                PawnKindDef kindToManhunt = getMostCombatPowerPawns(map, resolvePoints);

                if(kindToManhunt == null)
                {
                    return false;
                }

                List<Pawn> list = (from p in map.mapPawns.AllPawnsSpawned
                 where p.kindDef == kindToManhunt && AnimalUsable(p)
                 select p).ToList();

                float combatPower = kindToManhunt.combatPower;
                float num = 0f;
                int num2 = 0;
                Pawn pawn = null;
                list.Shuffle();
                foreach (Pawn item in list)
                {
                    if (num + combatPower > resolvePoints)
                    {
                        break;
                    }
                    DriveInsane(item);
                    num += combatPower;
                    num2++;
                    pawn = item;
                }
                if (num == 0f)
                {
                    return false;
                }
                string label;
                string text;
                LetterDef textLetterDef;

                if (num2 == 1)
                {
                    label = "LetterLabelAnimalInsanitySingle".Translate(pawn.Label, pawn.Named("ANIMAL"));
                    text = "AnimalInsanitySingle".Translate(pawn.Label, pawn.Named("ANIMAL"));
                    textLetterDef = LetterDefOf.ThreatSmall;
                }
                else
                {
                    label = "LetterLabelPsychicEmitter_MassAnimalInsanitySW".Translate() + ": " + kindToManhunt.LabelCap;
                    text = "LetterPsychicEmitter_MassAnimalInsanitySW".Translate(kindToManhunt.GetLabelPlural(-1));
                    textLetterDef = LetterDefOf.ThreatBig;
                }

                Find.LetterStack.ReceiveLetter(label, text, textLetterDef, pawn, null);
                SoundDefOf.PsychicPulseGlobal.PlayOneShotOnCamera(map);
                if (map == Find.CurrentMap)
                {
                    Find.CameraDriver.shaker.DoShake(1f);
                }
            }
            catch
            {
                return false;
            }
            return true;
        }
    }


    public class IncidentWorker_TradeFairSW : IncidentWorker
    {
        private const int MinDistance = 6;

        private const int MaxDistance = 15;

        private const int TimeoutDays = 9;

        protected override bool CanFireNowSub(IncidentParms parms)
        {
            Faction faction;
            int num;
            return base.CanFireNowSub(parms) && this.TryHostingFindFaction(out faction) && this.TryFindTile(out num) && LoadedModManager.GetMod<SparklingWorlds>().GetSettings<SparklingWorldsSettings>().tradeFair;
        }

        private bool TryFindTile(out int tile)
        {
            return TileFinder.TryFindNewSiteTile(out tile, MinDistance, MaxDistance, false, TileFinderMode.Random ,exitOnFirstTileFound: false);
        }

        private Site MakeSite(int tile, Faction faction)
        {
            Site site = SiteMaker.MakeSite(SiteDefsSW.TradeFairCoreSW, tile: tile, faction: Faction.OfAncientsHostile); //faction replaced with Ancient Hostiles in 1.2 because the site owner always becomes hostile to the player on entry.
            site.GetComponent<TimeoutComp>().StartTimeout(TimeoutDays * 60000);
            Find.WorldObjects.Add(site);
            return site;
        }

        public int FriendsCount(Faction faction)
        {
            List<Faction> list = Find.FactionManager.AllFactionsVisible.ToList();
            if (list.Contains(faction))
            {
                list.Remove(faction);
            }
            list = (from f in list
                    where !faction.HostileTo(f) && !faction.HostileTo(Faction.OfPlayer)
                    select f).ToList();
            return list.Count();
        }

        private bool TryHostingFindFaction(out Faction faction)
        {
            return (from x in Find.FactionManager.AllFactions
                    where !x.def.hidden && !x.def.permanentEnemy && !x.IsPlayer && !x.defeated && x.PlayerGoodwill >= 0 && FriendsCount(x) >= 2 && !SettlementUtility.IsPlayerAttackingAnySettlementOf(x)
                    select x).TryRandomElement(out faction);
        }

        protected override bool TryExecuteWorker(IncidentParms parms)
        {
            Faction faction;
            if (!this.TryHostingFindFaction(out faction))
            {
                return false;
            }
            int tile;
            if (!this.TryFindTile(out tile))
            {
                return false;
            }
            Site site = MakeSite(tile, faction);
            if (site == null)
            {
                return false;
            }
            string text = string.Format(this.def.letterText.AdjustedFor(faction.leader, "PAWN"), faction.def.leaderTitle, faction.Name, TimeoutDays).CapitalizeFirst();
            Find.LetterStack.ReceiveLetter(this.def.letterLabel, text, this.def.letterDef, site, null);
            return true;
        }
    }

    public class IncidentWorker_InsectHiveDropSW : IncidentWorker
    {
        protected const float HivePoints = 300f;

        protected int chunkDistributionDistance = 16;

        protected override bool CanFireNowSub(IncidentParms parms)
        {
            Map map = (Map)parms.target;
            return base.CanFireNowSub(parms) && HiveUtility.TotalSpawnedHivesCount(map) < 10 && map.mapTemperature.SeasonalTemp > -15f && map.mapTemperature.SeasonalTemp < 50f && TryFindShipChunkDropCell(map.Center, map, 999999, out IntVec3 intVec) && LoadedModManager.GetMod<SparklingWorlds>().GetSettings<SparklingWorldsSettings>().insectHiveDrop;
        }

        protected int RandomCountToDrop(IncidentParms parms)
        {
            int hiveCount = (int)parms.points / (int)HivePoints;
            //induce a wider hive distribution if there are a lot of hives spawned
            if(hiveCount > 20)
            {
                chunkDistributionDistance = 20;
            }
            if(hiveCount > 40)
            {
                chunkDistributionDistance = 24;
            }
            if(hiveCount > 50)
            {
                hiveCount = Rand.RangeInclusive(48, 54);
            }
            return hiveCount;
        }

        protected override bool TryExecuteWorker(IncidentParms parms)
        {
            Map map = (Map)parms.target;
            if (!TryFindShipChunkDropCell(map.Center, map, 999999, out IntVec3 intVec))
            {
                return false;
            }
            SpawnShipChunks(intVec, map, RandomCountToDrop(parms));
            Find.LetterStack.ReceiveLetter(this.def.letterLabel, this.def.letterText, this.def.letterDef, new GlobalTargetInfo(intVec, map));
            return true;
        }

        protected bool TryFindShipChunkDropCell(IntVec3 nearLoc, Map map, int maxDist, out IntVec3 pos)
        {
            ThingDef shipChunkIncoming = ThingDefsSW.InfestedShipChunkIncomingSW;
            return CellFinderLoose.TryFindSkyfallerCell(shipChunkIncoming, map, out pos, 40, nearLoc, maxDist, true);
        }

        protected void SpawnChunk(IntVec3 pos, Map map)
        {
            SkyfallerMaker.SpawnSkyfaller(ThingDefsSW.InfestedShipChunkIncomingSW, ThingDefsSW.InfestedShipChunkSW, pos, map);
        }

        protected void SpawnShipChunks(IntVec3 firstChunkPos, Map map, int count)
        {
            SpawnChunk(firstChunkPos, map);
            for (int i = 0; i < count - 1; i++)
            {
                if (TryFindShipChunkDropCell(firstChunkPos, map, chunkDistributionDistance, out IntVec3 pos))
                {
                    SpawnChunk(pos, map);
                }
            }
        }
    }

    public class IncidentWorker_HuntingLodgeOppSW : IncidentWorker
    {
        private const int MinDistance = 5;

        private const int MaxDistance = 10;

        private static readonly IntRange TimeoutDaysRange = new IntRange(10, 24);

        private static readonly IntRange FeeRange = new IntRange(100, 400);

        private const int FeeDemandTimeoutTicks = 60000;

        protected override bool CanFireNowSub(IncidentParms parms)
        {
            if (!base.CanFireNowSub(parms))
            {
                return false;
            }
            int num;
            return GetHostingFaction() != null && this.TryFindTile(out num) && LoadedModManager.GetMod<SparklingWorlds>().GetSettings<SparklingWorldsSettings>().huntingLodge;
        }

        public int CalculateFee(Faction hostingFaction)
        {
            if(hostingFaction == null)
            {
                hostingFaction = GetHostingFaction();
            }
            if(hostingFaction.PlayerRelationKind == FactionRelationKind.Ally)
            {
                return 0;
            }
            return FeeRange.RandomInRange;
        }

        private bool TryFindTile(out int tile)
        {
            return TileFinder.TryFindNewSiteTile(out tile, MinDistance, MaxDistance, false, TileFinderMode.Random ,exitOnFirstTileFound: false);
        }

        public static Site MakeSite(int tile, int timeOutDays, Faction faction, PawnKindDef animalKind)
        {
            Site site = (Site)WorldObjectMaker.MakeWorldObject(WorldObjectsDefsSW.EventSiteSW);
            site.Tile = tile;
            site.AddPart(new SitePart(site, SiteDefsSW.HuntingLodgeCoreSW, SiteDefsSW.HuntingLodgeCoreSW.Worker.GenerateDefaultParams(StorytellerUtility.DefaultSiteThreatPointsNow(), tile, faction)));
            site.SetFaction(Faction.OfAncientsHostile); //Faction replaces with AncientHostiles because of 1.2 where faction becomes hostile on site entry
            site.GetComponent<TimeoutComp>().StartTimeout(timeOutDays * 60000);
            site.GetComponent<WorldObjectComp_AnimalToHuntSW>().animalKindDef = animalKind;
            Find.WorldObjects.Add(site);
            return site;
        }

        public Faction GetHostingFaction()
        {
            return Find.FactionManager.RandomNonHostileFaction();
        }

        protected override bool TryExecuteWorker(IncidentParms parms)
        {
            Faction faction = GetHostingFaction();
            if (faction == null)
            {
                return false;
            }
            if(faction.leader == null)
            {
                return false;
            }
            int tile;
            if (!this.TryFindTile(out tile))
            {
                return false;
            }
            int timeOutDays = TimeoutDaysRange.RandomInRange;
            int fee = CalculateFee(faction);
            IEnumerable<PawnKindDef> animalList = (from k in Find.WorldGrid[tile].biome.AllWildAnimals
                                                   where Find.World.tileTemperatures.SeasonAndOutdoorTemperatureAcceptableFor(tile, k.race)
                                                select k);
            if(animalList.Count() < 1)
            {
                return false;
            }
            PawnKindDef huntingTarget = animalList.RandomElement();

            string letterText = this.GetLetterText(faction, huntingTarget, timeOutDays, fee);

            Map map = TradeUtility.PlayerHomeMapWithMostLaunchableSilver();
            ChoiceLetter_HuntingLodgeOppFeeDemand choiceLetter_HuntingLodgeOppFeeDemand = (ChoiceLetter_HuntingLodgeOppFeeDemand)LetterMaker.MakeLetter(this.def.letterLabel, letterText, LetterDefsSW.HuntingLodgeOppFeeDemand);

            choiceLetter_HuntingLodgeOppFeeDemand.title = "HuntinLodgeOppTitleSW".Translate();
            choiceLetter_HuntingLodgeOppFeeDemand.radioMode = true;
            choiceLetter_HuntingLodgeOppFeeDemand.map = map;
            choiceLetter_HuntingLodgeOppFeeDemand.fee = fee;
            choiceLetter_HuntingLodgeOppFeeDemand.siteDaysTimeout = timeOutDays;
            choiceLetter_HuntingLodgeOppFeeDemand.tile = tile;
            choiceLetter_HuntingLodgeOppFeeDemand.animalKind = huntingTarget;
            choiceLetter_HuntingLodgeOppFeeDemand.alliedFaction = faction;
            choiceLetter_HuntingLodgeOppFeeDemand.StartTimeout(60000);
            Find.LetterStack.ReceiveLetter(choiceLetter_HuntingLodgeOppFeeDemand, null);

            return true;
        }

        private string GetLetterText(Faction alliedFaction, PawnKindDef animalDef, int days, int fee)
        {
            string letterText = string.Format(base.def.letterText, alliedFaction.leader.LabelShort, alliedFaction.def.leaderTitle, alliedFaction.Name, animalDef.GetLabelPlural(-1), days.ToString()).CapitalizeFirst();
            if (fee > 0)
            {
                letterText = letterText + "\n\n" + "HuntinLodgeOppFeeDemandSW".Translate(alliedFaction.leader.LabelShort, fee).CapitalizeFirst();
            }
            return letterText;
        }
    }

    public class ChoiceLetter_HuntingLodgeOppFeeDemand : ChoiceLetter
    {
        public Map map;

        public int fee;

        public int siteDaysTimeout;

        public PawnKindDef animalKind;

        public Faction alliedFaction;

        public int tile;

        public override IEnumerable<DiaOption> Choices
        {
            get
            {
                if (base.ArchivedOnly)
                {
                    yield return base.Option_Close;
                }
                else
                {
                    DiaOption accept = new DiaOption("HuntinLodgeOpp_AcceptSW".Translate());
                    accept.action = delegate
                    {
                        Site o = IncidentWorker_HuntingLodgeOppSW.MakeSite(tile, this.siteDaysTimeout, this.alliedFaction, this.animalKind);
                        CameraJumper.TryJumpAndSelect(o);
                        TradeUtility.LaunchSilver(this.map, this.fee);
                        Find.LetterStack.RemoveLetter(this);
                    };
                    accept.resolveTree = true;
                    if (this.map == null || !TradeUtility.ColonyHasEnoughSilver(this.map, this.fee))
                    {
                        accept.Disable("NeedSilverLaunchable".Translate(this.fee.ToString()));
                    }
                    yield return accept;
                    yield return base.Option_Reject;
                    yield return base.Option_Postpone;
                }
            }
        }

        public override bool CanShowInLetterStack
        {
            get
            {
                return base.CanShowInLetterStack && !this.alliedFaction.HostileTo(Faction.OfPlayer) && (this.map == null || Find.Maps.Contains(this.map));
            }
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_References.Look<Map>(ref this.map, "map", false);
            Scribe_References.Look<Faction>(ref this.alliedFaction, "alliedFaction", false);
            Scribe_Defs.Look<PawnKindDef>(ref this.animalKind, "animalKind");
            Scribe_Values.Look<int>(ref this.fee, "fee", 0, false);
            Scribe_Values.Look<int>(ref this.siteDaysTimeout, "siteDaysTimeout", 5, false);
            Scribe_Values.Look<int>(ref this.tile, "tile", -1, false);
        }
    }

    public class WorldObjectCompProperties_AnimalToHuntSW : WorldObjectCompProperties
    {
        public WorldObjectCompProperties_AnimalToHuntSW()
        {
            this.compClass = typeof(WorldObjectComp_AnimalToHuntSW);
        }
    }

    public class WorldObjectComp_AnimalToHuntSW : WorldObjectComp
    {
        public PawnKindDef animalKindDef;

        public override void PostExposeData()
        {
            Scribe_Defs.Look<PawnKindDef>(ref this.animalKindDef, "animalKindDef");
        }
    }

    public class LordJob_DefendTradeFairSW : LordJob
    {
        private Faction faction;

        private IntVec3 baseCenter;

        public LordJob_DefendTradeFairSW(Faction faction, IntVec3 baseCenter)
        {
            this.faction = faction;
            this.baseCenter = baseCenter;
        }

        public LordJob_DefendTradeFairSW()
        {
        }

        public override StateGraph CreateGraph()
        {
            StateGraph stateGraph = new StateGraph();
            LordToil_DefendBase lordToil_DefendBase = (LordToil_DefendBase)(stateGraph.StartingToil = new LordToil_DefendBase(baseCenter));
            LordToil_DefendBase lordToil_DefendBase2 = new LordToil_DefendBase(baseCenter);
            stateGraph.AddToil(lordToil_DefendBase2);
            LordToil_AssaultColony lordToil_AssaultColony = new LordToil_AssaultColony();
            lordToil_AssaultColony.useAvoidGrid = true;
            stateGraph.AddToil(lordToil_AssaultColony);
            Transition transition = new Transition(lordToil_DefendBase, lordToil_DefendBase2);
            transition.AddSource(lordToil_AssaultColony);
            transition.AddTrigger(new Trigger_BecameNonHostileToPlayer());
            stateGraph.AddTransition(transition);
            Transition transition2 = new Transition(lordToil_DefendBase2, lordToil_DefendBase);
            transition2.AddTrigger(new Trigger_BecamePlayerEnemy());
            stateGraph.AddTransition(transition2);
            return stateGraph;
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_References.Look<Faction>(ref this.faction, "faction", false);
            Scribe_Values.Look<IntVec3>(ref this.baseCenter, "baseCenter", default(IntVec3), false);
        }
    }

    public class IncidentWorker_WeaponsPodCrash : IncidentWorker
    {
        protected override bool CanFireNowSub(IncidentParms parms)
        {
            return base.CanFireNowSub(parms) && LoadedModManager.GetMod<SparklingWorlds>().GetSettings<SparklingWorldsSettings>().weaponsPodCrash;
        }
        protected override bool TryExecuteWorker(IncidentParms parms)
        {
            Map map = (Map)parms.target;
            int gunCount = Rand.RangeInclusive(1, 2);
            List<Thing> things = GenerateWeapons(gunCount);
            IntVec3 intVec = DropCellFinder.RandomDropSpot(map);
            DropPodUtility.DropThingsNear(intVec, map, things, 110, false, true, true);
            Find.LetterStack.ReceiveLetter("LetterLabelWeaponsCachePodCrash".Translate(), "CargoPodCrash".Translate(), LetterDefOf.PositiveEvent, new TargetInfo(intVec, map, false), null, null);
            return true;
        }

        protected List<Thing> GenerateWeapons(int gunCount)
        {
            List<Thing> returnList = new List<Thing>();

            IEnumerable<ThingDef> weaponList = (from x in ThingSetMakerUtility.allGeneratableItems
                                                where x.weaponTags != null && ((x.weaponTags.Contains("SniperRifle") || x.weaponTags.Contains("IndustrialGunAdvanced") || x.weaponTags.Contains("SimpleGun") || x.weaponTags.Contains("GunHeavy")) && !x.weaponTags.Contains("SpacerGun"))
                                                select x);
            for (int i = 0; i < gunCount; i++)
            {
                ThingDef thingDef;
                weaponList.TryRandomElement(out thingDef);
                if (thingDef == null)
                {
                    Log.Warning("Could not resolve thingdef to spawn weapons");
                    continue;
                }
                Thing weapon = ThingMaker.MakeThing(thingDef);
                CompQuality compQuality = weapon.TryGetComp<CompQuality>();
                if (compQuality != null)
                {
                    compQuality.SetQuality(QualityUtility.GenerateQualityTraderItem(), ArtGenerationContext.Outsider);
                }
                weapon.HitPoints -= Rand.RangeInclusive(28, 42);
                returnList.Add(weapon);
            }

            return returnList;
        }
    }
}
