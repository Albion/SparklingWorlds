﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Verse;

namespace SparklingWorlds
{
    [DefOf]
    public static class LauncherDefsSW
    {
        public static ThingDef Gun_TripleRocket;

        public static ThingDef Gun_DoomsdayRocket;
    }

    [StaticConstructorOnStartup]
    static class RemoveLauncherArtCompSW
    {
        static RemoveLauncherArtCompSW()
        {
            LauncherDefsSW.Gun_TripleRocket.comps.RemoveAll(x => x is CompProperties_Art);
            LauncherDefsSW.Gun_DoomsdayRocket.comps.RemoveAll(x => x is CompProperties_Art);
        }
    }
}
