﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Verse;

namespace SparklingWorlds
{
    public class HediffCompProperties_InduceMentalState : HediffCompProperties
    {
        public MentalStateDef mentalState;

        public float mtbDays = 1f;

        public HediffCompProperties_InduceMentalState()
        {
            this.compClass = typeof(HediffComp_InduceMentalState);
        }
    }

    public class HediffComp_InduceMentalState : HediffComp
    {
        private HediffCompProperties_InduceMentalState Props
        {
            get
            {
                return (HediffCompProperties_InduceMentalState)this.props;
            }
        }

        public override void CompPostTick(ref float severityAdjustment)
        {
            base.CompPostTick(ref severityAdjustment);

            if (this.parent.pawn.IsHashIntervalTick(60) && !this.parent.pawn.InMentalState && !this.parent.pawn.Downed && this.parent.pawn.Awake())
            {
                if (Rand.MTBEventOccurs(Props.mtbDays, 60000f, 60f) && this.parent.pawn.Faction == Find.FactionManager.OfPlayer)
                {
                    //set "caused by mood" to true to induce recovery thought
                    this.parent.pawn.mindState.mentalStateHandler.TryStartMentalState(Props.mentalState, null, false, true, null, true);
                    Messages.Message("MentalStateInductionMessageSW".Translate(new object[] { this.parent.pawn.LabelShort, Props.mentalState.label }), this.parent.pawn, MessageTypeDefOf.NeutralEvent);
                }
            }
        }
    }
}
