﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Verse;

namespace SparklingWorlds
{
    [DefOf]
    public static class MadDefOfSW
    {
        public static JobDef EnterMADSW;

        public static JobDef OpenMADSW;

        public static ThingDef MindAlteringDeviceSW;

        public static ThoughtDef WrongSW;

        public static ThoughtDef ConfusedSW;

        public static ThoughtDef WreckedSW;

        public static ThoughtDef ScrambledSW;

        public static HediffDef BadMigraineSW;

        public static HediffDef BodyRestartSW;

        public static HediffDef RewireSW;

        public static HediffDef BraindeathSW;

        public static HediffDef Dementia;
    }
}
